---
jupytext:
  main_language: python
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3.8.6 64-bit
  name: ''
---

# Recherche d'une chaine de caractères dans un texte

+++

On dispose d'une collection de textes disponibles sur [Gutenberg](http://www.gutenberg.org/browse/languages/fr), par exemple [Le Rouge et le Noir](http://www.gutenberg.org/ebooks/798.txt.utf-8)

```{code-cell} ipython
:tags: ['remove-output']

#!pip install requests
```

```{code-cell} ipython
import requests
r = requests.get('http://www.gutenberg.org/ebooks/798.txt.utf-8')
```
Le texte complet de l'oeuvre est désormais dans la variable `r.text`.

```{code-cell} ipython
r.text[:250]
```

```{code-cell} ipython
len(r.text)
```

On peut chercher les occurrences du prénom "Julien", le héros du livre.

```{code-cell} ipython
r.text.find('Julien')
```

On peut vérifier que c'est bien le cas.

```{code-cell} ipython
livre = r.text
livre[26760:26760+len('Julien')]
```

Il existe aussi une méthode pour compter les occurrences de la chaîne de caractère.

```{code-cell} ipython
livre.count('Julien')
```

En revanche, le prénom Dylan n'apparaît pas dans le texte.

```{code-cell} ipython
livre.find('Dylan')
```

```{code-cell} ipython
livre.find('Julien', 26760+1)
```

```{code-cell} ipython
livre.find('Julien', livre.find('Julien', 0) + 1)
```

Écrire une fonction permettant de donner la position de toutes les occurrences d'un mot donné.

```{code-cell} ipython
def get_all(texte: str, chaine:str ) -> list:
    """
    La fonction renvoie un tableau contenant la position de toutes les occurrences d'un mot.

    >>> get_all("Toto", "Toto")
    [0]
    >>> get_all("La mère de Toto a trois fils : Riri, Fifi et … Toto", "Toto")
    [12, 46]
    >>> get_all("Toto", "toto")
    []
    """
    l = []
    if texte.find(chaine) == -1:
        return l
    else:
        l = [texte.find(chaine)]
    while texte.find(chaine, l[-1] + 1) > -1:
        l.append(texte.find(chaine, l[-1] + 1))
    return l
```

```{code-cell} ipython
l = get_all(livre, 'Julien')
```

```{code-cell} ipython
len(l)
```

```{code-cell} ipython
%timeit get_all(livre, "Julien")
```

```{code-cell} ipython
livre[l[6] - 50 :l[6] + len("Julien") + 50 ].replace('\r\n',' ')
```

## Mise en œuvre naïve de l'algorithme

Ecrire une fonction de correspondance qui renvoie `True` si 


```{code-cell} ipython
def correspondance_naive(texte, motif, i):
    p = len(motif)
    for j in range(p):
        if texte[i+j] != motif[j]:
            return (False, 1)
    return (True, 0)
```

```{code-cell} ipython
def cherche_naive(texte, motif):
    n = len(texte)
    p = len(motif)
    i = 0
    while i + p <= n:
        ok, decalage = correspondance_naive(texte, motif, i)
        if ok:
            return i
        else:
            i = i + decalage
    return -1
```

```{code-cell} ipython
cherche_naive(livre, "Julien")
```

### Complexité de l'algorithme naïf

Dans le pire des cas, on a un truc genre `cherche("Jule Jule Jules","Jules")` où les $p-1$ premières lignes du motif correspondent et seule la $p$ième diffère. Si, de plus, le mot à chercher est à la fin du texte, on a donc $p\times (n-p) \sim \mathcal{O}(n^2)$ en complexité

+++

## Version de Boyer-Moore (et Horspool)

Deux idées :
+ Effectuer un pré-traitement pour déterminer les décalages
+ parcourir le motif de droite à gauche et non de gauche à droite.

Écrire une fonction qui calcule (prétraite) les décalages et stocke les résultats sous la forme d'une liste.

```{code-cell} ipython
def calculADroite(motif: str) -> dict:
    ...
```


```{code-cell} ipython
:tags: [hide-cell]

def calculADroite(motif):
    p = len(motif)
    aDroite = {}
    for j in range(p):
        aDroite[motif[j]] = j
    return aDroite
```

```{code-cell} ipython
calculADroite('Julien')
```

```{code-cell} ipython
calculADroite('maman')
```

Écrire une fonction qui  renvoie le décalage d'un caractère s'il existe dans le motif, et -1 sinon.

```{code-cell} ipython
def droite(c, aDroite):
    ...
```


```{code-cell} ipython
:tags: [hide-cell]

def droite(c, aDroite):
    if c in aDroite.keys():
        return aDroite[c]
    else:
        return -1
```

Écrire une fonction qui renvoie `(False, decalage)` ou `(True, 0)` si le motif est présent dans texte à partir de la position i. On passera comme dernier argument le dictionnaire précalculé du motif.

```{code-cell} ipython
def correspondance_decalage(texte, motif, i, aDroite):
    ...
```


```{code-cell} ipython
:tags: [hide-cell]

def correspondance_decalage(texte, motif, i, aDroite):
    p = len(motif)
    for j in range(p - 1, -1, -1):
        x = texte[i + j]
        if x != motif[j]:
            decalage = max(1, j - droite(x, aDroite))
            return (False, decalage)
    return (True, 0)
```

La dernière fonction sera la suivante :

```{code-cell} ipython
def cherche_decalage(texte, motif):
    n = len(texte)
    p = len(motif)
    i = 0
    aDroite = calculADroite(motif)
    while i + p <= n:
        ok, decalage = correspondance_decalage(texte, motif, i, aDroite)
        if ok:
            return i
        else:
            i = i + decalage
    return -1
```

```{code-cell} ipython
cherche_decalage(livre,"Julien")
```

```{code-cell} ipython
%timeit cherche_naive(livre, "Julien")
```

```{code-cell} ipython
%timeit cherche_decalage(livre, "Julien")
```

```{code-cell} ipython
%timeit livre.find('Julien')
```

```{code-cell} ipython
%timeit cherche_naive(livre, "Dylan")
```

```{code-cell} ipython
%timeit cherche_decalage(livre, "Dylan")
```

```{code-cell} ipython
%timeit livre.find('Dylan')
```
