---
jupytext:
  main_language: python
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3.8.6 64-bit
  name: ''
---

# Les tables de hachages

## Rappels sur les dictionnaires (implémentation de référence de Python)

### Création et ajout des valeurs

En Python les tables de hachage sont les `dict` (dictionnaires)

La création d'un dictionnaire peut se faire de la façon suivante.

```{code-cell} ipython
d = dict()
d
```

Pour compléter le dictionnaire, il suffit d'ajouter une clef (ici `'a'`) et sa valeur (`'aa'`)

```{code-cell} ipython
d['a'] = 'aa'
d
```

```{code-cell} ipython
:tags: [remove-cell]

del(d)
```

Un dictionnaire peut /a priori/ contenir tout type de données.
```{code-cell} ipython
d = {'robert':'bob'}
d["liste"] = [1,2,3]
d['1'] = 1
d['samuel'] = None
d[1] = 3
d
```
Une autre façon d'initialiser un dictionnaire :
```{code-cell} ipython
d = dict([("clef","valeur"),('clé','valeur')])
```
En particulier, un dictionnaire peut contenir un dictionnaire.
```{code-cell} ipython
d['a'] = dict()
d['a']['ab'] = ['absent', 'absurde', 'absoudre']
```

### Méthodes sur les dictionnaires

Écrire un dictionnaire avec les mots chat (cat), chien (dog), vache (cow), tigre (tiger) et licorne (unicorn) et leurs traduction en anglais

```{code-cell} ipython
:tags: [hide-cell]

animaux = {'chat':'cat','chien':'dog','vache':'cow','tigre':'tiger','licorne':'unicorn'}
```

Si on veut rajouter un animal, il faut vérifier que celui-ci n'existe pas, avec le mot clef `in` :

```{code-cell} ipython
'chat' in animaux.keys()
```

```{code-cell} ipython
'souris' in animaux.keys()
```

```{code-cell} ipython
:tags: ['hide-input']
def ajoute(mot_français, mot_anglais, dictionnaire):
    if mot_français not in dictionnaire.keys():
        dictionnaire[mot_français] = mot_anglais
```

```{code-cell} ipython
ajoute('souris','mouse',animaux)
```

```{code-cell} ipython
animaux
```

On peut récupérer les clefs, les valeurs ou les deux.

```{code-cell} ipython
animaux.keys()
```

```{code-cell} ipython
animaux.values()
```

```{code-cell} ipython
animaux.items()
```

## Fonctionnement d'un dictionnaire

### Principe de base

Pour obtenir un dictionnaire (une table de hachage), on a essentiellement besoin d'un tableau[^tableau], ainsi que d'une méthode permettant de transformer la clef en un identifiant numérique.

La taille du tableau servant au stockage des clefs et des valeurs, celle-ci est déterminée au départ, essentiellement en fonction de la taille.

::::{prf:example} dictionnaire avec au plus 10 entrées

On suppose, dans cet exemple, que les clefs sont des valeurs numériques, par exemple, des indicatifs téléphoniques par pays :
```python
{
    33: 'France',
    32: 'Belgique',
}
```
::::

```{code-cell} ipython
from cours.structures import Tableau

class Dictionnaire:
    n = 10
    def __init__(self):
        self.tableau = Tableau(self.n)
        for i in range(self.n):
            self.tableau[i] = 0
    def inserer(self, clef, valeur):
        self.tableau[clef % self.n] = (clef, valeur)
    def obtenir(self, clef):
        sortie = self.tableau[clef % self.n]
        if sortie == 0:
            raise KeyError(f"{clef}")
        else:
            return sortie
    def __repr__(self):
        sortie = "{"
        for i in range(self.n):
            if self.tableau[i] != 0:
                sortie = sortie + f"{self.tableau[i][0]}: {self.tableau[i][1]}"
            if i != n - 1:
                sortie = sortie + ", "
        sortie = sortie + "}"
        return sortie
```

On peut donc utiliser cette implémentation de dictionnaire.

```{code-cell} ipython
dictionnaire = Dictionnaire()
dictionnaire.inserer(32, 'Belgique')
dictionnaire
```

On peut obtenir une clef particulière

```{code-cell} ipython
dictionnaire.obtenir(32)
```
et obtenir une erreur sur les clefs n'existant pas
```{code-cell} ipython
:tags: ['allow-errors']

dictionnaire.obtenir(33)
```

```{code-cell} ipython
:tags: ['remove-cell']

dictionnaire.inserer(33,'France')
```

```{exercise-start}
```
Compléter la classe `Dictionnaire` avec une méthode `valeurs` qui renvoie la totalité des couples clefs valeurs.
```{exercise-end}
```

Ici, la fonction `clef % n` où $n$ est la taille du tableau s'appelle une fonction de hachage (hash function en anglais).

```{prf:definition} Fonction de hachage
Une fonction de hachage est une fonction de $K$, un espace fini de clef à images dans $\[0, m\]$, un espace de nombres entiers (alvéoles). Une telle fonction doit posséder quelques propriétés :
+ elle doit être rapide à calculer $\mathcal{O}(1)$.
+ elle doit être déterministe (toujours renvoyer le même résultat)
+ elle doit «bien répartir» les images (alvéoles)
```

Si on ajoute une clef `42: 'Allemagne'`, que se passe-t-il ?

```{code-cell} ipython
:tags: ['hide-cell']

dictionnaire.inserer(42, 'Allemagne')
dictionnaire
```

### Problème des collisions

Le phénomène précédent s'appelle **collision** de clef et est inévitable. Pour s'en convaincre, le cardinal (taille de l'ensemble) des clefs étant très grand devant le cardinal des alvéoles, il est évident que les fonctions de hachages ne peuvent pas être injectives[^injectif], et donc avoir deux antécédents distincts qui fournissent la même image. On peut éventuellement raffiner la fonction de hachage[^raffinement] pour limiter ce phénomène.

```{margin} Paradoxe des anniversaires
Parmi 23 personnes, la probabilité que deux d'entre-elles aient leur anniversaire le même jour est inférieur à 0,5. En effet, cette probabilité est de $\frac{m!}{(m-n)!}×\frac1{m^n}$
```

On considère les clefs suivantes, avec $n = 11$ :
```{code-cell} ipython
d = {16: 'Rubis', 18: 'Jade', 6: 'Topaze', 29: 'Opale', 50: 'Perle',
13: 'Saphir', 27: 'Agate', 31: 'Grenat', 28: 'Onyx'}
```
Calculons les différentes alvéoles, liées aux clefs :
```{code-cell} ipython
for clef in d.keys():
    print(clef % 11, end=", ")
```

On a donc la présence de quelques collisions.

Parmi les idées présentes, on peut en distinguer deux.
+ Le tableau contient des listes chaînées et l'alvéole permet de savoir quel __bucket__ contient la liste chaînée qui doit être parcourue pour trouver la clef.

```{exercise} Mise en œuvre de la liste chaînée
Modifier la classe `Dictionnaire` proposée ci-dessus pour mettre en œuvre cette idée. En particulier, il faudra répondre aux points suivants :
+ [ ] la méthode `inserer` doit ajouter le couple `(clef,valeur)` à la suite de la liste chaînée
+ [ ] la méthode `obtenir` doit parcourir la liste chaînée stockée dans l'alvéole correspondant à la clef.
On pourra utiliser l'implémentation de liste du cours.
```
Le tableau doit contenir les listes suivantes :
```
0 -> None
1 -> None
2 -> (13,Saphir)
3 -> None
4 -> None
5 -> (16, Rubis) -> (27, Agate) -> None
6 -> (6, Topaze) -> (50, Perle) -> (28, Onyx) -> None
7 -> (18, Jade) -> (29, Opale) -> None
8 -> None
9 -> (31, Grenat)
10 -> None
```

Il existe une autre méthode dite par _coalescence_ qui consiste à stocker, en plus de la clef et de la valeur, un pointeur vers `_suivant`. Cette valeur est initialisée à une valeur inexistante (`-1` ou `None`) et lorsqu'une collision survient, on modifie cette valeur vers la valeur suivante libre.

````{prf:example} exemple de coalescence
On insère `(16,Rubis)`, `(18,Jade)` et `(6, Topaze)` (on omet dans cette représentation les alvéoles vides, qui contiennent initialement (None,None,-1)):
```
5 -> (16, Rubis, -1)
6 -> (6, Topaze, -1)
7 -> (18, Jade, -1)
```
La liste des emplacements vides est `[10,9,8,4,3,2,1,0]`

À l'étape suivante, on obtient alors, après insertion de `(29, Opale)`, `(50, Perle)` et `(13, Saphir)`.
```
2 -> (13, Saphir, -1)
5 -> (16, Rubis, -1)
6 -> (6, Topaze, 9)
7 -> (18, Jade, 10)
9 -> (50, Perle)
10 -> (29, Opale)
```
````

Dans le cas de la coalescence, on obtient _in fine_ une liste chaînée «par l'exétérieur», contrairement aux listes chaînées «à l'intérieur» dans le cas du chaînage simple.

Cette méthode est assez facile à mettre en œuvre, mais présente des inconvénients comme une limite au nombre de clefs (la taille de la table) et comme la table dépend de l'ordre de toutes les clefs, la suppression s'avère assez coûteuse.

Il existe d'autres méthodes de résolution des collisions, mais qui seront évoquées dans l'enseignement supérieur, de même que les fonctions de hachage.

```{exercise}
On considère un tableau de hachage dont la fonction de hash est `x mod 257`.
Donner les valeurs des emplacements de 
+ 1
+ 34
+ 236
+ 2678
```

```{exercise}
On considère un tableau de hachage dont la fonction de hash est `x mod 257`.
On suppose que les collisions sont gérées par chaînage interne. Donner une représentation après l'insertion de 
+ 2547
+ 1548
+ 1478
+ 450
+ 263
+ 520
```

```{exercise}
Reprendre l'exercice précédent, en insérant les valeurs dans l'ordre croissant.
```

```{exercise}
On considère un tableau de hachage dont la fonction de hash est `x mod 257`.
On suppose que les collisions sont gérées par chaînage externe (coalescence). Donner une représentation après l'insertion de 
+ 2547
+ 1548
+ 1478
+ 450
+ 263
+ 520
```

```{exercise}
Reprendre l'exercice précédent, en insérant les valeurs dans l'ordre croissant.
```


[^tableau]: un tableau possède la propriété d'avoir un accès direct en $\mathcal{O}(n)$ pour les insertions et les modifications.
[^injectif]: une fonction $f\colon E \to F$ est dite injective lorsque, $\forall (x, y) \in E^2,\ f(x) = f(y) \implies x = y$, autrement dit, si deux images sont identiques, c'est que les antécédents le sont aussi.
[^raffinement]: une possibilité est de prendre un nombre premier comme module de division. On peut aussi consulter [ce cours de licence d'informatique](http://julien.sopena.fr/enseignements/L3-INFO5-Hachage/cours/01-Tables_de_hachage/01-Tables_de_hachage.pdf).

## Vers les fonctions de hachage sur des objets génériques

:::{margin}
Cette partie peut-être lue plus tard dans votre formation
:::

En Python, on peut créer un hash, avec la fonction `hash` qui utilise la méthode `__hash__` de la classe de l'objet.

```{code-cell} ipython
:tags: [margin]

hash('toto')
```

Le principe de cette fonction de hash est de fournir un condensat qui soit tel que 

+ deux objets différents ont des condensats différents
+ le hash soit rapide à calculer

```{code-cell} ipython
:tags: [margin]

hash('toto') == hash('tot')
```

On peut s'en rendre compte en comparant les temps d'exécution.

```{code-cell} ipython
def compare_chaine(chaine1, chaine2):
    for position in range(len(chaine1)):
        if chaine1[position] != chaine2[position]:
            return False
    return True
```

```{code-cell} ipython
:tags: [margin]

%timeit compare_chaine(s1,s2)
```

```{code-cell} ipython
def compare_hash(chaine1, chaine2):
    if hash(chaine1) != hash(chaine2):
        return False
    else:
        return True
```

```{code-cell} ipython
:tags: [margin]

%timeit compare_hash(s1, s2)
```

On imagine le cas suivant : on dispose d'un dictionnaire dont les clefs sont des chaines de caractères ASCII (8 bits). Écrire une fonction `str_to_int`, qui a une chaine de caractères associe un entier.

:::{margin}
On appelle cette méthode de factorisation, la méthode de Horner, qui donne permet de calculer le polynôme suivant

$P = a_0 X^d + a_1 X^{d-1} + \dots + a_{d-1} X + a_d$

$P = (a_0 X^{d-1} + a_1 X^{d-2} + \dots + a_{d-1}) X + a_d$

$P = ((a_0 X^{d-2} + a_1 X^{d-3} + \dots + a_{d-2})X + a_{d-1}) X + a_d$

:::

:::{code-block} python
def chaine_en_entier(chaine: str) -> int:
:::
```{code-cell} ipython
:tags: ['hide-cell']

def chaine_en_entier(chaine: str) -> int:
    n = 0
    for lettre in chaine:
        n = n * 256 + ord(lettre)
    return n
```

```{code-cell} ipython
chaine_en_entier('toto')
```

On aimerait que 'toto', 'toot', 'oott', 'ttoo' renvoient le même résultat

```{code-cell} ipython
chaine_en_entier('toot')
```

```{code-cell} ipython
chaine_en_entier('toot') % 255
```

```{code-cell} ipython
chaine_en_entier('toto') % 255
```

