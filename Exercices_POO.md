---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3.9.7 64-bit
  name: python3
---

# Programmation orientée objet _ Exercices

+++

### Exercice 1. 
On considère une classe dont le code en python est le suivant :

```{code-cell} ipython
class Voiture :
    def __init__(self, annee, coul, vmax) :
        self.annee = annee
        self.couleur = coul
        self.vitesse_max = vmax
        self.age = 2021 - self.annee

    def petite_annonce(self) :
        return "À vendre voiture "+ str(self.couleur)+ " de " + str(self.annee) +\
             ", vitesse maximale  "+ str(self.vitesse_max)+ " km/h."
```

 1. Quels sont les attributs des instances de cette classe ? Quelles sont les méthodes ?
 2. Le type des attributs n'est pas documenté dans le code ni le rôle de la méthode.
 
 Proposer une documentation.
 
 3. Ecrire dans la cellule qui suit le code nécessaire à la création d'un objet `ma_voiture` datant de 1957, de couleur grise et roulant à 110 km/h maximum.
 Appliquer la méthode à cette instance.

```{code-cell} ipython

```

## Exercice 2 La serrure.
Une serrure fonctionne avec un code secret qui est une séquence de 4 chiffres. La serrure peut être verrouillée ou déverrouillée. Pour déverrouiller la serrure il faut fournir le bon code.

+++

 1. Ecrire le **squelette** d'une classe Serrure.
  Cette classe doit implémenter les méthodes 
  - `verrouiller()` met la serrure en etat verrouillee,
  - `deverrouiller(code) ` deverouille la serrure si le code est egale au code secret. 
    
    renvoie True si le deverouillage a réussi, False sinon
  - `changer_code_secret(code)` remplace le code secret par code si c'est un code valide,
    renvoie True si le changement a réussi, False sinon
    
 Cette classe doit permettre d'écrire le code suivant :
 
 `>>> ma_serrure = Serrure(1925)      # le code de la serrure est 1925` <br>
 `>>> ma_serrure.verouillee           # etat de la serrure`     
 `True`<br>
 `>>> ma_serrure.deverouiller(1965)  # tentative de deverouillage`<br>
 `    False` 
 2. Compléter le code de chaque méthode.
 3. Documenter le code
 4. Proposer un petit scénario de test

```{code-cell} ipython
# Question 1 , 2 et 3
```

```{code-cell} ipython
# Question 4
```

## Exercice 3 Une Alarme
Une alarme est un système qui émet un son lorsqu'elle est armée et déclenchée.
Par défaut l'alarme est armée mais on peut la désarmer. Dans ce cas elle n'émettra pas de son si on la déclenche.

On souhaite écrire une classe Alarme qui ait le comportement suivant : <br>
`>>> mon_alarme = Alarme ("Pin Pon Pin Pon Pin Pon") `<br>
`>>> mon_alarme.declencher()`<br>
`   Pin Pon Pin Pon Pin Pon `<br>
`>>> mon_alarme.desarmer()`<br>
`>>> mon_alarme.declencher()    # rien ne se passe car l'alarme est désarmée` <br> 

+++

##  Exercice 4 Le coffre fort.
Un coffre fort contient un montant. Ce montant est nul si le coffre est vide.
Le coffre peut être ouvert ou fermé.
Si le coffre est ouvert, on peut le consulter, y déposer de l’argent ou en retirer.
Si le coffre est fermé, ces opérations sont impossibles.
On ne peut pas retirer plus que ce que contient le coffre.

 1. Compléter le code de la classe Coffre
 

```{code-cell} ipython
class Coffre :
    """ Une classe pour les coffres-forts 
        attributs : 
           ouvert  : bool 
           contenu : float
    """ 
    
    def __init__(self,  montant) :
        """ Coffre(montant) , crée un coffre fermé avec montant comme contenu initial """
        assert montant >=0        # Le montant initial doit être positif ou nul
        self.ouvert = False       # par défaut le coffre est fermé
        self.contenu = montant
        
    def ouvrir(self) :
        """ met le coffre en etat ouvert """
        self.ouvert = True
        pass
        
    def fermer(self) :
        """ met le coffre en etat fermé """
        pass
        
    def consulter(self) :
        """ donne le contenu du coffre s'il est ouvert, -1 sinon"""
        pass
        
    def deposer(self, montant) :
        """ajoute le montant au contenu si le coffre est ouvert. Renvoie le montant déposé (0 si fermé) """
        pass
        
    def retirer(self, montant) :
        """ retire le montant du coffre s'il est ouvert . Renvoie le montant déposé (0 si fermé)  """
        pass
        
```

 2. Ecrire un scénario utilisant cette classe pour tester les différentes méthodes.
 

```{code-cell} ipython
# scénario 
```

 3. Un programmeur fait une faute de frappe dans son code et écrit pour la méthode fermer() :<br>
`def fermer(self) : self.ouvret = False` <br>
Quelles vont être les conséquences de cette faute de frappe ?

**Remarque (hors programme)** En python on peut demander à limiter les attributs d'une classe avec la directive __slots__ = ...

Ici `__slots__=("ouvert","contenu")` au début de la classe va indiquer que ce sont les 2 seuls attributs. 

+++

## Exercice 5 Mini projet 
Ecrire la définition d'une classe CoffreAlarme. C'est un coffre qui est pourvu d'une serrure et d'une alarme.
Pour ouvrir le coffre il faut que la serrure soit déverrouillée.
Si l'on tente d'ouvrir le coffre alors que la serrure est verrouillee alors l'alarme se déclenche. 

Vous utiliserez les classes des exercices précédents en les adaptant.


+++

## Exercice 6 Des fractions
On représente une fraction par un objet qui a deux attributs : numerateur et denominateur qui sont des entiers.<br>
 1. Ecrire le début du code d'une classe Fraction.<br>
On doit pouvoir écrire <br>
`f = Fraction( 2, 3) `

2. Ajouter une méthode `representation` qui renvoie une chaine de caractères représentant la fraction sous forme a/b <br>
 `>>> f.representation()`<br>
 `2/3`
 
 **Remarque (hors programme)** En python la méthode `__repr__(self)` est appelée pour représenter un objet lors d'un print.<br>
 Si vous appelez votre méthode `__repr__(self)` alors elle sera automatiquement appelée par python dans :<br>
 `print(f)`<br>
 `2/3`
3. Programmer une méthode `egale(self, g)` qui renvoie True si et seulement si les deux fractions sont égales. <br>
4. Programmer les méthodes `somme(self, f)` et `produit(self,f)` qui vont effectuer la somme et le produit de la fraction `self`avec la fraction `f` et renvoyer l'objet fraction représentant le résultat. <br>
5. Programmer une méthode simplifier qui rend la fraction irréductible. 
 

```{code-cell} ipython

```

 

+++
