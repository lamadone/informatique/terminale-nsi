---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.12.0
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Exercices sur les fractales

```{code-cell} ipython
:tags: [remove-output]

!pip install ipycanvas
```

```{code-cell} ipython
from ipycanvas import Canvas
```

```{code-cell} ipython
c = Canvas(width=200,height=200,sync_image_data=True)
```

```{code-cell} ipython
def segment(x1,y1,x2,y2):
   c.stroke_line(x1,200-y1,x2,200-y2)
```

```{code-cell} ipython
segment(0,0,100,0)
segment(0,0,0,100)
```

```{code-cell} ipython
def fractale(x,y,l,etape):
    if etape == 0:
        segment(x,y,x+l,y)
        segment(x,y,x,y+l)
    else:
        segment(x,y,x+l,y)
        segment(x,y,x,y+l)
        fractale(x+l,y,l/2,etape-1)
        fractale(x,y+l,l/2,etape-1)
```

```{code-cell} ipython
c.clear()
fractale(0,0,100,4)
c
```

```{code-cell} ipython
:tags: [remove-cell]

del c
c = Canvas(width=200,height=200,sync_image_data=True)
fractale(0,0,100,4)
```

```{code-cell} ipython
:tags: [remove-cell]

c.sync_image_data
```

```{code-cell} ipython
:tags: [remove-cell]

c.to_file('fractale.png')
```

```{code-cell} ipython
:tags: [remove-input]

from IPython.display import Image
Image(filename='fractale.png')
```
