# Devoir surveille N<sup>o</sup> 1

````{exercise}
<div style="text-align: center; font-weight: bold;">Chiffrement selon le « Code de César »</div>

*Notion  abordée : programmation objet.*


Dans cet exercice, on étudie une méthode de chiffrement de chaînes de caractères
alphabétiques. Pour des raisons historiques, cette méthode de chiffrement
est appelée «code de César». On considère que les messages ne contiennent
que les lettres capitales de l’alphabet "ABCDEFGHIJKLMNOPQRSTUVWXYZ" et la
méthode de chiffrement utilise un nombre entier fixé appelé la clé de
chiffrement.

1. Soit la classe `CodeCesar` définie ci-dessous :

   ```{code-block} python
   class CodeCesar:
       def __init__(self, cle):
           self.cle = cle
           self.alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

       def decale(self, lettre):
           num1 = self.alphabet.find(lettre)
           num2 = num1+self.cle
           if num2 >= 26:
               num2 = num2-26
           if num2 < 0:
               num2 = num2+26
           nouvelle_lettre = self.alphabet[num2]
           return nouvelle_lettre
   ```
   On rappelle que la méthode `str.find(lettre)` renvoie l'indice (index) de la
   `lettre` dans la chaine de caractères `str`.

   **Représenter** le résultat d'éxécution du code Python suivant :

   ```{code-block} pycon
   >>> code1 = CodeCesar(3)
   >>> code1.decale('A))
   ...
   >>> code1.decale('X')
   ...
   ```
2. La méthode de chiffrement du « code de César » consiste à décaler les
   lettres du message dans l’alphabet d'un nombre de rangs fixé par la clé.
   Par exemple, avec la clé 3, toutes les lettres sont décalées de 3 rangs
   vers la droite : le A devient le D, le B devient le E, etc.

   **Ajouter** une méthode cryptage(self, texte) dans la classe CodeCesar
   définie à la question précédente, qui reçoit en paramètre une chaîne de
   caractères (le message à crypter) et qui retourne une chaîne de
   caractères (le message crypté).

   Cette méthode cryptage(self, texte) doit crypter la chaîne texte avec la
   clé de l'objet de la classe CodeCesar qui a été instancié. 

   Exemple :
   ```{code-block} pycon
   >>> code1 = CodeCesar(3)
   >>> code1.cryptage("NSI")
   'QVL'
   ```

3. **Écrire** une fonction qui :
   * prend en argument la clef de chiffrement et le message à chiffrer ;
   * crée un objet de la classe `CodeCesar` ;
   * renvoie le texte chiffré.

4. On ajoute la méthode `transforme(texte)` à la classe `CodeCesar` :

   ```{code-block} python
   def transforme(self, texte):
       self.cle = -self.cle
       message = self.cryptage(texte)
       self.cle = - self.cle
       return message
   ```
   On exécute la ligne suivante dans une console
       `CodeCesar(10).transforme("PSX")`

   **Que va-t-il s'afficher** ? **Expliquer** votre réponse.
````

````{exercise}
<div style="text-align: center; font-weight: bold;">Une fonction pour convertir une adresse IP en décimal pointé en
notation binaire</div>

*Notion abordée : listes*

On dispose de la fonction `dec_bin` :
* qui prend en paramètre d'entrée un nombre entier compris entre 0 et 255 ;
* qui renvoie une **_liste_** de 8 éléments correspondant à la conversion du
nombre en écriture décimale en notation binaire. Chaque élément de cette
liste est de type entier.

```{warning}
Il n'est pas demandé d'écrire la fonction `dec_bin`
```

Exemples d'exécution de la fonction dec_bin:
* `dec_bin(10)` renvoie la liste `[0,0,0,0,1,0,1,0]`
* `dec_bin(255)` renvoie la liste `[1,1,1,1,1,1,1,1]`

**Écrire** une fonction en langage Python que l'on appellera IP_bin qui:
* *prend* en paramètre d'entrée une liste de 4 entiers compris entre 0 et 255
correspondant à l'adresse IP en notation décimale
* *renvoie* une liste de 4 listes correspondant à l'adresse IP en notation
binaire.

La fonction `IP_bin` peut faire appel à la fonction `dec_bin`.

Exemple d'exécution de la fonction IP_bin :
```{code-block} pycon
>>> IP_bin([192,168,0,1])
[[1,1,0,0,0,0,0,0],[1,0,1,0,1,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,1]]
```

````

`````{exercise}

<div style="text-align: center; font-weight: bold;">Chiffrement XOR</div>

*Thèmes abordés : conversion décimal/binaire, table de vérité, codage des
caractères*

L’objectif de l’exercice est d’étudier une méthode de cryptage d’une chaîne de
caractères à l’aide du codage ASCII et de la fonction logique XOR.

1. Le nombre 65, donné ici en écriture décimale, s’écrit 01000001 en
   notation binaire. En détaillant la méthode utilisée, **donner**
   l’écriture binaire du nombre 89.

2. La fonction logique OU EXCLUSIF, appelée XOR et représentée par le symbole
   ⊕, fournit une sortie égale à 1 si l’une ou l’autre des deux entrées vaut 1 mais
   pas les deux.

    | $E_1$ | $E_2$ | $E_1$ ⊕ $E_2$ |
    | ----- | ----- | ------------- |
    | 0     | 0     | 0             |
    | 0     | 1     | 1             |
    | 1     | 0     | 1             |
    | 1     | 1     | 0             |

    Si on applique cette fonction à un nombre codé en binaire, elle opère bit à
    bit :
    ```{code-block} text
        1100
     ⊕
        1010
    _________
     =  0110
    ```

    **Poser et calculer** l'opération `11001110 ⊕ 01101011`

3. On donne, ci-dessous, un extrait de la table ASCII qui permet d’encoder
   les caractères de A à Z.

   On peut alors considérer l’opération XOR entre deux caractères en
   effectuant le XOR entre les codes ASCII des deux caractères. Par exemple
   : `'F' XOR 'S'` sera le résultat de `01000110 ⊕ 01010011`.

   ````{grid}
   ```{grid-item}
   | Code ASCII décimal | Code ASCII binaire | Caractère |
   | --                 | --                 | --        |
   | 65                 | 01000001           | A         |
   | 66                 | 01000010           | B         |
   | 67                 | 01000011           | C         |
   | 68                 | 01000100           | D         |
   | 69                 | 01000101           | E         |
   | 70                 | 01000110           | F         |
   | 71                 | 01000111           | G         |
   | 72                 | 01001000           | H         |
   | 73                 | 01001001           | I         |
   | 74                 | 01001010           | J         |
   | 75                 | 01001011           | K         |
   | 76                 | 01001100           | L         |
   | 77                 | 01001101           | M         |
   ```
   ```{grid-item}
   | Code ASCII décimal | Code ASCII binaire | Caractère |
   | --                 | --                 | --        |
   | 78                 | 01001110           | N         |
   | 79                 | 01001111           | O         |
   | 80                 | 01010000           | P         |
   | 81                 | 01010001           | Q         |
   | 82                 | 01010010           | R         |
   | 83                 | 01010011           | S         |
   | 84                 | 01010100           | T         |
   | 85                 | 01010101           | U         |
   | 86                 | 01010110           | V         |
   | 87                 | 01010111           | W         |
   | 88                 | 01011000           | X         |
   | 89                 | 01011001           | Y         |
   | 90                 | 01011010           | Z         |
   ```
   ````
   On souhaite mettre au point une méthode de chiffrement à l'aide de la
   fonction XOR.

   Pour cela, on dispose d'un message à chiffre et d'une clef de chiffrement
   _de même longueur_ que ce message. Le message et la clef sont composés
   uniquement des caractères du tableau ci-dessus et on applique la fonction
   XOR caractère par caractère entre les lettres du message à chiffrer et
   les lettres de la clef de chiffrement.

   Par exemple, voici le chiffremnt du mot ALPHA à l'aide de la clef YAKYA

   ````{grid}
   ```{grid-item}
   Message à chiffrer

   Clef

    

   Message chiffré
   ```
   ```{grid-item}
   | A       |       L | P       | H | A |
   | --      | --      | --      | - | - |
   | Y       |       A | K       | Y | A |
   | ↓       |       ↓ | ↓       | ↓ | ↓ |
   | A XOR Y | L XOR A | P XOR K | … | … |
   ```
   ````
   **Écrire** une fonction `xor_chiffre(message, clef)` qui prend en
   paramètres deux chaines de caractères et qui renvoie la liste des entiers
   correspondant au message chiffré.

   ```{tip}
   * On pourra utiliser la fonction native du langage Python `ord(c)` qui
   prend en paramètre un caractère `c` et qui renvoie un nombre représentant
   le code ASCII du caractère `c`.
   * On considère également que l'on dispose d'une fonction écrite `xor(n1,
       n2)` qui prend en paramètre deux nombres `n1` et `n2` et qui renvoie
   le résultat de `n1 ⊕ n2`.
   ```
4. On  souhaite maintenant générer une clé de la taille du message à partir
   d’un mot quelconque. On considère que le mot choisi est plus court que le
   message, il faut donc le reproduire un certain nombre de fois pour créer
   une clé de la même longueur que le message.

   Par exemple, si le mot choisi est YAK pour crypter le message ALPHABET,
   la clé sera YAKYAKYA.

   **Créer** une fonction `generer_clef(mot, n)` qui renvoie la clef de
   longueur `n` à partir de la chaîne de caractères `mot`.

5. **Recopier** et **compléter** la table de vérité  ($E_1$ ⊕ $E_2$) ⊕ $E_2$.

    | $E_1$ | $E_2$ | $E_1$ ⊕ $E_2$ | ($E_1$ ⊕ $E_2$) ⊕ $E_2$ |
    | ----- | ----- | ------------- | ----------------------- |
    | 0     | 0     | 0             |                         |
    | 0     | 1     | 1             |                         |
    | 1     | 0     | 1             |                         |
    | 1     | 1     | 0             |                         |

    À l'aide de ce résultat, **proposer** une démarche pour déchiffrer un
    message chiffré.

`````
