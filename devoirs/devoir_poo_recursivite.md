# Devoir N<sup>o</sup> 1

## Récursivité

````{exercise}
On considère le code suivant :
```{code-block} pascal
fonction f(n)
    SI n = 1 ALORS
        renvoyer 0
    SINON
        renvoyer 1 + f(n // 2)
    FINSI
```
````

````{exercise}
Écrire une fonction récursive qui donne le maximum d'un tableau de nombre entiers écrit sous la forme d'une liste Python.
```{code-block} python
def maxi(T: list) -> int:
    pass
```
````

````{exercise}
1. Donner la définition d'une fonction récursive.
2. Quel est le point de vigilance d'une fonction récursive ?
````

## Programmation orienté objet

`````{exercise}
Proposer une classe `Chaine` qui stocke une chaine de caractères sous la forme d'une liste et qui soit muable.

````{note}
La classe doit passer les tests suivants :
```{code-block} python
chaine = Chaine('rale')
assert chaine[1] == 'a'
assert chaine.append('r') == Chaine('raler')
chaine[1] = 'o'
assert chaine[1] == 'o'
```
On peut définir les méthodes spéciales suivantes :
+ `Chaine.__getitem__(self, indice)` pour récupérer un indice ;
+ `Chaine.__setitem__(self, indice, valeur)` pour affecter une nouvelle valeur à un indice.
````
`````
