---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Correction de la fonction Mystère

```{code-cell} ipython
:tags: [remove-cell]
!pip install tutormagic
```

```{code-cell} ipython
:tags: [remove-cell]
%load_ext tutormagic
```

Rappel de l'énoncé :
On donne une fonction `Mystere` dont le code est rappelé plus loin. Il faut
étudier cette fonction, ces propriétés et le résultat de son application.

::::{warning}
L'iframe de [https://Pythontutor.com](https://pythontutor.com) nécessite
d'autoriser le code (js, xhr et iframe) depuis pythontutor.
On peut ajouter
```text
frama.io pythontutor.com frame allow
frama.io pythontutor.com script allow
frama.io pythontutor.com xhr allow
```
à la configuration d'uMatrix.
::::

On considère la fonction suivante.
```{code-block} python
def Mystere(a, b):
    if b == 1:
        return a
    else:
        return a*Mystere(a, b - 1)

Mystere(5, 4)
```


```{code-cell} ipython
:tags: [remove-input, hide-ouptut]
%%tutor -l python3 -s
def Mystere(a, b):
    if b == 1:
        return a
    else:
        return a*Mystere(a, b - 1)

Mystere(5, 4)
```

Le code ci dessus montre assez clairement que, dans une fonction récurisve
avec plusieurs variables, seule la variable «de récursivité» est modifiée.
Ici `a` n'est pas réaffectée pendant le processus de calcul de la valeur de
retour : `a` est toujours une référence vers 5, et c'est le résultat de
`Mystere(5, ⋅ )` qui évolue avec les appels successifs.
