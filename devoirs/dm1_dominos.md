# Devoir à la maison N<sup>o</sup> 1

```{info}
Le but de ce devoir à la maison est de travailler la récursivité sur un
problème difficile à résoudre en mathématiques. On va donc aborder le
problème d'une façon algorihtmique.

**Compétences**
+ Modéliser un problème par un algorithme récursif
+ Écrire une fonction récursive.
+ Vérifier qu'un programme satisfait à un jeu de test
```

```{exercise}
Dessiner les 3 pavages possibles par des dominos 2×1 d'un rectangle de 3×2.
```

```{exercise}
Dessiner une solution de pavage par des dominos 2×1 d'un rectangle de 3×8.
```

```{exercise}
1. On suppose qu'une partie du rectangle a été remplie. Dessiner les 3
   possibilités qui permettent de compléter.
2. Pour la première possibilité, indiquer combien de dominos sont
   nécessaires de façon à compléter pour remplir, ou pour donner une des
   deux autres situations.
3. Indiquer comment on peut obtenir les deux situations similaires.
```

````{exercise}
Écrire une fonction récursive `compter_arrangements_dominos_3xn(n: int) ->
int` qui renvoie le nombre de dominos nécessaires pour paver le domaine.
Cette fonction devra satisfaire le jeu de test suivant :
```{code-block} python
def test_nombre_dominos() -> None:
    assert compter_arrangements_dominos_3xn(2) == 3
    assert compter_arrangements_dominos_3xn(8) == 153
    assert compter_arrangements_dominos_3xn(12) == 2131
```
````
