---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.0
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

+++ {"deletable": false, "editable": false, "run_control": {"frozen": true}}

<div style="width:100%;border-collapse:separate;border:solid 2px;border-radius:14px;border-color:pink">
    <div style="display:flex;">
        <div style="display:inline;width:5%;"></div>
        <div style="display:inline;width:15%;">NSI</div>
        <div style="display:inline;width:15%;"></div>
        <div style="display:inline;width:70%;text-align:right;"><b></b></div>
        <div style="display:inline;width:10%;">pablo Neruda</div>
    </div>
    <div  style="display: flex;background-color:#EEE">
        <h1 style="width:100%;text-align:center;">Le modèle relationnel</h1>
    </div>
    <div  style="display: flex;">
        <p style="width:100%;text-align:left; color:black"> 	Bases de données         </p>
    </div>
</div>

+++ {"deletable": false, "editable": false, "run_control": {"frozen": true}}

<center>
<!--style="text-align:center; background-color:#FFFFE0; border: 1px solid black;"-->
<table >
    <tr style="background-color: #17818e;">
        <th style="text-align: center;  color:#FFFFFF;  border: 1px solid black;">Contenus</th>
        <th style="text-align: center;  color:#FFFFFF;  border: 1px solid black;"> Capacités attendues </th>
        <th style="text-align: CENTER;  color:#FFFFFF;  border: 1px solid black;">  Commentaires </th> 
    </tr>  
    <tr>
        <td  style="text-align: left; border: 1px solid black;">Modèle relationnel : relation, attribut, domaine, clef primaire, clef étrangère, schéma relationnel.</td>
        <td style="text-align: left; border: 1px solid black;">Identifier les concepts définissant le modèle relationnel.</td>
        <td style="text-align: left; border: 1px solid black;">Ces concepts permettent d’exprimer les contraintes d’intégrité (domaine, relation et référence).</td>       
    </tr>
    <tr>
        <td  style="text-align: left; border: 1px solid black;">Base de données relationnelle. </td>
        <td style="text-align: left; border: 1px solid black;">Savoir distinguer la structure d’une base de données de son contenu. <br />
Repérer des anomalies dans le schéma d’une base de données. La structure est un ensemble de schémas relationnels qui respecte les contraintes du modèle relationnel.</td>
        <td style="text-align: left; border: 1px solid black;">Les anomalies peuvent être des redondances de données ou des anomalies d’insertion, de suppression, de mise à jour.<br />
On privilégie la manipulation de données nombreuses et réalistes.</td>       
    </tr>
<!--<tr>
        <td  style="text-align: left; border: 1px solid black;"></td>
        <td style="text-align: left; border: 1px solid black;"></td>
        <td style="text-align: left; border: 1px solid black;"></td>       
    </tr>
-->
</table>
     </center>
     

```{code-cell} ipython
# Cette leçon correspond aux du début jusqu'à la 7°  minutes de la vidéo.
from IPython.display import YouTubeVideo

YouTubeVideo('pqoIBiM2AvE', width=800, height=300)
```

+++ {"deletable": false, "editable": false, "run_control": {"frozen": true}}

# Un peu d'histoire

Le modèle relationnel est une manière de modéliser les relations existantes entre plusieurs informations, et de les ordonner entre elles. Cette modélisation qui repose sur des principes mathématiques mis en avant par <b>E.F. Codd</b> (laboratoire de recherche d’IBM) est souvent implémentée dans une base de données.


<!--
    <tr>
        <th style="text-align: center; background-color: #ff7f00; color:#FFFFFF;  border: 1px solid black;"></th> 
        <td style="text-align: left; background-color:#FFFFFF  ; color:#000000;  border: 1px solid black;"></td>
    </tr> 
-->



<center>
<table>
    <tr>
        <td style="text-align: center; background-color: #ff7f00; color:#FFFFFF;  border: 1px solid black;">1964</td> 
        <td style="text-align: left; background-color:#FFFFFF  ; color:#000000;  border: 1px solid black;">Apparition du terme <b>database</b>.</td>
    </tr>  
    <tr>
        <td style="text-align: center; background-color: #ff7f00; color:#FFFFFF;  border: 1px solid black;">1968</td> 
        <td style="text-align: left; background-color:#FFFFFF  ; color:#000000;  border: 1px solid black;"><b>Dick Pick</b> crée un système d'exploitation contenant un <b>SGBD</b>.</td>
    </tr>     
    <tr>
        <td style="text-align: center; background-color: #ff7f00; color:#FFFFFF;  border: 1px solid black;">1970</td> 
        <td style="text-align: left; background-color:#FFFFFF  ; color:#000000;  border: 1px solid black;"> <b>Edgar Franck Cood</b> pose les bases du modèle relationnel</td>
    </tr>
    <tr>
<td style="text-align: center; background-color: #ff7f00; color:#FFFFFF;  border: 1px solid black;">1974</td>
        <td style="text-align: left; background-color:#FFFFFF  ; color:#000000;  border: 1px solid black;"> Création du langage <b>SQL</b></td>
    </tr>
    <tr>
        <td style="text-align: center; background-color: #ff7f00; color:#FFFFFF;  border: 1px solid black;">1975</td> 
        <td style="text-align: left; background-color:#FFFFFF  ; color:#000000;  border: 1px solid black;"><b>Peter Chen</b> invente le modèle <b>Entité-Association</b>.</td>
    </tr>      
    <tr>
<td style="text-align: center; background-color: #ff7f00; color:#FFFFFF;  border: 1px solid black;">1979</td>
        <td style="text-align: left; background-color:#FFFFFF  ; color:#000000;  border: 1px solid black;">Création du premier système de gestion de base de données <b>Oracle</b></td>
    </tr>
    <tr>
        <td style="text-align: center; background-color: #ff7f00; color:#FFFFFF;  border: 1px solid black;">1980</td>
        <td style="text-align: left; background-color:#FFFFFF  ; color:#000000;  border: 1px solid black;">Le volume mondial de données stockées est estimé à 10<sup>18</sup> octets</td>
    </tr>
    <tr>
        <td style="text-align: center; background-color: #ff7f00; color:#FFFFFF;  border: 1px solid black;">1990</td>
        <td style="text-align: left; background-color:#FFFFFF  ; color:#000000;  border: 1px solid black;">Le volume mondial de données stockées est estimé à 10<sup>19</sup> octets</td>
    </tr>
    <tr>
        <td style="text-align: center; background-color: #ff7f00; color:#FFFFFF;  border: 1px solid black;">1995</td>
        <td style="text-align: left; background-color:#FFFFFF  ; color:#000000;  border: 1px solid black;">Première version du langage MySQL</td>
    </tr>
    <tr>
        <td style="text-align: center; background-color: #ff7f00; color:#FFFFFF;  border: 1px solid black;">1997</td> 
        <td style="text-align: left; background-color:#FFFFFF  ; color:#000000;  border: 1px solid black;">Apparition du terme <b>Big Data</b></td>
    </tr>
    <tr>
        <td style="text-align: center; background-color: #ff7f00; color:#FFFFFF;  border: 1px solid black;">2000</td> 
        <td style="text-align: left; background-color:#FFFFFF  ; color:#000000;  border: 1px solid black;">Apparition du <b>Cloud Computing</b></td>
    </tr>     
    <tr>
        <td style="text-align: center; background-color: #ff7f00; color:#FFFFFF;  border: 1px solid black;">2002</td>
        <td style="text-align: left; background-color:#FFFFFF  ; color:#000000;  border: 1px solid black;">Le volume mondial de données stockées est estimé à 10<sup>20</sup> octets</td>
    </tr>
    <tr>
        <td style="text-align: center; background-color: #ff7f00; color:#FFFFFF;  border: 1px solid black;">2010</td>
        <td style="text-align: left; background-color:#FFFFFF  ; color:#000000;  border: 1px solid black;">Le volume mondial de données stockées est estimé à 10<sup>21</sup> octets</td>
    </tr>
    <tr>
        <td style="text-align: center; background-color: #ff7f00; color:#FFFFFF;  border: 1px solid black;">2014</td>
        <td style="text-align: left; background-color:#FFFFFF  ; color:#000000;  border: 1px solid black;">Le volume mondial de données stockés est estimé à 10<sup>22</sup> octets</td>
    </tr>
    </table>
</center>

+++

##  Les limites des outils traditionnels



Pour regrouper des données on dispose par exemple de logiciel comme open office (tableur) ou excel, mais cet outil s'il est adapté par exemple à des données sur les élèves de la classe  mais il est déconseillé d'utiliser ce logiciel  pour de nombreuses donnée.

**au Royaume-Uni en 2020,  16000  données ont été perdue [pour le suivie  des  cas de Covid-19  covid perte donnée à cause du choix d'un outil non adapté](https://www.numerama.com/politique/653217-16-000-anglais-malades-du-covid-ont-ete-oublies-a-cause-dune-feuille-excel-trop-pleine.html)**

Nous avons vu en classe de première que le fichier csv pouvait être utilisé pour de nombreuses données mais ce système  peut aussi être limité dans de nombreux cas.


<div class="alert alert-info" style="border-left:15px solid #31708f;">
    <b>Définitions</b>: 
    <ul>
        <li>Une <b>base de données</b> (<b>BDD</b>) représente un ensemble ordonné de données dont l'organisation est régie par un modèle de données. </li>
        <li>Le <b>Système de Gestion de Bases de Données</b> (<b>SGBD</b>) peut être vu comme le logiciel qui prend en charge la structuration, le stockage, la mise à jour et la maintenance des données.  
C'est en fait l'interface entre la base de données et les multiples utilisateurs (ou leurs programmes).</li>
    </ul>
</div>

<div class = "alert  alert-block alert-danger" style="border-left: 15px solid #a94442;">
    <b>Histoire</b>: 

Quasiment toutes les bases de données que nous utilisons aujourd'hui sont basées sur les travaux d'**Edgar F. Codd (1970)**. C'est le point essentiel dans l'histoire des bases de données.
    </div>


<!--
<center>
<table>
<tr>
    <th>titre</th><th>nom  auteur</th><th> prenom auteur </th><th>editeur </th><th>langue </th><th>annee publication </th><th>reference</th>
<tr>
<tr>
    <td>1984</td><td>Orwell </td><td style="text-align: left;">George </td><td>J’ai Lu </td><td>anglais </td><td>1949 </td><td>120125</td>
</tr> 
<tr>
<td>Dune </td><td>Herbert </td><td style="text-align: left;">Frank </td><td>Pocket </td><td>anglais </td><td>1965 </td><td>02154</td>
</tr>
<tr>
<td>Fondation </td><td>Asimov </td><td style="text-align: left;">Isaac </td><td>Pocket </td><td>anglais </td><td>1951 </td><td>95321</td>
</tr>
<tr>
<td>Le meilleur des mondes </td><td>Huxley </td><td style="text-align: left;">Aldous </td><td>J’ai Lu </td><td>anglais </td><td>1931 </td><td>1254</td>
</tr>
<tr>
<td>Fahrenheit 451 </td><td>Bradbury </td><td style="text-align: left;">Ray </td><td>J’ai Lu </td><td>anglais </td><td>1953 </td><td>2365</td>
</tr>
<tr>
<td>Ubik </td><td>K. Dick </td><td style="text-align: left;">Philip </td><td>Denoël </td><td>anglais </td><td>1969 </td><td>4523669</td>
</tr>
<tr>
<td>Chroniques martiennes </td><td>Bradbury </td><td style="text-align: left;">Ray </td><td>Denoël </td><td>anglais </td><td>1950 </td><td>24568</td>
</tr>
<tr>
<td>La nuit des temps </td><td>Barjavel </td><td style="text-align: left;">René </td><td>Folio SF </td><td>français </td><td>1968 </td><td>724867</td>
</tr>
<tr>
<td>Blade runner </td><td>K Dick </td><td style="text-align: left;">Philip </td><td>J’ai Lu </td><td>anglais </td><td>1968 </td><td>24967</td>
<tr>
<td>Les robots </td><td>Asimov </td><td style="text-align: left;">Isaac </td><td>Folio SF </td><td>anglais </td><td>1950 </td><td>3588</td>
</tr>

<tr>
<td>La planète des singes </td><td>Boulle </td><td style="text-align: left;">Pierre </td><td> Folio SF </td><td>francais </td><td>1963 </td><td>89547</td>
</tr>

<tr>
<td>Ravage </td><td>Barjavel </td><td style="text-align: left;">René</td><td> Folio SF </td><td>français </td><td>1943 </td><td>96541</td>
</tr>

<tr>
<td>Le maître du haut chateau </td><td>K.Dick </td><td style="text-align: left;">Philip </td><td>Denoël </td><td>anglais </td><td>1962 </td><td>34587</td>
</tr>

<tr>
<td>Le monde des A </td><td>Van Vogt </td><td style="text-align: left;">Alfred Elton </td><td>Folio SF </td><td>anglais </td><td>1945 </td><td>26974</td>
</tr>

<tr>
<td>La fin de l’éternité </td><td>Asimov</td><td style="text-align: left;">Isaac </td><td>Folio  SF </td><td>anglais </td><td>1955 </td><td>885236</td>
</tr>
</table>
</center>
-->

+++ {"deletable": false, "editable": false, "run_control": {"frozen": true}}

## Conception des bases de données relationnelles

```{code-cell} ipython
from IPython.display import HTML,IFrame
IFrame("https://videotheque.univ-poitiers.fr/embed.php?id=hbk4f4jqhmrcqcfw7r43&link=cj6jz7vsqp6mt55qwrdtz7q78jp5r5",
       width=800, height=400)
```

+++ {"deletable": false, "editable": false, "run_control": {"frozen": true}}

https://videotheque.univ-poitiers.fr/embed.php?id=hbk4f4jqhmrcqcfw7r43&link=cj6jz7vsqp6mt55qwrdtz7q78jp5r5

+++ {"deletable": false, "editable": false, "run_control": {"frozen": true}}

La modélisation se réalise en trois étapes principales qui correspondent à trois niveaux d'abstraction différents :
-  Niveau conceptuel : représentation de la base de données indépendamment de toute considération informatique
- Niveau logique : adaptation du schéma conceptuel en tableaux à deux dimensions 
- Niveau physique : implémentation informatique sur un système de gestion de bases de données relationnelles

<img  src='https://megamaths.hd.free.fr/static/bdd1.svg' alt=''/>

+++ {"deletable": false, "editable": false, "run_control": {"frozen": true}}

## Le modèle  Entité-Association
Ce modèle permet de distinguer les entités qui constituent la base de données et les associations entre ces entités.

+++

### **Entités, attributs et identifiants**  

<div class="alert alert-warning" style="border-left:15px solid #eba83a;">
<ul>
  <li>On appelle <b>entité</b> un objet (un événement, un lieu, une personne, ...) pouvant être identifié sans ambiguïté.</li>
  <li>Chaque entité est donc unique et est décrite par un ensemble de propriétés appelées <b>attributs</b>.</li>
  <li> Un ou plusieurs attributs permettent d'identifier de manière unique l'entité, on parle alors d'<b>identifiant</b> (ou de <b>clé</b>).</li>
</ul>
</div>

On peut représenter une entité par le schéma suivant :

<center>
  <table>
    <tr><th style="text-align:center; background-color:#FFFFE0; border: 1px solid black;">Nom de l'entité<br />identifiable</th></tr>
    <tr><td  style="text-align:center; background-color:#FFFFFF; border: 1px solid black;">attribut 1</td></tr>
    <tr><td  style="text-align:center; background-color:#FFFFFF; border: 1px solid black;">attribut 2</td></tr>
    <tr><td  style="text-align:center; background-color:#FFFFFF; border: 1px solid black;">.....</td></tr>
  </table>
</center>
    
<div class="alert alert-warning" style="border-left:15px solid #eba83a;">
    Un <b>attribut</b> est désigné par :
<ul>
    <li>Un <b>nom</b></li>
    <li>Une valeur de <b>type</b> défini (entier, chaîne de caractères, date...)</li>
</ul>
    </div>

Prenons l'exemple d'une entité « *Auteur* ».    
Un auteur est bien identifié de manière unique par son numéro de sécurité sociale (*Num_securite_sociale*) et est caractérisé par un nom (Nom_auteur), un prénom (*Prenom_auteur*) et une date de naissance (*Date_naissance*).

<center>
  <table>
      <tr><th style="text-align:center; background-color:#FFFFE0; border: 1px solid black;">Auteur</th><th style="text-align:left; background-color:#FFFFFF; border: none;"> </th></tr>
      <tr><td style="text-align:center;background-color:#FFFFFF; border: 1px solid black;">Num_securite_sociale</td><td style="text-align:left; background-color:#FFFFFF; border: none;">de type entier</td></tr>
    <tr><td  style="text-align:center; background-color:#FFFFFF;  border: 1px solid black;">Nom_autheur</td><td  style="text-align:left; background-color:#FFFFFF; border: none;">de type chaîne de caractère</td></tr>
    <tr><td  style="text-align:center; background-color:#FFFFFF; border: 1px solid black;">Prenom_autheur</td><td  style="text-align:left;">de type chaîne de caractère</td></tr>
    <tr><td  style="text-align:center; background-color:#FFFFFF; border: 1px solid black;">Date_naissance</td><td  style="text-align:left; background-color:#FFFFFF; border: none;">de type date</td></tr>
  </table>
</center>

+++ {"deletable": false, "editable": false, "run_control": {"frozen": true}}

Une entité peut avoir une ou plusieurs <b>occurrences</b> (et parfois même aucune). Pour illustrer cela, voici un exemple de 3 occurrences de l'entité « Auteur ».

<center>
  <table>
<tr>
    <th style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Num_securite_sociale </th>
    <th style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Nom_auteur</th>
    <th style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Prenom_auteur</th>
    <th style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Date_naissance</th></tr>
<tr>
    <td style="text-align:center; background-color:#FFFFFF; border:1px solid black;">182086926825812 </td>
    <td style="text-align:center; background-color:#FFFFFF; border:1px solid black;">SIMPSON</td>
    <td style="text-align:center; background-color:#FFFFFF; border:1px solid black;">Homer</td>
    <td style="text-align:center; background-color:#FFFFFF; border:1px solid black;">25/09/1962</td>
      </tr>

<tr><td style="text-align:center; background-color:#FFFFFF; border:1px solid black;">274037511510792 </td>
    <td style="text-align:center; background-color:#FFFFFF; border:1px solid black;">SIMPSON</td>
    <td  style="text-align:center; background-color:#FFFFFF; border: 1px solid black;">Lucie </td>
    <td style="text-align:center; background-color:#FFFFFF; border:1px solid black;">11/03/2004</td></tr>

<tr>
    <td  style="text-align:center; background-color:#FFFFFF; border: 1px solid black;"> 163221382362658 </td>
    <td  style="text-align:center; background-color:#FFFFFF; border: 1px solid black;"> FLANDERS</td>
    <td  style="text-align:center; background-color:#FFFFFF; border: 1px solid black;">Ned</td>
    <td  style="text-align:center; background-color:#FFFFFF; border: 1px solid black;">05/11/1963</td></tr>
        </table>
</center>

<!--
<div class = "alert alert-success"><b>Remarque</b>:  
    
Ces occurrences sont appelées **tuples** ou lignes.
    
On devrait plutôt parler d'**entités-types**, (les entités étant en fait des instances d'entités-types) mais  on gardera le terme **entités** dans la suite pour simplifier.
</div>
-->

+++

### Associations
Une association définit un lien sémantique entre des entités. Elle permet de traduire une partie des règles de gestion qui n'ont pas été satisfaites par la simple définition des entités.

<div class="alert alert-warning" style="border-left:15px solid #eba83a;">
Une association est caractérisée par :
<ul>
    <li>  Un <b>nom</b> : généralement on utilise un verbe définissant le lien entre les entités</li>
    <li> Deux <b>cardinalités</b> : elles sont présentes sur les 2 extrémités du lien. Chaque cardinalité est un couple de valeurs (mini, maxi) qui traduisent 2 règles de gestion (une par sens)</li>
</ul>
</div>

<div class="alert alert-success" style="border-left: 15px solid #3c763d;">
    <b>Remarque</b>:  Une association peut aussi avoir des attributs.
</div>

On peut représenter une association par le schéma suivant :

<img alt=""  src="https://megamaths.hd.free.fr/static/bdd/bdd2.svg"  />

Dans l'exemple suivant, on définit une association « Être originaire de » entre les entités « Auteur » et « Pays ».  
Ce lien possède 2 **cardinalités** (1,1) et (0,n) qui traduisent les 2 règles de gestion suivantes :
- Un auteur est originaire au minimum et au maximum d’un seul pays (sens « Auteur » vers « Pays »)
- Dans un pays, il peut y avoir au minimum aucun auteur originaire de ce pays et au maximum plusieurs auteurs. (sens « Pays » vers « Auteur »)

<img alt=""  src="https://megamaths.hd.free.fr/static/bdd/bdd3.svg"  />

<div class="alert alert-success" style="border-left: 15px solid #3c763d;">
    <b>Remarque</b>:  Les cardinalités les plus répandues sont les suivantes : 0,N ; 1,N ; 0,1 ; 1,1.</div>
    
À partir des entités et associations, il est possible d'élaborer le schéma conceptuel des données pour une gestion de livres comme le montre l'exemple ci-dessous :

<img alt=""  src="https://megamaths.hd.free.fr/static/bdd/bdd4.svg"  />

<div class="alert alert-success" style="border-left: 15px solid #3c763d;">
    <b>Remarque</b>: Une situation à modéliser peut avoir plusieurs schémas différents, chaque modèle présentant des avantages et des inconvénients.</div>

+++

<div class="alert alert-warning" style="border-left:15px solid #eba83a;">
    <b>Définition</b>: <br />  
<b>MCD</b> (Modèle Conceptuel des Données) est la structure des entités, des associations et des attributs.
</div>

+++

## Le modèle relationnel

Dans le modèle relationnel, les entités et les associations du schéma conceptuel sont transformées en tableaux à deux dimensions appelés **relations**.

<div class="alert alert-warning" style="border-left:15px solid #eba83a;">
    <b>Définition</b>: <br />  
    <b>MLD</b> (Modèle Logique des Données)est la liste des relations entre les entités avec leurs attributs.
</div>


### Transformation d'une entité en relation 
**Toute entité devient relation** comme le montre la figure suivante :

+++ {"deletable": false, "editable": false, "run_control": {"frozen": true}}

<center>
<table>
    <tr style="background-color:#FFFFFF; ">
        <td>
    <table>
      <tr><th style="text-align:center; background-color:#FFFFE0; border: 1px solid black;">Auteur</th></tr>
      <tr><td style="text-align:center;background-color:#FFFFFF; border: 1px solid black;">Num_securite_sociale</td></tr>
      <tr><td  style="text-align:center; background-color:#FFFFFF;  border: 1px solid black;">Nom_auteur</td>      </tr>
      <tr><td  style="text-align:center; background-color:#FFFFFF; border: 1px solid black;">Prenom_auteur</td></tr>
      <tr><td  style="text-align:center; background-color:#FFFFFF; border: 1px solid black;">Date_naissance</td></tr>
   </table>
   </td>
    <td>
       <img  alt=''  src='https://megamaths.hd.free.fr/static/arrow-right-blue.svg' width='60px'>
        </td>
        <td>
          <table>
      <tr><th style="text-align:center; background-color:#FFFFE0; border: 1px solid black;">Auteur</th></tr>
      <tr><td style="text-align:center;background-color:#FFE0FF; border: 1px solid black;"><img  alt=''  src='images/key.svg' style="float:left; width:5px" />CP&nbsp; &nbsp;  Num_securite_sociale</td></tr>
    <tr><td  style="text-align:center; background-color:#FFFFFF;  border: 1px solid black;">Nom_auteur</td>      </tr>
    <tr><td  style="text-align:center; background-color:#FFFFFF; border: 1px solid black;">Prenom_autheur</td></tr>
    <tr><td  style="text-align:center; background-color:#FFFFFF; border: 1px solid black;">Date_naissance</td></tr>
  </table>
        </td>
    </tr>
</table>
</center>

+++

<div class="alert alert-success" style="border-left: 15px solid #3c763d;">
    <b>Remarques</b>:<br />
 <ul>
    <li> L'identifiant de l'entité est la <b>clé primaire (Primary Key)</b> de la relation. Il est représenté sur la figure ci-dessus par une clé et la dénomination <b>CP</b> (ou <b>PK</b>).</li>
     <li> L'ensemble des valeurs possibles d'un attribut définit un domaine.</li>
  </ul>
</div>
    
On trouve également la notation textuelle: <span style="background-color:#FFFFE0;"><b>NomRelation</b>(<u>identifiant</u>, attribut1,attribut2...)</span>.  

On peut par  exemple noter la relation « Auteur » par :
<center>
    <b>Auteur</b>(<u>Num_securite_sociale</u>, Nom_auteur, Prenom_auteur, Date_naissance)
  </center>

<div class="alert alert-success" style="border-left: 15px solid #3c763d;">
    <b>Remarques</b>:<br />

<ul>
    <li>Le nom de la relation est en gras</li>
    <li> La clé primaire est soulignée</li>
    <li>Il y a un # devant la clé étrangère</li>
 </ul>
    </div>

+++

### Transformation d'une association en relation

Il existe 2 cas :
- Associations possédant au moins une cardinalité (0,1) ou (1,1)
- Associations sans cardinalités (0,1) ou (1,1)

#### Associations possédant au moins une cardinalité (0,1) ou (1,1)

Prenons l'exemple de l'association « Auteur/Pays » :

<img  alt=''  src='https://megamaths.hd.free.fr/static/bdd/bdd5.svg'  />
<img  alt=''  src='https://megamaths.hd.free.fr/static/arrow-down-blue.svg' width='40px' />
<img  alt=''  src='https://megamaths.hd.free.fr/static/bdd/bdd6.svg'  />

<div class="alert alert-success" style="border-left: 15px solid #3c763d;">
    <b>Remarques</b>:  
<ul>
    <li>On a ajouté un attribut <i>Nom_pays</i> dans la relation <i>Auteur</i>, cet attribut correspond à la clé primaire de la relation Pays.   On dit que c'est une <b>clé étrangère (Foreign Key)</b>.<br />
        Dans le schéma ci-dessus, elle est également représentée par une clé avec le préfixe <b>FK</b>.</li>
  <li>On lie ces deux attributs entre eux.</li>
    </ul>
    </div>


<div class="alert alert-warning" style="border-left:15px solid #eba83a;">

Dans l'écriture textuelle, on ajoute la clé étrangère avec le symbole « **#** » ou en soulignant l'attribut concerné par un trait en pointillé
<ul> 
    <li><b>Auteur</b>(<u>Num_securite_sociale</u>, #Nom_pays, Nom_auteur, Prenom_auteur, Date_naissance)</li>
    <li><b>Auteur</b>(<u>Num_securite_sociale</u>, <u style=" border-bottom: 1px dashed #999;text-decoration: none;">Nom_pays</u>, Nom_auteur, Prenom_auteur, Date_naissance)</li>
</ul>
</div>

+++

#### Associations sans cardinalité (0,1) ou (1,1)

Prenons l'exemple de l'association Auteur/Livre :

<img  alt=''  src='https://megamaths.hd.free.fr/static/bdd/bdd7.svg'  />
<img  alt=''  src='https://megamaths.hd.free.fr/static/arrow-down-blue.svg' width='40px' />
<img  alt=''  src='https://megamaths.hd.free.fr/static/bdd/bdd8.svg'  />

<div class="alert alert-success" style="border-left: 15px solid #3c763d;"><b>Remarque</b>:

On a transformé l'association en relation par l'ajout de deux attributs correspondant respectivement aux clés primaires de chacune des entités. Dans ce cas, il y a donc <b>deux clés étrangères</b>. Et ce <b>couple de clés étrangères forme la clé primaire</b>. 
    
Sur notre schéma, on a les 2 dénominations associées <b>(CP et FK)</b>.
</div>

+++ {"deletable": false, "editable": false, "run_control": {"frozen": true}}

Nous avons donc le modèle relationnel  complet.

<center><img  alt=''  src='https://megamaths.hd.free.fr/static/bdd/bdd9.svg'  /></center>

+++

##  Les contraintes d'intégrité

Le modèle relationnel impose une règle minimale qui est **l'unicité des clés**, comme nous allons le voir ci-dessous. Cependant il existe plusieurs points pour respecter l'intégrité d'une base de données.

- **Unicité de clé**  
Par définition, une relation est un ensemble de tuples. Un ensemble n'ayant pas d'élément en double, il ne peut pas exister deux fois le même tuple dans une relation.
<div class="alert alert-warning" style="border-left:15px solid #eba83a;"> Toute relation doit donc posséder une <b>clé unique</b> (<b>clé primaire</b>).</div>
- **Contraintes référentielles**  
Correctement construite, une base de données fait appel à des données situées dans différentes relations.  
Pour que les données restent utilisables et cohérentes, il ne faut pas que l'on puisse détruire des données qui dépendent les unes des autres. C'est le rôle de l'intégrité référentielle de protéger ces relations. Autrement dit, 
<div class="alert alert-warning" style="border-left:15px solid #eba83a;">l'intégrité référentielle vérifie qu'une valeur de clé étrangère existe bien en tant que valeur de clé primaire dans une autre table.</div>   
Dans notre exemple : Des éditeurs sont reliés à un livre.   
L'intégrité référentielle empêchera la Suppression d'un éditeur si des livres y Sont rattachés dans la base de données. En procédant ainsi, les éditeurs seront toujours reliés à leur livre et l'utilisateur ne pourra pas supprimer (involontairement) des données essentielles.
- **Valeurs nulles**  
Lors de l'insertion de tuples dans une relation, il arrive fréquemment qu'un attribut soit inconnu ou non applicable.   
Par exemple, la population ou la superficie d'un pays à une certaine date peuvent être inconnues. On est alors amené à introduire dans la relation une valeur conventionnelle, appelée valeur nulle.<br />
<div class="alert alert-warning" style="border-left:15px solid #eba83a;"><b>Attention: </b> Tout attribut dans une relation ne peut prendre une valeur nulle. </div>  
En effet, l'existence d'une clé unique impose la connaissance de la clé afin de pouvoir vérifier que cette valeur de clé n'existe pas déjà. La clé ne peut donc pas être nulle !
- **Contraintes de domaines**    
En théorie, une relation est construite à partir d'un ensemble de domaines. En pratique, les domaines gérés par les systèmes sont souvent limités aux types de base (entiers, réels, chaînes de caractères), parfois monnaie et date. Afin de spécialiser un type de données pour composer un domaine plus fin (par exemple, les années de parution d'un livre qui peuvent être des entiers compris entre 1500 et 2100. Cette notion de contrainte de domaine est souvent ajoutée aux règles d'intégrité.


+++ {"deletable": false, "editable": false, "run_control": {"frozen": true}}

## **À retenir.**

- Le modèle relationnel consiste est un modèle dans lequel les données sont représentées par des ensembles de **n-uplets** appelés des **relations**.  
- Un élément d'une relation est appelé une entité. Il représente généralement un objet, une action, une personne du monde réel. 
- Chaque entité possède des propriétés appelées des attributs. 
- On spécifie une relation en donnant son schéma, c'est-à-dire son nom, la liste de ses attributs avec leur domaine, c'est-à-dire  l'ensemble des valeurs que peuvent prendre un attribut.  
- Une base de données est un ensemble de relations et le schéma d'une base est l'ensemble des schémas des relations qui la compose.

La cohérence des données au sein d'une base est assurée par des contraintes d'intégrité. Ces dernières sont des invariants, c'est-à-dire des propriétés logiques que les données doivent vérifier à tout instant.

On distingue parmi ces contraintes :

- Les contraintes d'entité qui garantissent que chaque entité d'une relation est unique. Une clé primaire est un ensemble d'attributs qui identifie chaque entité de la relation de manière unique et garantit la contrainte d'entité.
-  Les contraintes de référence qui créent des associations entre deux relations. Elle permettent de garantir qu'une entité d'une relation B mentionne une entité existante dans une relation A. Une clé étrangère est un ensemble d'attributs d'une table qui sont une clé primaire dans une autre table.
-  Les contraintes de domaines qui restreignent les valeurs d'un attribut à celles du domaine et évitent que l'on puisse donner à un attribut une valeur illégale.
-  Les contraintes utilisateurs qui restreignent encore plus les valeurs d'un ou de plusieurs attributs et sont guidées par la nature des données que l'on souhaite stocker dans la base.

Ces contraintes doivent être utilisées pour assurer la qualité des données :
elles permettent de s'assurer que les données sont « conformes » aux entités du monde réel qu'elles représentent.

+++ {"deletable": false, "editable": false, "run_control": {"frozen": true}}

## Exercices 

Compétences attendues:
- Savoir analyser un modèle Entités/Associations.
- Savoir passer du modèle Entités/Associations au modèle relationnel.

+++ {"solution2": "hidden", "solution2_first": true}

<u><b>Exercice 1</b></u> 

On souhaite gérer des réservations dans une compagnie d'hôtels. On considère donc le modèle Entités/Associations suivant :

<center><img  alt=''  src='https://megamaths.hd.free.fr/static/bdd/bdd10.svg'  /></center>



1. Le modèle Entité/association.    
  À l'aide de ce modèle, répondre aux questions suivantes :  
  1.  Peut-on avoir des clients homonymes ?
  2.  Un client peut-il réserver plusieurs chambres à une date donnée ?
  3. Est-il possible de réserver une chambre sur plusieurs jours ?
  4. Peut-on savoir si une chambre est libre à une date donnée ?
  5.  Peut-on réserver plusieurs fois une chambre à une date donnée ?

+++ {"solution2": "hidden"}

<u><b>Exercice 1 correction</b></u> 
1.   
  1. Le nom du client n'est pas l'attribut identifiant l'entité Client. L'identifiant est ici l'attribut *numero*. Il peut donc y avoir des clients homonymes, ces clients auront des numéros différents.  
  2. La cardinalité maximale de l'entité Client est n donc un client peut réserver plusieurs fois. Cependant d'après la cardinalité de l'entité *Reservation*, une chambre ne peut correspondre qu'à une et une seule réservation. On en conclue qu'un client peut réserver plusieurs chambres à une date donnée s'il effectue plusieurs réservations.
  3. Oui, un client peut réserver une chambre sur plusieurs jours à conditions qu'il effectue plusieurs réservations.
  4. Oui, pour savoir si une chambre est disponible à une date donnée, il faudra lister les réservations s'assurer et rechercher qu'il n'y a aucune réservation à la date donnée.
  5. Oui dans ce schéma rien ne garantit que la même chambre puisse être louée qu'une seule fois.

+++ {"solution2": "hidden", "solution2_first": true}

2.  Donner le schéma relationnel de la base de données « compagnie d'Hotels »

+++ {"solution2": "hidden"}

Il faut commencer pour convertir toutes les entités en relations et ensuite compléter ces relations en fonction des associations.



<center><img  alt=''  src='https://megamaths.hd.free.fr/static/bdd/bdd13.svg'  /></center>
<!--<center><img  alt=''  src='images/bdd12.svg'  /></center>-->

+++ {"solution2": "hidden", "solution2_first": true}

3. À partir du modèle relationnel construit en 2. remplir le tableau ci-dessous :


  <table>
<tr>
    <th  width=150px,  style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Relation</th>
    <th  width=150px, style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Attribut</th>
    <th  width=100px, style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Type</th>
    <th  width=100px, style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Unicité</th>
    <th  width=100px, style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Domaine<br />éventuel</th>
    <th  width=100px,  style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Valeur<br/>nulle<br />permise</th>
    <th  width=100px, style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Clé</th></tr>
<tr>
    <td style="text-align:left; border:1px solid black;">Chambre</td>
    <td style="text-align:left; border:1px solid black;">Nom_hotel</td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;"></td>
      </tr>
<tr>
    <td style="text-align:left; border:1px solid black;">Chambre</td>
    <td style="text-align:left; border:1px solid black;">Prix</td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;"></td>
</tr>
<tr>
    <td style="text-align:left; border:1px solid black;">Réservation</td>
    <td style="text-align:left; border:1px solid black;">Date_resa</td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;"></td>
      </tr>
<tr>
    <td style="text-align:left; border:1px solid black;">Client</td>
    <td style="text-align:left; border:1px solid black;">Numero</td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;"></td>
      </tr>
    </table>
    


- Pour la colonne *Type*, on choisira parmi : *Entier, Réel, Texte, Date*.
- Pour les colonnes *unicité* et *Valeur nulle* permise, on répondra par Oui ou Non.
- Pour la colonne *Clé*, on mettra CP pour clé primaire et CE pour clé étrangère ou on
laissera vide.
- Pour la colonne *Domaine éventuel*, on précisera le domaine possible.

+++ {"solution2": "hidden"}

 <table>
<tr>
    <th  width=150px,  style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Relation</th>
    <th  width=150px, style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Attribut</th>
    <th  width=100px, style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Type</th>
    <th  width=100px, style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Unicité</th>
    <th  width=100px, style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Domaine<br />éventuel</th>
    <th  width=100px,  style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Valeur<br/>nulle<br />permise</th>
    <th  width=100px, style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Clé</th></tr>
<tr>
    <td style="text-align:left; border:1px solid black;">Chambre</td>
    <td style="text-align:left; border:1px solid black;">Nom_hotel</td>
    <td style="text-align:left; border:1px solid black;">Texte</td>
    <td style="text-align:left; border:1px solid black;">Non</td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;">Non</td>
    <td style="text-align:left; border:1px solid black;">CE</td>
      </tr>
<tr>
    <td style="text-align:left; border:1px solid black;">Chambre</td>
    <td style="text-align:left; border:1px solid black;">Prix</td>
    <td style="text-align:left; border:1px solid black;">Réel</td>
    <td style="text-align:left; border:1px solid black;">Non</td>
    <td style="text-align:left; border:1px solid black;">R<sup>+</sup></td>
    <td style="text-align:left; border:1px solid black;">Oui</td>
    <td style="text-align:left; border:1px solid black;"></td>
</tr>
<tr>
    <td style="text-align:left; border:1px solid black;">Réservation</td>
    <td style="text-align:left; border:1px solid black;">Date_resa</td>
    <td style="text-align:left; border:1px solid black;">Date</td>
    <td style="text-align:left; border:1px solid black;">Non</td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;">Non</td>
    <td style="text-align:left; border:1px solid black;"></td>
      </tr>
<tr>
    <td style="text-align:left; border:1px solid black;">Client</td>
    <td style="text-align:left; border:1px solid black;">Numero</td>
    <td style="text-align:left; border:1px solid black;">Entier</td>
    <td style="text-align:left; border:1px solid black;">Oui</td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;">Non</td>
    <td style="text-align:left; border:1px solid black;">CP</td>
      </tr>
    </table>
    
- *Nom_hotel* est une clé étrangère dans la relation Chambre, il ne peut pas  être nul, par contre il peut ne pas être unique.
- Prix doit vraisemblablement être positif.
- *Date_resa* n'est pas unique mais doit être présent.
- *Numero* est bien la clé primaire de la relation Client, cet attribut est donc unique et non nul.


+++

<u><b>Exercice 2</b></u> 

On donne ci-dessous le modèle Entités/Associations représentant des visites dans un centre médical

<center><img  alt=''  src='https://megamaths.hd.free.fr/static/bdd/bdd11.svg'  /></center>
<!--<center><img  alt=''  src='images/bdd11.svg'  /></center>-->


1. Le modèle Entité/association.   
En utilisant ce modèle, répondre aux questions suivantes :
  1. Un patient peut-il effectuer plusieurs visites ?
  2. Un médecin peut-il recevoir plusieurs patients dans la même consultation ?
  3. Peut-on prescrire plusieurs médicaments dans une même consultation ?
  4. Deux médecins différents peuvent-ils prescrire le même médicament ?

+++

2.  Donner le schéma relationnel de la base de données « visites médicales »

+++

3. On donne ci-dessous les occurrences de la relation Consultation issue du modèle relationnel. Citer les anomalies constatées :

<table>
<tr>
    <th  width=150px,  style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Numero</th>
    <th  width=150px, style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Matricule</th>
    <th  width=100px, style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Numero_SS</th>
    <th  width=100px, style="text-align:center; background-color:#FFFFE0; border:1px solid black;">Date_consult</th>
<tr>
    <td style="text-align:left; border:1px solid black;">1</td>
    <td style="text-align:left; border:1px solid black;">123</td>
    <td style="text-align:left; border:1px solid black;"></td>
    <td style="text-align:left; border:1px solid black;">21/11/2018</td>
</tr>
<tr>
    <td style="text-align:left; border:1px solid black;">2</td>
    <td style="text-align:left; border:1px solid black;">123</td>
    <td style="text-align:left; border:1px solid black;">182086926825812</td>
    <td style="text-align:left; border:1px solid black;"></td>
</tr>
<tr>
    <td style="text-align:left; border:1px solid black;">2</td>
    <td style="text-align:left; border:1px solid black;">526</td>
    <td style="text-align:left; border:1px solid black;">'Aspirine'</td>
    <td style="text-align:left; border:1px solid black;">13/03/2019</td>
</tr>
</table>

+++ {"solution2": "hidden", "solution2_first": true}

<u><b>Exercice 3</b></u> 

On souhaite modéliser un annuaire téléphonique simple dans lequel chaque personne (identifiée par son nom et son prénom) est associée à son numéro de téléphone.  

Proposer une modélisation relationnelle de cet annuaire.

+++ {"solution2": "hidden"}

On peut modéliser l'annuaire de la manière suivante:     <div class="alert alert-block alert-warning"><b>Annuaire</b>(<i>nom</i>: String, <i>prenom</i>: String, <u><i>tel</i>: String</u>)</div>


On n'oubliera pas de préciser que le numéro, par définition unique, est une clé primaire.  
Son domaine peut être String afin d'éviter les problèmes de 0 en première position ou de permettre de saisir des caractères non numériques comme +33 pour la France.

+++

<u><b>Exercice 4</b></u> 

Donner la modélisation relationnelle d'un bulletin scolaire.  
Cette dernière doit permettre de mentionner
- des élèves, possédants un numéro d'étudiant alphanumérique unique
- un ensemble de matières fixées, mais qui ne sont pas données
- au plus une note sur 20, par matière et par élève.

On prendra soin de préciser toutes les contraintes utilisateurs qui ne peuvent êtres inscrites dans les schémas des relations.

+++

<u><b>Exercice 5</b></u>  <!--Exercice 142-->
On considère la solution donnée pour l'exercice 3.  Dire si chacun des ensembles est une relation valide pour le schéma *Annuaire*.


1. {}
2. {('Titi', 'Toto', '0123456789')}
3. {('Titi','Toto', '0123456789'),('Doe', 'John', '0123456789')}
4. {('Titi', 'Toto', '0123456789'), ('Titi', 'Toto', '987654343210')}
5. {('Titi', 'Toto', '0123456789'), ('Doe', 'John')}
6. {('Titi','Toto',42)}


+++

<u><b>Exercice 6</b></u>  <!--Exercice 143-->

On considère la solution donnée pour l'exercice 4. Dire si chacun des ensembles est une relation valide pour le schéma de la base de données du bulletin de notes.



1.  
 -  Eleve={}
 -  Matiere = {}
 -  Note={}


2.  
 -  Eleve= {('Titi','Toto', 'AB56789),}
 -  Matiere = {('NSI',0), ('Sport',1)}
 -  Note = {('AB56789',1,17)}


3. 
 - Eleve= {('Titi', 'Toto', 'AB56789'),}
 - Matiere = {('NSI',0)}
 - Note = {('AB56789',1,17)}


4. 
 - Eleve= {('Titi','Toto','AB56789'),}
 - Matiere = {('NS1',0),}
 - Note = {('AB56789',0,17), ('AB56789',0,18)}


5. 
 - Eleve= {('Titi','Toto', 'AB56789'),}
 - Matiere = {('NSI',0), ('Sport',1)}
 - Note = {('AB56789',0, 17), ('AB56780', 1, 17)}

+++

<u><b>Exercice 7</b></u>  <!--Exercice 144-->

Modéliser des informations sur les départements français.  
Pour chaque département on veut pouvoir stocker son nom, son code, son chef-lieu et la liste de tous les départements voisins.    
**Attention**, les codes de département sont tous des nombres, sauf la Corse du Sud et la Haute Corse qui ont les codes 2A et 2B respectivement. Les départements d'Outre-Mer ont un code sur trois chiffres (de 971 à 976).   
Proposer une contrainte utilisateur permettant d'éviter la redondance d'information dans la liste des voisins.

+++

<u><b>Exercice 8</b></u>  <!--Exercice 145-->

Proposer une modélisation pour un réseau de bus.   
Cette dernière doit être suffisamment riche pour permettre de générer, pour chaque arrêt de bus du réseau, une fiche horaire avec tous les horaires de passage de toutes les lignes de bus qui desservent l'arrêt.

**Indication** : ici, plus qu'une simple traduction du français vers le modèle relationnel, on essayera de déterminer dans un premier temps quelles informations sont pertinentes et comment les représenter. On pourra ensuite procéder à la modélisation sous forme de relations

+++

<u><b>Exercice 9</b></u>  <!--Exercice 146-->

On considère deux relations <span style="background-color:#FFFFE0;">R(<u>a Int</u>,b Int,c Int)</span> et <span style="background-color:#FFFFE0;">S(<u>#a Int,e Int</u>)</span> où l'attribut <b><i>a</i></b> de <b><i>S</i></b> est une clé étrangère faisant référence à <b><i>a</i></b> de <b><i>R</i></b>.   Dire si les affirmations suivantes sont vraies ou fausses, en justifiant.

1. Les a de R sont tous deux à deux distincts.
2. Les b de R sont tous deux à deux distincts.
3. Les a de S sont tous deux à deux distincts.
4. Les e de S sont tous deux à deux distincts.
5. S peut être vide alors que R est non vide.
6. R peut être vide alors que S est non vide
