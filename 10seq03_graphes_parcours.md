---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

```{code-cell} python
# Exemple de représentation d'un graphe non orienté par un dictionnaire
graphe_non_oriente = {
    'A': ['B', 'C'],
    'B': ['A', 'D', 'E'],
    'C': ['A', 'F'],
    'D': ['B'],
    'E': ['B', 'F'],
    'F': ['C', 'E']
}

# Exemple de représentation d'un graphe orienté par un dictionnaire
graphe_oriente = {
    'A': ['B', 'C'],
    'B': ['D'],
    'C': ['E'],
    'D': ['B'],
    'E': ['F'],
    'F': ['C']
}
```

# Parcours dans un graphe

## Parcours en profondeur

```{code-cell} ipython
def dfs(graphe, debut, visite=None):
    if visite is None:
        visite = set()
    visite.add(debut)
    
    for voisin in graphe[debut]:
        if voisin not in visite:
            dfs(graphe, voisin, visite)
    return visite

# Exemple d'utilisation
graphe = {
    'A': ['B', 'C'],
    'B': ['A', 'D', 'E'],
    'C': ['A', 'F'],
    'D': ['B'],
    'E': ['B', 'F'],
    'F': ['C', 'E']
}
dfs(graphe, 'A')

```


## Parcours en largeur

```{code-cell} ipython
from collections import deque

def bfs(graphe, debut):
    visite = set()
    file = deque([debut])
    while file:
        sommet = file.popleft()
        if sommet not in visite:
            visite.add(sommet)
            print(sommet)
            file.extend(voisin for voisin in graphe[sommet] if voisin not in visite)
    return visite

# Exemple d'utilisation
graphe = {
    'A': ['B', 'C'],
    'B': ['A', 'D', 'E'],
    'C': ['A', 'F'],
    'D': ['B'],
    'E': ['B', 'F'],
    'F': ['C', 'E']
}
bfs(graphe, 'A')

```

# Algos

## Algorithme de Dijkstra

Principe de l'algorithme: Trouve le plus court chemin entre un sommet source et tous les autres sommets dans un graphe pondéré.
Applications: Plus court chemin dans un graphe pondéré.

```{code-cell} python
import heapq

def dijkstra(graphe, debut):
    distances = {sommet: float('infinity') for sommet in graphe}
    distances[debut] = 0
    pq = [(0, debut)]
    while pq:
        (dist_actuelle, sommet_actuel) = heapq.heappop(pq)
        if dist_actuelle > distances[sommet_actuel]:
            continue
        for voisin, poids in graphe[sommet_actuel].items():
            distance = dist_actuelle + poids
            if distance < distances[voisin]:
                distances[voisin] = distance
                heapq.heappush(pq, (distance, voisin))
    return distances

# Exemple de graphe pondéré
graphe_pondere = {
    'A': {'B': 1, 'C': 4},
    'B': {'A': 1, 'C': 2, 'D': 5},
    'C': {'A': 4, 'B': 2, 'D': 1},
    'D': {'B': 5, 'C': 1}
}
dijkstra(graphe_pondere, 'A')
```
