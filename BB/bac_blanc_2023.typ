BACCALAURÉAT BLANC

ÉPREUVE D’ENSEIGNEMENT DE SPÉCIALITÉ

SESSION 2023

NUMÉRIQUE et SCIENCES INFORMATIQUES

Durée de l’épreuve : 3 heures 30

L’usage de la calculatrice n’est pas autorisé.

Dès que ce sujet vous est remis, assurez-vous qu’il est complet.

Ce sujet comporte 7 pages numérotées de 1/7 à 7/7.

<anchor>Le sujet est composé de trois exercices indépendants.

#emph[Exercice 1~: ];<anchor-1>#emph[ ];L’exercice porte sur les bases
de données et les types construits de données.#emph[#strong[
];#strong[\(4,5 points)];]

<anchor-2> \
On pourra utiliser les mots clés SQL suivants : AND, FROM, INSERT, INTO,
JOIN, OR, ON, SELECT, SET, UPDATE, VALUES, WHERE, COUNT, DISTINCT.

Partie 1

On étudie une base de données permettant la gestion d’une ludothèque
dont voici le schéma relationnel comportant 5 relations~:

Jeux\(#emph[idjeu] Int, titre String, NbJoueurMin Int, NbJoueurMax Int,
age Int, editeur String)

Adherent\(#emph[idad] Int, nom String, prenom String)

Emprunte\(#emph[idemprunt] Int,\#idjeu Int,\#idad Int, dateemprunt
String)

Classement\(#emph[\#idjeu] Int#emph[, \#idgenre] Int)

Genre\(#emph[idgenre] Int, genre String)

<anchor-3>Dans ce schéma relationnel :<anchor-4> \
- les clés primaires sont soulignées ;<anchor-5> \
- les clés étrangères sont précédées d’un \#.<anchor-6> \
Ainsi Emprunte.idjeu est une clé étrangère faisant référence à Jeu.idjeu

Voici un extrait de 4 des 5 tables
ci-dessus~:#box(width: 3.572cm, image("Pictures/1000000100000087000000AEA25417FAEAE7AA51.png"))

#box(width: 11.15cm, image("Pictures/10000001000001DC00000087ECCF195357F3F767.png"))

#box(width: 4.964cm, image("Pictures/10000001000000C7000000556EC3555CDA0F150A.png"));#box(width: 4.577cm, image("Pictures/100000010000009700000070D1FBA062032E5188.png"))

#block[
#set enum(numbering: "1)", start: 1)
+ Citer le nom des attributs de la table Jeux.

+ A quel genre appartient le jeu King Domino~?

+ En s’appuyant uniquement sur les extraits des tables fournies,
  expliquer ce que renvoie la requête suivante~:

  SELECT titre FROM Jeux WHERE NbJoueurMin \= 1~;

+ Une nouvelle adhérente s’inscrit à la ludothèque. Il s’agit de Julie
  Fayol. Elle emprunte le jeu Momiji le 15/01/2023.

  Écrire les requêtes qui permettent de mettre à jour les tables
  correspondantes.

+ Une erreur s’est glissée dans la table Jeux. L’éditeur de 7 Wonders
  n’est pas Asmodee mais Repos Production.

  Écrire la requête qui permet d’effectuer la correction.

+ Écrire une requête qui permet de connaître les titres de tous les jeux
  se jouant avec des cartes. \(Remarque~: on connaît l’id du genre)

+ Pour les deux questions suivantes, on suppose qu’on travaille sur
  l’ensemble des tables et plus seulement leurs extraits.

  #block[
  #set enum(numbering: "a)", start: 1)
  + Écrire une requête permettant de connaître le nombre de titres
    distincts empruntés par Fabien Maloux.
  + Écrire une requête permettant de connaître les titres des jeux du
    genre Expert ainsi que les âges auxquels on peut y jouer.
  ]
]

#strong[Partie 2];~

Les données sont récupérées pour être analysées à l’aide de Python.

Un bilan annuel de la ludothèque est fourni sous la forme d’un tableau
de dictionnaires dans lequel pour chaque emprunteur on a ajouté le
nombre d’emprunt sur une période donnée \(T1 pour Trimestre 1, T2 pour
trimestre 2, etc).

En voici un extrait ci-dessous~:

#box(width: 19.001cm, image("Pictures/10000001000003F50000007BD8596DC3CBFAE63B.png"))

#block[
#set enum(numbering: "1)", start: 1)
+ Que renvoie bilan\[1\]\[\"nbemprunt\"\]~?
]

#block[
#set enum(numbering: "1)", start: 2)
+ On veut connaître le nom de la personne qui a emprunté 6 livres au
  premier trimestre. Quelle instruction écrit-on~?
]

#block[
#set enum(numbering: "1)", start: 3)
+ Écrire une fonction #emph[recherche\_nom\(T)] ayant pour
  unique paramètre un tableau de dictionnaires #emph[T] \(comme bilan
  présenté précédemment) renvoyant un tableau contenant le nom de tous
  les adhérents ayant emprunté plus de 10 livres le premier trimestre.
]

#emph[Exercice 2];~: Cet exercice porte sur les arbres binaires et la
programmation objet.

\(3 points)

Cet exercice est composé de 3 parties indépendantes. La partie 1 porte
sur quelques concepts fondamentaux sur les arbres binaires, la partie 2
s’intéresse à la distance entre deux nœuds d’un arbre binaire et la
partie 3 présente les Arbre\_Somme.

#emph[#strong[Partie 1];];~

On implémente les arbres binaires en utilisant la programmation objet
avec la classe #emph[Nœud] ci-dessous~:

#box(width: 8.431cm, image("Pictures/10000001000001A000000078BE5847CB36ACA46B.png"))

#block[
#set enum(numbering: "1)", start: 1)
+ Combien d’attributs possèdent la classe Nœud~?

+ Représenter l’arbre obtenu avec les instructions
  suivantes~:#box(width: 6.315cm, image("Pictures/100000010000012B0000011EE37CC2CB0A112ED2.png"))

  \>\>\> a \= Noeud\(4)

  \>\>\> a.gauche \= Noeud\(5)

  \>\>\> a.droit \= Noeud\(14)

  \>\>\> a.gauche.droit \= Noeud\(8)

+ On considère l’arbre de la Figure 1~:

  #block[
  #set enum(numbering: "a)", start: 1)
  + Quelle est la profondeur du nœud d’étiquette 12~?
  + Quelle est la taille de l’arbre~?
  + Quel est le parcours préfixe de cet arbre~?
  ]
]

#emph[#strong[Partie 2];];~:

Dans toute cette partie, on prendra pour exemple l’arbre de la Figure 1.

On appelle distance entre deux nœuds, le nombre minimal de branches qui
sépare les deux nœuds. On notera d la distance.

Ainsi, pour l’arbre de la Figure 1, on a d\(12, 4) \= 2 et d\(8, 9) \=
4.

#block[
#set enum(numbering: "1)", start: 1)
+ Quelle est la distance entre les nœuds d’étiquette 5 et d’étiquette
  1~?
+ Soient n1 et n2, deux nœuds d’un arbre binaire, on admettra la
  relation suivante
]

d\(n1,n2)\= d\(n1,r) + d\(n2,r) – 2 × d\(r, dpc)

où r représente la racine de l’arbre et dpc le dernier parent \(nœud) en
commun depuis la racine aux nœuds n1 et n2.

Par exemple, pour les nœuds d’étiquette 12 et 4, le dernier parent en
commun est le nœud d’étiquette 5.

+ #block[
  #set enum(numbering: "a)", start: 1)
  + Écrire une fonction #emph[contient\(arbre, valeur)] qui renvoie
    #emph[True] si le nœud ayant l’étiquette #emph[valeur] est dans
    l’arbre, #emph[False] sinon.
  + Recopier et compléter la fonction #emph[chemin\(arbre, n)] qui
    renvoie la liste des nœuds compris entre la racine et le nœud
    d’étiquette #emph[n] compris. La valeur #emph[n] est supposée
    présente dans l’arbre.
  ]

#block[
#set enum(numbering: "1.", start: 2)
+ #block[
  #set enum(numbering: "a)", start: 1)
  + #box(width: 9.437cm, image("Pictures/10000001000001BF000000D4812512AE4DB861E1.png"))
  ]
]

#block[
#set enum(numbering: "1.", start: 3)
+ #block[
  #set enum(numbering: "a)", start: 1)
  + Pour trouver le dernier parent commun à deux nœuds n1 et n2, on
    procède de la manière suivante~:
  ]
]

- - - On détermine le chemin entre la racine et le nœud n1~;
    - On détermine le chemin entre la racine le le nœud n2~;
    - On compare les deux listes ci-dessus et le premier élément
      différent entre les deux listes est l’élément qui suit le dernier
      parent commun.

Écrire une fonction #emph[dpc\(arbre, n1,n2)] qui renvoie le dernier
parent commun aux nœud n1 et n2.

#block[
#set enum(numbering: "1)", start: 3)
+ Déduire des fonctions précédentes, une fonction
  #emph[distance\(arbre,n1,n2)] qui renvoie la distance entre deux
  nœuds.

  \(Remarque~: On aura remarqué que la longueur de la liste du chemin
  entre la racine et le nœud renvoie le nombre de nœud et non la
  distance. On admettra que la formule donnée en début de partie
  appliquée aux chemins renvoie effectivement la distance)
]

#emph[Exercice 3];~: Cet exercice porte sur les structures de données
#emph[#strong[\(4,5 points)];]

Dans cet exercice, on s’intéresse à une structure de données abstraite
#emph[#strong[les files de ];];#strong[priorité. ];L’intérêt de cette
structure est d’extraire directement l’élément de plus grande priorité.

Les éléments d’une file de priorité seront représentés par des tuples
\(e, k) où e est l’élément présent dans la file et k sa priorité. Plus
la valeur de k est grande, plus la priorité est importante.

La plus petite priorité est de 1. Des éléments différents peuvent avoir
la même priorité.

Une file de priorité ne peut pas comporter deux tuples identiques.

La structure file de priorité est munie des 4 primitives suivantes~:

#figure(
align(center)[#table(
  columns: 1,
  align: (col, row) => (auto,).at(col),
  inset: 6pt,
  [],
  [#strong[Structure de données abstraite];~: File\_de\_priorité],
  [Opérations~:],
  [- creer\_file\_prio\_vide~: vide → File\_de\_priorité

  creer\_file\_prio\_vide\() renvoie une file de priorité vide.

  ],
  [- est\_vide : File\_de\_priorité → Booléen

  est\_vide\(file\_de\_priorite) renvoie True si file\_de\_priorite est
  vide, False sinon.

  ],
  [- enfiler\_prio~: File\_de\_priorite, element, priorite → Rien

  enfiler\_prio\(file\_de\_priorite, e,k) ajoute le tuple \(e,k) à la
  file de priorité.

  ],
  [- defiler\_max\_prio~: File\_de\_priorite → \(element, priorite)

  defiler\_max\_prio\(file\_de\_priorite) renvoie un tuple de plus
  grande priorité et le retire de la file.

  ],
)]
)

Voici un exemple de file de priorité

#box(width: 11.437cm, image("Pictures/1000000100000237000000694120EF8A86B7E4E4.png"))

Les éléments sont enfilés à gauche quelle que soit leur priorité.

Partie 1

#block[
#set enum(numbering: "1)", start: 1)
+ Recopier le numéro de la ligne et compléter la colonne de droite du
  tableau ci-dessous en donnant l’état de la file de priorité après
  chaque instruction.
]

#figure(
align(center)[#table(
  columns: 3,
  align: (col, row) => (auto,auto,auto,).at(col),
  inset: 6pt,
  [], [], [],
  [1],
  [F \= creer\_file\_prio\_vide\()],
  [],
  [2],
  [enfiler\_prio\(F, 2, 1)],
  [],
  [3],
  [enfiler\_prio\(F, 3, 5)],
  [],
  [4],
  [enfiler\_prio\(F, 4, 7)],
  [],
  [5],
  [enfiler\_prio\(F, 7, 1)],
  [],
  [6],
  [defiler\_max\_prio\(F)],
  [],
  [7],
  [defiler\_max\_prio\(F)],
  [],
  [8],
  [enfiler\_prio\(F, 5, 3)],
  [],
)]
)

#block[
#set enum(numbering: "1)", start: 2)
+

+ Recopier et compléter la fonction#emph[ tuple\_min\(F)] qui prend en
  argument une file de priorité #emph[F] et renvoie le dernier tuple
  rencontré #emph[\(élément, priorité)] de plus faible priorité. A la
  fin de l’exécution, la file #emph[F] doit contenir les mêmes éléments
  qu’avant l’exécution sans tenir compte de l’ordre des éléments.

  #box(width: 9.23cm, image("Pictures/100000010000019B000000FCC30C5966CAE4C398.png"))
]

#block[
#set enum(numbering: "1)", start: 4)
+ Écrire une fonction #emph[taille\(F)] qui prend en argument une file
  de priorité #emph[F] et renvoie le nombre d’éléments dans la file. A
  la fin de l’exécution, la file #emph[F] doit contenir les mêmes
  éléments qu’avant l’exécution sans tenir compte de l’ordre des
  éléments.
]

#block[
#set enum(numbering: "1)", start: 5)
+ On considère maintenant un tableau T d’entiers. L’objectif de cette
  question est de trier le tableau T \(par ordre décroissant) en
  utilisant une file de priorité.

  #block[
  #set enum(numbering: "a)", start: 1)
  + Écrire une fonction #emph[tableau\_vers\_file\(T)] qui prend en
    argument un tableau d’entiers distincts #emph[T] et renvoie une file
    de priorité où les éléments sont rangés dans le même ordre que les
    éléments du tableau.

    \(Aide~: On réfléchira à la priorité à affecter au élément en ne
    perdant pas de vue que l’objectif final est de trier T)

    Par exemple, l’exécution de #emph[tableau\_vers\_file\(\[8 ,1 ,6
    ,5\])] doit renvoyer une file de priorité de ce type
    #box(width: 5.085cm, image("Pictures/100000010000011100000034A36911DAFEA503DC.png"))
    \(les … sont à remplacer par ce qui convient)

  + En déduire une fonction #emph[trier\(T)] qui prend en paramètre un
    tableau #emph[T] d’entiers et renvoie un nouveau tableau trié dans
    l’ordre décroissant.
  ]
]

#strong[Partie 2];~: #emph[Des piles à l’aide des files de priorité]

Dans cette partie, on cherche à implémenter la structure de données
abstraite Pile à l’aide des files de priorités.

+ Rappeler ce qu’est une Pile ainsi que son interface minimale.

+ On considère la Pile #emph[p] ci-contre.
  #box(width: 1.556cm, image("Pictures/100000010000003700000066EA9B1EB1D2D81FE1.png"))

  Il s’agit d’une file de priorité dont les priorités sont ici masquées.

  #block[
  #set enum(numbering: "a)", start: 1)
  + Quelle est l’élément de plus basse priorité~? De plus haute
    priorité~?
  + Quelle priorité va-t-on choisir pour l’élément 2~?
  + En déduire la représentation de la Pile #emph[p] ci-contre sous la
    forme d’une file de priorité.
  ]

+ On considère la classe #emph[Pile] ci-dessous~:

#box(width: 11.536cm, image("Pictures/10000001000001B400000088C41C17F7800EEC46.png"))

#block[
#set enum(numbering: "1.", start: 4)
+ #block[
  #set enum(numbering: "a)", start: 1)
  + Écrire la méthode #emph[depiler\(self).]
  + Écrire la méthode #emph[empiler\(self,e)];.
  ]
]

#block[
#set enum(numbering: "1)", start: 1)
+
]
