import csv
import math
from tkinter import*
COTE=1


def importe_csv_en_listes(fichier):
	'''importe des données depuis un fichier csv. 
	'fichier' est une chaîne de caractères
	Renvoie une liste de listes'''
	lecteur = csv.reader(open(fichier,'r'))
	return list(lecteur)    


def distance(point1,point2):
    '''renvoie la distance entre point1 et point2
    Chaque point est une liste comprenant des nombres et une étiquette'''
    somme = 0
    for i in range(1,len(point1)):
        somme = somme + (float(point1[i])-float(point2[i]))**2
        
    return math.sqrt(somme)
    
	

def plus_proche_voisin(nouveau_point, liste_de_listes):
    #Paramètre du premier point de la liste (on ne prend pas la première ligne)
    point0 = liste_de_listes[1]
    distance_min = distance(nouveau_point, point0)
    indice = 1
    
    for i in range(1,len(liste_de_listes)) :
        point = liste_de_listes[i]
        dist = distance(point, nouveau_point)
        if dist < distance_min:
            indice = i
            distance_min = dist
    return liste_de_listes[indice]



def k_plus_proches_voisins(k, nouveau_point, liste_de_listes):
    '''renvoie la liste des k plus proches voisins
    utilise la fonction plus_proche_voisin'''
    
    #On recopie la liste de points reçue car on va la modifier.
    liste = list(liste_de_listes)
    #On prépare le résultat: la liste des indices des k plus proches voisins de nouveau_point 
    resultat = []
    for i in range(k):
        plus_proche = plus_proche_voisin(nouveau_point, liste)
        resultat.append(plus_proche)
        liste.remove(plus_proche)
    return resultat


def etiquettes(liste_de_voisins):
    '''renvoie un dictionnaire donnant le nombre d'occurences
    de chaque étiquette de la liste de points donnée'''
    
    etiquette_des_voisins = {}
    for ligne in liste_de_voisins:
        valeur_etiquette = ligne[0]
        if valeur_etiquette in etiquette_des_voisins:
            etiquette_des_voisins[valeur_etiquette] +=1
        else:
            etiquette_des_voisins[valeur_etiquette] =1
    return etiquette_des_voisins

def etiquette_majoritaire(liste_de_voisins):
    '''renvoie l'étiquette majoritaire d'une liste de points'''
    
    dico_etiquettes = etiquettes(liste_de_voisins)
    effectif_max = 0
    resultat = ""
    for etiquette in dico_etiquettes.keys():
        if dico_etiquettes[etiquette] > effectif_max:
            effectif_max = dico_etiquettes[etiquette]
            resultat = etiquette
    return resultat

#Fonctions permettant de créer l'image à partir des 784 valeurs   
def gris_en_hexa(chaine):
    chainehexa = hex(255-int(chaine))
    chainehexa = chainehexa[2:]
    resultat = "#"+chainehexa+chainehexa+chainehexa
    return resultat
       
def dessine_pixel(x, y, couleur):
    canevas.create_rectangle(x,y, x+COTE, y+COTE, fill=couleur, width=0) 
    
def dessine_chiffre(indice):
    #on récupère la valeur de k
    k = int(Saisiek.get())
    #On récupère la liste de valeurs du chiffre à afficher
    liste = test[indice]
    #Ici on va chercher l'étiquette la plus probable pour la ligne 'liste
    liste_de_voisins = k_plus_proches_voisins(k, liste, apprentissage)
    chiffre = etiquette_majoritaire(liste_de_voisins)
    #Ensuite on affiche cette valeur dans le champ 'Etiquette', à la place de la chaîne "à trouver"
    Etiquette.configure(text=chiffre)
    for i in range(1,29):
        for j in range(1,29):
            couleur = gris_en_hexa(liste[i+(j-1)*28])
            dessine_pixel(i*COTE, j*COTE, couleur)  
            
            
###########Fonctions permettant de réagir aux actions de l'utilisateur            
def valide_saisie(event):   
    valeur = int(Numpoint.get())
    dessine_chiffre(valeur) 
        
def moins_un():
    valeur = int(Numpoint.get())
    if valeur>1:
        valeur -=1
    valeurNumpoint.set(str(valeur))    
    dessine_chiffre(valeur) 

def plus_un():
    valeur = int(Numpoint.get())
    if valeur<len(apprentissage):
        valeur +=1
    valeurNumpoint.set(str(valeur)) 
    dessine_chiffre(valeur) 


            

#initialisation
apprentissage = importe_csv_en_listes('mini_mnist_train.csv')
test = importe_csv_en_listes('mnist_test.csv')

fenetre=Tk()
Textimage = Label(fenetre, text = "image:", justify=RIGHT)
Textimage.grid(column=1, row=1)

canevas = Canvas(fenetre, width=28*COTE , height=28*COTE, background='white')
canevas.grid(column=2, row=1)

TextPrevision = Label(fenetre, text = "chiffre le plus probable:", justify=RIGHT)
TextPrevision.grid(column=1, row=2)

Etiquette = Label(fenetre, text = "")
Etiquette.grid(column=2, row=2)

Textk = Label(fenetre, text = "Valeur de k:", justify=RIGHT)
Textk.grid(column=1, row=3)

valeurk = StringVar(fenetre)
valeurk.set("1")
Saisiek = Entry(fenetre, textvariable=valeurk, width=2)
Saisiek.bind("<Return>", valide_saisie)
Saisiek.grid(column=2, row=3)



TextNumimage = Label(fenetre, text = "numéro de ligne:", justify=RIGHT)
TextNumimage.grid(column=1, row=4)

valeurNumpoint = StringVar(fenetre)
valeurNumpoint.set("1")
Numpoint = Entry(fenetre, textvariable=valeurNumpoint, width=6)
Numpoint.bind("<Return>", valide_saisie)
Numpoint.grid(column=2, row=4, columnspan=2)

BoutonMoins = Button(fenetre, text = '-', command=moins_un)
BoutonMoins.grid(column=2, row=5)

BoutonPlus = Button(fenetre, text = '+', command=plus_un)
BoutonPlus.grid(column=3, row=5)

dessine_chiffre(1)

fenetre.mainloop()
