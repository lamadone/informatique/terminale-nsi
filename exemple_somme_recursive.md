---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: 'Python 3.7.6 64-bit (''base'': conda)'
  name: python3
---

# Un exemple pour débuter

```{code-cell} ipython
def somme_entiers_recursive(n: int) -> int:
    """
    Calcule récursivement la somme des $n$ premiers entiers

    >>> somme_entiers_recursive(4)
    10
    """
    if not isinstance(n,int):
        raise Exception("L'argument doit être de type entier")
    elif n < 0:
        raise Exception("L'argument doit être positif")
    

    if n > 0:
        return somme_entiers_recursive(n - 1) + n
    else:
        return 0
```

```{code-cell} ipython
:tags: [hide-output]

from doctest import testmod
testmod(verbose=True)
```

```{code-cell} ipython
:tags: [remove-cell]

from pycallgraph2 import PyCallGraph
from pycallgraph2 import Config
from pycallgraph2 import GlobbingFilter
from pycallgraph2.output import GraphvizOutput

with PyCallGraph(config=Config(max_depth=99, trace_filter=GlobbingFilter(exclude=['pycallgraph.*','cours.*'],include=['*'])),output=GraphvizOutput(output_file='somme.png')):
    somme_entiers_recursive(4)
```

```{code-cell} ipython
#!pip install nbtutor
!jupyter nbextension install --overwrite --py nbtutor
!jupyter nbextension enable --py nbtutor
```

```{code-cell} ipython
%load_ext nbtutor
```

```{code-cell} ipython
%%nbtutor -i
somme_entiers_recursive(4)
```

```{code-cell} ipython

```
