---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

```{code-cell} ipython3
%%bash
source $PWD/.venv/bin/activate
pip install git+https://github.com/lgpage/nbtutor@7ccbc2c#nbtutor
#pip install --force tutormagic
#pip install nbtutor
jupyter nbextension install --overwrite --sys-prefix --py nbtutor
jupyter nbextension enable --py nbtutor
```

```{code-cell} ipython3
%reload_ext tutormagic
```

```{code-cell} ipython3
%reload_ext nbtutor
```

```{code-cell} ipython3
%%nbtutor --reset --force -d 50
def Mystere(a, b):
    if b == 1:
        return a
    else:
        return a*Mystere(a, b - 1)

print(Mystere(5,4))
```

```{code-cell} ipython3

```
