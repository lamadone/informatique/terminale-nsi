class Tableau:
    """
    Une classe pour manipuler un tableau
    """
    def __init__(self, taille: int):
        """
        Un tableau est initialisé à une taille fixée
        """
        self.__taille = taille
        self.__L = [None] * taille

    def __repr__(self):
        return f"{self.__L}"

    def __getitem__(self, index):
        if 0 <= index < self.__taille:
            return self.__L[index]
        else:
            raise IndexError("L'indice demandé est en dehors du tableau")

    def __setitem__(self, index, valeur):
        if 0 <= index < self.__taille:
            self.__L[index] = valeur
        else:
            raise IndexError("L'indice demandé est en dehors du tableau")

    def __len__(self):
        return self.__taille


class Liste:
    def __init__(self, *args):
        self._l = list(args)

    def __repr__(self):
        return f"{self._l}"

    def tete(self, a):
        self._l = self._l + [a]

    def queue(self):
        if self._l != []:
            liste = Liste()
            for i in self._l[1:]:
                liste.tete(i)
            return self._l[0], liste

    def est_vide(self):
        return self._l == []


class Pile:
    """
    Une classe pour avoir une interface basique de pile
    """
    def __init__(self):
        """
        Initialisation d'une variable interne
        """
        self._liste = []

    def __repr__(self):
        return f"{self._liste}"

    def empiler(self, a):
        self._liste = self._liste + [a]

    def depiler(self):
        return self._liste.pop()

    def est_vide(self):
        return len(self._liste) == 0

    def len(self):
        return len(self._liste)

    def top(self):
        return self._liste[-1]

    def clear(self):
        self._liste = []

    def dup(self):
        self.empiler(self.top())

    def swap(self):
        if len(self._liste) < 2:
            raise Exception("La taille de la pile doit être ≥ 2")
        self._liste[-1], self._liste[-2] = self._liste[-2], self._liste[-1]


class File:
    def __init__(self):
        self.__L = []

    def __repr__(self):

        return f"{self.__L}"

    def enfiler(self, a):
        self.__L = [a] + self.__L

    def defiler(self):
        e = self.__L[-1]
        self.__L = self.__L[:-1]
        return e

    def est_vide(self):
        return len(self.__L) == 0
