#!/usr/bin/env python3
import csv
import sys
import subprocess
import requests

   
def publish(username, password, document):
    document = document.split('/')[-1]
    r = requests.put(
        f'https://educloud.ac-creteil.fr/remote.php/dav/files/vjumel/NSI/{document}',
        auth=(username, password),
        files = {'file': open(document, 'rb')}
    )
    return r.status_code



def read_and_publish(username, password):
    with open('compile.csv','r') as fp:
        fichiers = csv.DictReader(fp, delimiter=';')
        for seance in fichiers:
            documents = seance['fichiers'].split(' ')
            status = []
            for document in documents:
                status.append(
                    publish(
                        username,
                        password,
                        document.replace('.tex','.pdf')
                    )
                )
    return tuple(status)


if __name__ == '__main__':
    username, password= tuple(sys.argv[1:])
    read_and_publish(username, password)

