---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Files


## Principe général

Une file est une structure de données organisée suivant le principe du __premier arrivé, premier servi/sorti__. On utilise souvent l'acronyme anglais __FIFO__ pour First In, First Out.


![](queue.jpg)


Il faut se représenter une file d'attente organisée de personnes. On définit alors un tête de file (la personne qui arrive au gichet) et une queue de file, la personne qui vient d'arriver dans la file.

Ces files sont utilisés naturellement dans de nombreux domaines :
  - gestion de stock
  - arrivée des paquets sur un réseau
  - …


## Primitives essentielles

Une file dispose des primitives usuelles suivantes :

* Enfiler
* Défiler
* estVide
* len


Contrairement aux tableaux, aux listes ou aux piles dont la mise en œuvre est assez «naturelle» avec les outils de l'informatique, la file se met en œuvre, soit à l'aide de tableaux, soit à l'aide d'une liste chainée[<sup>1</sup>](#_blank "Hors programme"), soit encore à l'aide de deux piles.

```{code-cell} ipython
from cours.structures import File
```

```{code-cell} ipython
f = File()
```

```{code-cell} ipython
f
```

```{code-cell} ipython
f.enfiler('perle')
```

```{code-cell} ipython
f
```

```{code-cell} ipython
f.enfiler('jaune')
```

```{code-cell} ipython
f.enfiler('rouge')
```

```{code-cell} ipython
f
```

```{code-cell} ipython
f.defiler()
```

```{code-cell} ipython
f
```


