---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Complément sur les tableaux

Dans certains langage comme le C, la structure élémentaire de données est le tableau. Il est de taille fixé lors de sa création et celle-ci ne peut changer (contrairement aux listes). En toute rigeur, le type des données est donné également à la création du tableau et ne peut être changé. Ici, ça ne sera pas le cas, même si nous tâcherons d'avoir des données de même type au sein d'un tableau.

+++

Comme dans les cas précédents, j'ai regroupé les objets utiles dans un module, qu'on importe ici.

```{code-cell} ipython
from cours.structures import Tableau
```

On crée un premier tableau en précisant la taille.

```{code-cell} ipython
t = Tableau(10)
```

On peut afficher le contenu du tableau. Cette possibilité n'existe ici qu'à des fins pédagogiques et n'est pas présente dans tous les langages utilisant des tableaux.

```{code-cell} ipython
t
```

Le choix ici est d'initialiser toutes les valeurs du tableau à `None`.

On peut accéder à la valeur d'un élément, avec une notation qui ressemble à celles des `list`.

```{code-cell} ipython
t[0]
```

Ici, `t[0]`  n'affiche rien et c'est «normal».

```{code-cell} ipython
t[0] = 1
```

On peut affecter une valeur à un des éléments du tableau, avec une notation compatible avec les notations sur les `list`.

```{code-cell} ipython
t[0]
```

```{code-cell} ipython
:tags: [raises-exception]

t[10]
```

Si on cherche à accéder à un indice qui n'existe pas, on obtient une erreur.

```{code-cell} ipython
:tags: [raises-exception]

t.append('5')
```

Le tableau ayant une taille fixe, il ne peut pas être étendu avec `.append()`.

```{code-cell} ipython
:tags: [raises-exception]

t1 = Tableau(1)
t2 = Tableau(1)
t1[0] = 1
t2[0] = 2
t1 + t2
```

Il n'y a pas non plus d'opérateur d'addition/concaténation.

```{code-cell} ipython
:tags: [raises-exception]

t1 = Tableau(1)
t2 = Tableau(1)
t3 = Tableau(2)
t1[0] = 1
t2[0] = 2
t3 = t1 + t2
```

Même en déclarant le nouveau tableu à l'avance.

## Exercices d'application avec des tableaux

+++

On se donne une liste test triée, qu'on convertit en tableau.

```{code-cell} ipython
tableau = Tableau(8)
for k,v in enumerate([1,2,3,12,13,22,45,57]):
    tableau[k] = v
```

Dans les deux exercices suivants, on indiquera une valeur en dehors de la liste avec $\infty$ qu'on peut représenter en Python par `float('inf')` (norme [IEEE 754](https://fr.wikipedia.org/wiki/IEEE_754) sur la représentation des nombres).

+++

Écrire une fonction _itérative_ qui, pour un tableau trié, renvoie l'indice d'une valeur donnée, en utilisant l'algorithme de dichotomie.

```{code-cell} ipython
---
nbgrader:
  grade: false
  grade_id: cell-8bbb2d0a81eeee7e
  locked: false
  schema_version: 3
  solution: true
  task: false
---
def dicho(tab, x) -> int:
    ### BEGIN SOLUTION
    mil = len(tab) // 2
    while x != tab[mil]:
        newtab = Tableau(mil)
        for k in range(mil):
            newtab[k] = tab[k]
        tab = newtab
        mil = len(tab) // 2
        return mil
    else:
        return float('inf')
    ### END SOLUTION
```

```{code-cell} ipython
dicho(tableau, 7)
```

```{code-cell} ipython
---
nbgrader:
  grade: true
  grade_id: cell-219d0612aa6bc886
  locked: true
  points: 5
  schema_version: 3
  solution: false
  task: false
---
assert dicho(tableau, 2) == 1
assert dicho(tableau, 1) == 0
assert dicho(tableau, 57) == 7
assert dicho(tableau, 100) == float('inf')
assert dicho(tableau, 0) == float('inf')
```

Écrire une fonction _récursive_ qui, pour un tableau trié, renvoie l'indice d'une valeur donnée, en utilisant l'algorithme de dichotomie.

```{code-cell} ipython
---
nbgrader:
  grade: false
  grade_id: cell-0d3eb9f34020121b
  locked: false
  schema_version: 3
  solution: true
  task: false
---
def dichoREC(tab, x) -> int:
    ### BEGIN SOLUTION
    if len(tab)==0 :                                          
        return float('inf') # ou None cest cette ligne qui ne va pas car -1 se propage
    else :                                                                  
        mil=len(tab)//2                                                     
        if tab[mil] == x :                                                  
            #print("trouvé",mil)                                            
            return mil                                                      
        elif tab[mil]>x :                                                   
            # on garde la partie de gauche du tableau
            newtab = Tableau(mil)
            for k in range(mil):
                newtab[k] = tab[k]
            tab = newtab
            return dichoREC(tab,x)
        else:
            newtab = Tableau(mil)
            for k in range(mil):
                newtab[k] = tab[k+mil]
            tab = newtab
            return mil + dichoREC(tab,x) 
    ### END SOLUTION
```

```{code-cell} ipython
---
nbgrader:
  grade: true
  grade_id: cell-bfa0c60e2897b8d4
  locked: true
  points: 5
  schema_version: 3
  solution: false
  task: false
---
assert dichoREC(tableau, 2) == 1
assert dichoREC(tableau, 1) == 0
assert dichoREC(tableau, 57) == 7
assert dichoREC(tableau, 100) == float('inf')
assert dichoREC(tableau, 0) == float('inf')
```

```{code-cell} ipython

```

https://www.cours-gratuit.com/cours-lisp/cours-lisp-les-fonctions-de-base-quote-car-cdr-cons

https://en.wikipedia.org/wiki/CAR_and_CDR
