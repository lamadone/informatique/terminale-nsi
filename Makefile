# system python interpreter. used only to create virtual environment
PY = python3
VENV = ".venv"
BIN=$(VENV)/bin

# make it work on windows too
ifeq ($(OS), Windows_NT)
    BIN=$(VENV)/Scripts
    PY=python
endif


all:
	lint build

$(VENV): requirements.txt requirements-dev.txt
	$(PY) -m venv --system-site-packages $(VENV)
	$(BIN)/pip install --upgrade -r requirements.txt
	$(BIN)/pip install --upgrade -r requirements-dev.txt
	touch $(VENV)

.PHONY: build
build: $(VENV)
	$(BIN)/jb build .

.PHONY: lint
lint: $(VENV)
	$(BIN)/flake8


