---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Arbres binaires

On présente ici les arbres

+++

## Quelques exemples

+++

Le premier exemple qui vient en tête est celui de l'organisation des fichiers sous les systèmes POSIX.

```{code-cell} ipython
!tree -L 1 -d /
```

```{code-cell} ipython
!tree -L 1 -d /usr
```

```{code-cell} ipython
:tags: [remove-input]
from graphviz import Digraph
import os

depart = os.getcwd()
repertoires = Digraph(engine='dot')

repertoires.attr(minlen='2',splines='line')
repertoires.node('/')

for d in os.listdir('/'):
    try:
        os.chdir(os.path.join('/',d))
        repertoires.node(d)
        repertoires.edge('/',d)
    except NotADirectoryError:
        pass
    except PermissionError:
        repertoires.node(d)
        repertoires.edge('/',d)


for d in os.listdir('/usr'):
    try:
        os.chdir(os.path.join('/usr/',d))
        repertoires.node(d)
        repertoires.edge('usr',d)
    except NotADirectoryError:
        pass
    except PermissionError:
        repertoires.node(d)
        repertoires.edge('usr',d)


for d in os.listdir('/home'):
    try:
        os.chdir(os.path.join('/home/',d))
        repertoires.node(d)
        repertoires.edge('home',d)
    except NotADirectoryError:
        pass
    except PermissionError:
        repertoires.node(d)
        repertoires.edge('home',d)

os.chdir(depart)
repertoires
```

```{code-cell} ipython
:tags: [remove-cell]
!rm repertoires
```


+++

On peut présenter des calculs comme un arbre :

```{code-cell} ipython
:tags: [remove-input]
from graphviz import Digraph

dot=Digraph()

dot.attr('node',shape='box',style='rounded')
dot.node('A','+')
dot.node('B','-')
dot.node('I','+')
dot.node('D','×')
dot.node('E','^')
dot.attr('node',shape='diamond',style='rounded')
dot.node('C','1')
dot.node('F','2')
dot.node('G','3')
dot.node('H','2')
dot.node('J','2')
dot.node('K','4')
dot.edges([('A','B'),('A','C'),('B','D'),('B','E'),('E','F'),('E','G'),('D','I'),('D','H'),('I','J'),('I','K')])

dot
```

Les arbres sont des cas particuliers des graphes :

```{code-cell} ipython
:tags: [remove-input]
from graphviz import Graph

metro = Graph()
metro.node('Basilique de Saint-Denis')
metro.node('Mairie de Saint-Ouen')
metro.node('La Fourche')
metro.node('Place de Clichy')
metro.node('Saint-Lazare')
metro.node('Montparnasse')
metro.node('Porte de Clichy')
metro.edges([
    ('Basilique de Saint-Denis','Mairie de Saint-Ouen'),
    ('Mairie de Saint-Ouen','La Fourche'),
    ('Place de Clichy','La Fourche'),
    ('Place de Clichy','Saint-Lazare'),
    ('Saint-Lazare','Montparnasse'),
    ('Mairie de Saint-Ouen','Porte de Clichy'),
    ('Saint-Lazare','Porte de Clichy'),
    ('La Fourche','Porte de Clichy'),
])

metro
```

Au delà de ces quelques exemples, on peut noter quelques points pertinents sur les arbres.

Un autre exemple d'arbre : https://stackoverflow.com/questions/42621190/display-this-decision-tree-with-graphviz

+++

## Définitions

+++

+ Le point de départ d'un arbre est la **racine**
+ Les points nommés sont des **nœuds**
+ Les arêtes entre deux nœuds sont des **branches**
+ si un nœud ne comporte aucune arête qui «descende» vers un autre nœud, on dit que c'est une **feuille**

+++

La **profondeur** d'une feuille (nœud) est le nombre de nœuds traversé de la racine jusqu'à la feuille (nœud). Par exemple, la profondeur de `'^'` dans le deuxième graphe est 3.

La **hauteur** d'un arbre est le maximum des profondeurs.

La **taille** d'un arbre est le nombre de ses nœuds.

+++

Lorsque chaque nœud ne comporte qu'au plus deux branches, on dit que l'arbre est **binaire**.

+++

## Arbres binaires

On peut développer un arbre en considérant, pour chaque nœud autre que les feuilles qu'il se découpe en un sous arbre gauche et un sous arbre droit.

```{code-cell} ipython
:tags: [remove-input]
from graphviz import Digraph

dot=Digraph()


dot.attr('node',shape='box',style='rounded')
dot.node('A','+')
dot.node('B','-')
dot.node('I','+')
dot.node('D','×')
dot.node('E','^')
dot.attr('node',shape='diamond',style='rounded')
dot.node('C','1')
dot.node('F','2')
dot.node('G','3')
dot.node('H','2')
dot.node('J','2')
dot.node('K','4')
dot.edges([('A','B'),('A','C'),('B','D'),('B','E')])


with dot.subgraph(name='cluster_0') as c:
    c.attr(color='blue')
    c.edges([('E','F'),('E','G')])
    
with dot.subgraph(name='cluster_1') as c:
    c.attr(color='red')
    c.edges([('D','I'),('D','H'),('I','J'),('I','K')])
dot
```

On peut ici considérer deux sous-arbres (à gauche en rouge et à droite en bleu) du nœud `'-'`, qui sont ici des arbres binaires eux aussi. Cette propriété possède son importance, pour permettre des parcours récursifs sur les arbres.

+++


