---
jupytext:
  main_language: python
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3.8.6 64-bit
  name: python3
---

# Des graphes avec classes

On va chercher à représenter un graphe, comme structure de données en la
munissant de l'interface suivante :
+ une méthode __voisins__ qui renvoie les voisins d'un sommet donné
+ une méthode __arete__ qui ajoute un arc entre $e_i$ et $e_j$, les créant si nécessaire.
+ une méthode __sommet__ qui renvoie la liste des sommets.

```{code-cell} ipython
class Graph:
    def __init__(self):
        self.sommetList = dict()

    def voisins(self, n):
        if n in self.sommetList:
            return self.sommetList[n]
        else:
            return None

    def ajout_arete(self,o,e):
        if o not in self.sommetList:
            self.sommetList[o] = {e: 1}
        else:
            self.sommetList[o][e] = 1
        if e not in self.sommetList:
            self.sommetList[e] = {}

    def sommet(self):
        return list(self.sommetList.keys())

    def __contains__(self,n):
        return n in self.sommetList

    def __iter__(self):
        return iter(self.sommetList.keys)

    def __repr__(self):
        return str(self.sommetList)

    def __getitem__(self, n):
        return self.voisins(n)
```

```{code-cell} ipython
g = Graph()
```

```{code-cell} ipython
g.ajout_arete('A','B')
```

```{code-cell} ipython
g
```

```{code-cell} ipython
g.ajout_arete('A','E')
```

```{code-cell} ipython
g.addArete('B','C')
```

```{code-cell} ipython
g.ajout_arete('C','D')
```

```{code-cell} ipython
g.ajout_arete('C','E')
```

```{code-cell} ipython
g.ajout_arete('D','A')
```

```{code-cell} ipython
g.ajout_arete('E','D')
```

```{code-cell} ipython
g.ajout_arete('E','B')
```

```{code-cell} ipython
g
```

```{code-cell} ipython
g.sommet()
```

```{code-cell} ipython
g.voisins('A')
```

```{code-cell} ipython
g['A']
```

```{code-cell} ipython
'F' in g
```

```{code-cell} ipython
g['F']
```

