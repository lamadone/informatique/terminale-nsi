# Quelques structures en fonctionel

:::{code-cell} ipython
def lst_ch(sequence=None):
    if sequence:
        return (sequence[0], lst_ch(sequence[1:]))
    else:
        return ()
:::



:::{code-cell} ipython
def tete(liste):
    return liste[0] if liste else None

def queue(liste):
    return liste[1] if liste else ()

def longueur(liste, long=0):
    if liste:
        return longueur(queue(liste), long+1)
    else:
        return long
:::

```pyconsole
>>> lc = lst_ch("abcd")
>>> lc
('a', ('b', ('c', ('d', ()))))
>>> tete(lc)
'a'
>>> queue(lc)
('b', ('c', ('d', ())))
>>> longueur(lc)
4
```


