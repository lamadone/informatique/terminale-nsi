---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Quelques exercices avancés

## Sur les représentations de nombres

:::{prf:definition} code de Gray
Le **code de Gray** est un code binaire qui a pour intérêt de ne modifier
qu'un seul bit entre deux valeurs consécutives. On parle aussi de **nombre
binaire réfléchi**.
:::

Un tel code peut être utilisé pour mesure une rotation pas à pas avec une
cellule, permettant une représentation continue des $N$ nombres choisis.

Le code binaire usuel se nomme **nombre binaire réfléchi**.

```{figure} https://upload.wikimedia.org/wikipedia/commons/9/9b/Encoder_Disc_%283-Bit%29.svg
---
scale: 30%
align: center
---
Une représentation d'un capteur
```

```{exercise-start}
:label: codegray
```

1. Compléter le tableau

    | x | y | z | t | d | c | b | a |
    |:--|-- |-- |-- |-- |-- |-- |--:|
    | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
    | 0 | 0 | 0 | 1 | 0 | 0 | 0 | 1 |
    | 0 | 0 | 1 | 1 | 0 | 0 | 1 | 0 |

2.
    1. Écrire un algorithme permettant de traduire un nombre binaire réfléchi en
        nombre binaire naturel.
    2. Écrire un algorithme permettant de traduire un nombree binaire
       naturel en nombre binaire réfléchi.

3. Combien de codes binaires à 4 bits peut-on imaginer ? Et à $N$ bits ?

```{exercise-end}
```

````{solution} codegray

```{code-block} python3
b = 0b101
bin(b^(b//2))
b = 0b101
bin(b^(b>>1))
```

````

