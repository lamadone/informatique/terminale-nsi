# Structures abstraites de données

## Extrait du programme

```{list-table}
:header-rows: 1
* - Contenu
  - Capacité
  - Commentaires
* - Listes, … : structures linéaires.
  - Distinguer structures par le jeu de méthodes qui les caractérisent.

      Choisir une structure de données adaptée à la situation à modéliser.
  - 
```

## Définition

```{prf:definition} Structure abstraite de donnée
On appelle __structure abstraite de données__ une représentation mentale
(et informatique) de données, définies par :
+ une façon de stocker les données
+ des __primitives__, c'est à dire des fonctions élémentaires sur ces données
```

Ces représentations sont extrêmement importantes, car elles induisent une
efficacité dans le programme lui-même, aussi bien dans sa conception que
dans son exécution. Cependant, celles-ci ont également un coût (du moins
    leur mise en œuvre dans un langage donné). Il est donc essentiel de bien
choisir sa structure de donnée [^1]

[^1]: on parle aussi de __type abstrait de données__ (TAD)
