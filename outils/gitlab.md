---
jupytext:
  text_representation:
    format_name: myst
kernelspec:
  display_name: Python 3
  name: python3
---

# Utilisation de Git

Ce mémo présente assez rapidement les concepts et l'utilisation de Git, en
particulier avec la plateforme https://gitlab.com.


