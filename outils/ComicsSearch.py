import requests
import os

Recherche = input("Moteur de recherche de bandes dessinées dans le domaine de l'informatique.\n-------------------------\nEntrez votre recherche : ")

# Téléchargement de la base -------------------------
print("Téléchargement de la base de données")
requete_base_de_donnees = requests.get("http://mchardine.free.fr/comics/data.txt", allow_redirects=True)
base_de_donnees = str(requete_base_de_donnees.content.decode('utf-8', 'ignore')).split("----/NOUVELLEPAGE----")

# Recherche -------------------------
Compteur = 0
for bulle in base_de_donnees:
    if Recherche.lower() in bulle:
        Compteur+=1
        url = bulle[bulle.index("----NOUVELLEPAGE----")+20:]
        r = requests.get(url, allow_redirects=True)
        open(os.getcwd() + "\\" + Recherche + " - " + url[url.rindex("/")+1:], 'wb').write(r.content)
        print("Téléchargement de", url)

#Affichage -------------------------
print("-------------------------\nRecherche :", Recherche,"-", Compteur, "résultats sauvegardés dans",os.getcwd() )
print("Bonne lecture !")
