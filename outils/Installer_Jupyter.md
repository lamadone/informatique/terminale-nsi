# Installer Python

````{margin}
```{note}
On peut aussi installer les versions
«[Portable](https://www.portabledevapps.net/)» :
  + [Python](https://www.portabledevapps.net/python-portable.php)
  + [Jupyter](https://www.portabledevapps.net/jupyter-portable.php)
```
````

## Installer Python sur ordinateur
```{tabbed} Windows
1. Installer Python depuis [le site officiel](https://python.org)
3. [Installer Jupyter](https://jupyter.org/install)
    ```sh
    pip install jupyter
    ```
    Et pour une version plus moderne
    ```sh
    pip install jupyterlab
    ```
2. Installer VSCode
```

```{tabbed} Mac
1. Je ne sais pas
2. Installer VS Code
```

````{tabbed} GNU/Linux
Jupyter doit être dans les paquets standards de votre distribution.
Sous Debian et dérivées :

1. Installer jupyter
    ```shell
    apt update
    apt install jupyter
    ```
2. Installer le dépot de vscodium
    ```shell
    apt install extrepo
    extrepo enable vscodium
    ```
3. Installer codium
    ```shell
    apt update
    apt install codium
    ```
````


## Installer Python sur ordiphone/tablette

````{tabbed} Android

Installer [Termux](https://play.google.com/store/apps/details?id=com.termux)
ou [depuis le site officiel](https://termux.com).

Dans l'application Termux :

```shell
pkg upgrade
# Install runtime deps
pkg install python libzmq libcrypt
# Add build deps
pkg install python-dev libzmq-dev libcrypt-dev clang
pip3 install -U pip
pip3 install pyzmq --install-option="--zmq=/usr/lib"
pip3 install jupyter
# Remove build deps and pip cache
apt remove python-dev libzmq-dev libcrypt-dev clang
apt autoremove
rm .cache ../usr/var/cache -rf
```

Alternativement, on peut suivre le
[README de ce dépot](https://github.com/Susmigo/jupyter_Notebook_android)
````

```{tabbed} iPad

Installer l'application
[Carnets Jupyter](https://apps.apple.com/fr/app/carnets-jupyter/id1450994949)
```

````{tabbed} Windows Surface
```{admonition} Non testé
:class: warning
Même procédure d'installation que sous Windows : installer Anaconda, …
```
````
