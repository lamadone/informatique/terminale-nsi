---
jupytext:
  text_representation:
    format_name: myst
kernelspec:
  display_name: Python 3
  name: python3
---

# Configurer l'espace de travail pour les projets

Pour travailler idéalement sur les projets en NSI, il est pertinent d'installer quelques outils pour le travail.

On suppose que vous avez suivi les indications de la page [](Installer_Jupyter.md#installer-python).

## Installer des bibliothèques supplémentaires

::::{tabbed} Windows
Si vous utiliser Anaconda, il faut contourner le bug d'OpenSSL :
:::{code} shell
 $env:Path += 'C:\Program files\Anaconda\Library\bin'
:::
::::

::::{tabbed} GNU/Linux
Normalement, rien à faire
::::

::::{tabbed} Mac
Je n'en sais rien, mais normalement, il n'y pas de problème.
::::

## Utiliser les environnements dédiés de Python.

### Créer un environnement dédié de travail

Avant de commencer un nouveau projet qui peut avoir des dépendances
particulières, il est recommandé de cloisonner le développement et les
bibliothèques supplémentaires installés dans un environnement dédié. La
méthode générique consiste à utiliser `venv`, un module de Python.

Dans tous les cas, depuis une invite de commande[^shell] dans le dossier du
projet.
::::{margin}
Ces manipulations sont à faire à chaque fois qu'on commence la session de
travail

Après une session de travail, la commande `deactivate` permet de désactiver
l'environnement virtuel.
::::

:::{code-block} shell
python -m venv --prompt venv-<projet> .venv
:::

::::{tabbed} Windows
:::{code-block} shell
.\.venv\Scripts\Activate.ps1
:::
::::

::::{tabbed} GNU/Linux ou Mac
:::{code-block} shell
source .venv/bin/activate
::::

### Installer automatiquement les dépendances d'une projet

Lors du travail dans un projet, il est souvent nécessaire d'installer des
dépendances et de les spécifier pour les autres collaborateurs/utilisateurs.

Généralement, on place les dépendances dans le fichier `requirements.txt`,
une par ligne.

:::{code-block}
flask
:::

L'installation du paquet se fait avec
::::{margin}
Pour la mise à jour
:::{code-block}
pip install -U -f -r requirements.txt
:::
::::

::::{margin}
:::{note}
Pour les paquets nécessaires pour le développement (`pytest`, `tox`, …) on
utilisera plutôt le fichier `requirements-dev.txt` qui s'installe de la même
façon.
:::
::::


:::{code-block}
pip install -r requirements.txt
:::


### Tester avec tox/pytest

Des [infrastructures de
test](https://lamadone.frama.io/informatique/terminale-nsi/tests_comment_les_utiliser.html) ont été décrites précédement, et si les développeurs (ici le professeur) ont correctement initialisé le projet, celui-ci pourra être testé en lançant la commande `tox`.

La [page
suivante](https://lamadone.frama.io/informatique/terminale-nsi/outils/gitlab.hml)
permettra de présenter comment ce mécanisme peut s'automatiser avec Git et la
CI[^CI].


[^shell]:  PowerShell ou le terminal de GNU/Linux ou Mac
[^CI]: Intégration Continue, un ensemble de mécanismes permettant
  l'automatisation des tests et de la constructions des programmes,
  permettant de vérifier que le livrable est conforme à la spécificiation.
