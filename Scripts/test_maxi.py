import unittest
from maxi_recursif import maxi

class TestMax(unittest.TestCase):
    
    def test_maxi(self):
        T = [1, 2, 1]
        self.assertEqual(2, maxi(T))
