def maxi(T):
    if len(T) == 1:
        return T[0]
    else:
        max_candidat = maxi(T[1:])
        if max_candidat > T[0]:
            return max_candidat
        else:
            return T[0]

T = [1, 2]
assert maxi(T) == 2
