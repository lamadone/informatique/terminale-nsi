---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.0
kernelspec:
  display_name: Python 3 (system-wide)
  language: python
  metadata:
    cocalc:
      description: Python 3 programming language
      priority: 100
      url: https://www.python.org/
  name: python3
  resource_dir: /ext/jupyter/kernels/python3
---

+++ {"nbgrader": {"grade": false, "grade_id": "8ed910", "locked": true, "schema_version": 3, "solution": false, "task": false}}

# Convertir une adresse IPv4 de la notation décimale à la notation binaire
Le but de ce TP est d'écrire des fonctions permettant de cacluler / définir des réseaux à partir d'une adresse et d'une longueur de préfixe (voir le cours).

+++ {"nbgrader": {"grade": false, "grade_id": "7538bc", "locked": true, "schema_version": 3, "solution": false, "task": false}}

Par exemple, l'adresse `192.168.0.1/24` signifie que cette adresse fait partie du réseau `192.168.0.0/24`, qu'il contient $2^{32 - 24} - 2 = 2^8 - 2 = 254 $ adresses, de `192.168.0.1/24` à `192.168.0.254/24`, les adresses `192.168.0.0/24` et `192.168.0.255/24` étant respectivement réservées pour nommer le réseau et l'adresse de [broadcast](https://fr.wikipedia.org/wiki/Broadcast_%28informatique%29).

+++ {"nbgrader": {"grade": false, "grade_id": "bbbad8", "locked": true, "schema_version": 3, "solution": false, "task": false}}

On peut imaginer reproduire la sortie d'un logiciel comme [`ipcalc`](https://gitlab.com/ipcalc/ipcalc)
```shell
% ipcalc 192.168.0.1/24
Address:   192.168.0.1          11000000.10101000.00000000. 00000001
Netmask:   255.255.255.0 = 24   11111111.11111111.11111111. 00000000
Wildcard:  0.0.0.255            00000000.00000000.00000000. 11111111
=>
Network:   192.168.0.0/24       11000000.10101000.00000000. 00000000
HostMin:   192.168.0.1          11000000.10101000.00000000. 00000001
HostMax:   192.168.0.254        11000000.10101000.00000000. 11111110
Broadcast: 192.168.0.255        11000000.10101000.00000000. 11111111
Hosts/Net: 254                   Class C, Private Internet
```

+++ {"nbgrader": {"grade": false, "grade_id": "b263b1", "locked": true, "schema_version": 3, "solution": false, "task": false}}

On peut aussi regarder des modules existants : [ipcalc](https://ipcalc.readthedocs.io/en/latest/)

+++ {"nbgrader": {"grade": false, "grade_id": "5dc997", "locked": true, "schema_version": 3, "solution": false, "task": false}}

## Rappels

+++ {"nbgrader": {"grade": false, "grade_id": "5eae45", "locked": true, "schema_version": 3, "solution": false, "task": false}}

Écrire une fonction `retiregauche()` qui retire les deux premiers caractères d'une chaine de caractère. On rappelle que, si `s = "abc"` est une chaine de caractères, alors :
+ `s[1]` $\to$ `'b'`
+ `s[1:]` $\to$ `'bc'`
+ `s[:1]` $\to$ `'a'`
+ `s + 'd'` $\to$ `'abcd'`
+ `[lettre for lettre in s]` $\to$ `['a','b','c']`

```{code-cell} ipython3
---
nbgrader:
  grade: false
  grade_id: b32567
  locked: false
  schema_version: 3
  solution: true
  task: false
tags:
  - remove-cell
---
def retiregauche(chaine: str) -> str:

    return chaine[2:]

```

```{code-cell} ipython3
---
nbgrader:
  grade: true
  grade_id: a20836
  locked: true
  points: 3
  schema_version: 3
  solution: false
  task: false
---
assert retiregauche('abc') == 'c'
assert retiregauche('gauche') == 'uche'
assert retiregauche('0b11001100') == '11001100'
```

+++ {"nbgrader": {"grade": false, "grade_id": "25a925", "locked": true, "schema_version": 3, "solution": false, "task": false}}

Écrire une fonction `mybin()` qui transforme un nombre entre 0 et 255 en son écriture binaire, les zéros non-significatifs devant être présents (ex: `mydec(8)` $\to$ `'00001000'`)

```{code-cell} ipython3
---
nbgrader:
  grade: false
  grade_id: 429a45
  locked: false
  schema_version: 3
  solution: true
  task: false
---
def mybin(entier: int) -> str:

    if not isinstance(entier, int):
        raise TypeError("L'entrée doit être un entier")
    if entier > 255 or entier < 0:
        raise ValueError("L'entier saisi doit être entre 0 et 255")
    b = retiregauche(bin(entier))
    if len(b) < 8:
        b = '0'*(8 - len(b)) + b
    return b

```

```{code-cell} ipython3
---
nbgrader:
  grade: true
  grade_id: c654d7
  locked: true
  points: 3
  schema_version: 3
  solution: false
  task: false
---
assert mybin(0) == '00000000'
assert mybin(255) == '11111111'
assert mybin(8) == '00001000'
```

```{code-cell} ipython3
---
nbgrader:
  grade: true
  grade_id: 3dea72
  locked: true
  points: 1
  schema_version: 3
  solution: false
  task: false
---
### BEGIN HIDDEN TESTS
from nose.tools import assert_raises
assert_raises(TypeError, mybin, '8')
assert_raises(ValueError, mybin, -1)
assert_raises(ValueError, mybin, 256)
### END HIDDEN TESTS
```

+++ {"nbgrader": {"grade": false, "grade_id": "87daf6", "locked": true, "schema_version": 3, "solution": false, "task": false}}

Écrire une fonction `mydec()` qui prend un entier écrit en binaire (sous forme d'un string de '0' et '1') et le convertit en un nombre décimal.

```{code-cell} ipython3
---
nbgrader:
  grade: false
  grade_id: f634c3
  locked: false
  schema_version: 3
  solution: true
  task: false
---
def mydec(binaire: str) -> int:

    if not isinstance(binaire, str):
        raise TypeError("L'entrée doit être une chaine de caractère")
    if not set(binaire).issubset({'1', '0'}):
        raise ValueError("L'entrée doit excusivement contenir des 0 et des 1")
    return int('0b'+binaire,2)

```

```{code-cell} ipython3
---
nbgrader:
  grade: true
  grade_id: c4f8b5
  locked: true
  points: 3
  schema_version: 3
  solution: false
  task: false
---
assert mydec('00000000') == 0
assert mydec('11111111') == 255
assert mydec('00001000') == 8
```

```{code-cell} ipython3
---
nbgrader:
  grade: true
  grade_id: 7a38ca
  locked: true
  points: 0
  schema_version: 3
  solution: false
  task: false
---
### BEGIN HIDDEN TESTS
from nose.tools import assert_raises
assert_raises(TypeError, mydec, 10)
assert_raises(ValueError, mydec, '0b10')
### END HIDDEN TESTS
```

+++ {"nbgrader": {"grade": false, "grade_id": "bea0ca", "locked": true, "schema_version": 3, "solution": false, "task": false}}

## Conversion d'adresses IPv4

On va désormais chercher à convertir des adresses IPv4 en leur notation binaire correspondante. On pourra se servir des fonctions `mybin()` et `mydec()`

+++ {"nbgrader": {"grade": false, "grade_id": "cc5677", "locked": true, "schema_version": 3, "solution": false, "task": false}}

Écrire une fonction qui transforme une adresse IPv4 en une liste d'entiers.

```{code-cell} ipython3
---
nbgrader:
  grade: false
  grade_id: 703a4f
  locked: false
  schema_version: 3
  solution: true
  task: false
---
def ip_int_list(ip: str) -> list:

    if not isinstance(ip, str):
        raise TypeError
    if len(ip) > 15:
        raise ValueError
    liste = ['']
    for element in ip:
        if element != '.':
            liste[-1] = liste[-1] + element
        else:
            liste = liste + ['']
    for indice,element in enumerate(liste):
        liste[indice] = int(element)
        if int(element) > 255:
            raise ValueError
    return liste

```

```{code-cell} ipython3
---
nbgrader:
  grade: true
  grade_id: 890efa
  locked: true
  points: 3
  schema_version: 3
  solution: false
  task: false
---
assert ip_int_list('8.8.8.8') == [8, 8, 8, 8]
assert ip_int_list('192.168.210.4') == [192, 168, 210, 4]
assert ip_int_list('127.0.0.1') == [127, 0, 0, 1]
```

```{code-cell} ipython3
---
nbgrader:
  grade: true
  grade_id: b8cff1
  locked: true
  points: 0
  schema_version: 3
  solution: false
  task: false
---
### BEGIN HIDDEN TESTS
from nose.tools import assert_raises
assert_raises(TypeError, ip_int_list, 192)
assert_raises(ValueError, ip_int_list, '192.168.260.4')
### END HIDDEN TESTS
```

+++ {"nbgrader": {"grade": false, "grade_id": "b915e1", "locked": true, "schema_version": 3, "solution": false, "task": false}}

Écrire une fonction qui donne renvoie une chaine contenant $n$ 1 consécutifs, suivi de $32 - n$ 0.

```{code-cell} ipython3
---
nbgrader:
  grade: false
  grade_id: 10e015
  locked: false
  schema_version: 3
  solution: true
  task: false
---
def prefix_len(n: int) -> str:
    return '1'*n + '0'*(32 - n)
```

```{code-cell} ipython3
---
nbgrader:
  grade: true
  grade_id: 5556a5
  locked: true
  points: 3
  schema_version: 3
  solution: false
  task: false
---
assert prefix_len(32) == '11111111111111111111111111111111'
assert prefix_len(24) == '11111111111111111111111100000000'
assert prefix_len(8) == '11111111000000000000000000000000'
```

+++ {"nbgrader": {"grade": false, "grade_id": "a15077", "locked": true, "schema_version": 3, "solution": false, "task": false}}

Écrire une fonction qui sépare une chaine de la forme `a/b` en un tuple `(a, b)`

```{code-cell} ipython3
---
nbgrader:
  grade: false
  grade_id: 9fcea7
  locked: false
  schema_version: 3
  solution: true
  task: false
---
def ip_prefixlen(ip: str) -> tuple:

    if not isinstance(ip, str):
        raise TypeError
    liste = ['']
    for element in ip:
        if element != '/':
            liste[-1] = liste[-1] + element
        else:
            liste = liste + ['']
    return tuple(liste)

```

```{code-cell} ipython3
---
nbgrader:
  grade: true
  grade_id: '268724'
  locked: true
  points: 1
  schema_version: 3
  solution: false
  task: false
---
assert ip_prefixlen('192.168.210.4/24') == ('192.168.210.4', '24')
```

+++ {"nbgrader": {"grade": false, "grade_id": "956f88", "locked": true, "schema_version": 3, "solution": false, "task": false}}

Écrire une fonction qui affiche les composantes d'une adresse IPv4 en notation binaire.

```{code-cell} ipython3
---
nbgrader:
  grade: false
  grade_id: ead0e0
  locked: false
  schema_version: 3
  solution: true
  task: false
---
def ip_list_bin(ip: str) -> str:

    s = ''
    for element in ip_int_list(ip):
        s = s + mybin(element)
    return s

```

```{code-cell} ipython3
---
nbgrader:
  grade: true
  grade_id: c9fc59
  locked: true
  points: 3
  schema_version: 3
  solution: false
  task: false
---
assert ip_list_bin('192.168.210.4') == '11000000101010001101001000000100'
assert ip_list_bin('127.0.0.1') == '01111111000000000000000000000001'
assert ip_list_bin('8.8.8.8') == '00001000000010000000100000001000'
```

+++ {"nbgrader": {"grade": false, "grade_id": "5b1f94", "locked": true, "schema_version": 3, "solution": false, "task": false}}

Écrire une fonction qui remplace les bits de la première chaine par des 0 lorsqu'ils sont à 0 dans la deuxième chaine.

```{code-cell} ipython3
---
nbgrader:
  grade: false
  grade_id: 52577b
  locked: false
  schema_version: 3
  solution: true
  task: false
---
def ou(chaine1: str, chaine2: str) -> str:

    if not set(chaine1).issubset({'1', '0'}):
        raise ValueError
    if not set(chaine2).issubset({'1', '0'}):
        raise ValueError
    if len(chaine1) != len(chaine2):
        raise ValueError
    s = ''
    for indice in range(len(chaine1)):
        if chaine2[indice] == '1':
            s = s + chaine1[indice]
        else:
            s = s + '0'
    return s

```

```{code-cell} ipython3
---
nbgrader:
  grade: true
  grade_id: 27c521
  locked: true
  points: 1
  schema_version: 3
  solution: false
  task: false
---
assert ou('11000000101010001101001000000100','11111111111111111111111100000000') == '11000000101010001101001000000000'
```

+++ {"nbgrader": {"grade": false, "grade_id": "947e03", "locked": true, "schema_version": 3, "solution": false, "task": false}}

Écrire une fonction qui remplace les bits de la première chaine par des 1 lorsqu'ils sont à 0 dans la deuxième chaine.

```{code-cell} ipython3
---
nbgrader:
  grade: false
  grade_id: e4c73e
  locked: false
  schema_version: 3
  solution: true
  task: false
---
def ouc(chaine1: str, chaine2: str) -> str:

    if not set(chaine1).issubset({'1', '0'}):
        raise ValueError
    if not set(chaine2).issubset({'1', '0'}):
        raise ValueError
    if len(chaine1) != len(chaine2):
        raise ValueError
    s = ''
    for indice in range(len(chaine1)):
        if chaine2[indice] == '1':
            s = s + chaine1[indice]
        else:
            s = s + '1'
    return s

```

```{code-cell} ipython3
---
nbgrader:
  grade: true
  grade_id: ab176c
  locked: true
  points: 1
  schema_version: 3
  solution: false
  task: false
---
assert ouc('11000000101010001101001000000100','11111111111111111111111100000000') == '11000000101010001101001011111111'
```

+++ {"nbgrader": {"grade": false, "grade_id": "aac4d6", "locked": true, "schema_version": 3, "solution": false, "task": false}}

Écrire une fonction qui sépare en octets de 8 bits et donne la transformation décimale d'une séquence de 32 bits

```{code-cell} ipython3
---
nbgrader:
  grade: false
  grade_id: 8b72ba
  locked: false
  schema_version: 3
  solution: true
  task: false
---
def bin2ip(binaire: str) -> list:

    octets = (binaire[i*8:(i+1)*8] for i in range(4))
    return [mydec(octet) for octet in octets]

```

```{code-cell} ipython3
---
nbgrader:
  grade: true
  grade_id: 8cb950
  locked: true
  points: 2
  schema_version: 3
  solution: false
  task: false
---
assert bin2ip('11000000101010001101001000000000') == [192, 168, 210, 0]
assert bin2ip('11000000101010001101001000000100') == [192, 168, 210, 4]
```

+++ {"nbgrader": {"grade": false, "grade_id": "226c19", "locked": true, "schema_version": 3, "solution": false, "task": false}}

## Conclusion

On cherche à afficher les informations liés à une addresse (avec préfixe donné).

```{code-cell} ipython3
---
nbgrader:
  grade: false
  grade_id: aeb684
  locked: false
  schema_version: 3
  solution: true
  task: false
---
def print_network(ip):

    liste = bin2ip(ou(ip_list_bin(ip_prefixlen(ip)[0]),prefix_len(int(ip_prefixlen(ip)[1]))))
    s = f"{liste[0]}.{liste[1]}.{liste[2]}.{liste[3]}/{ip_prefixlen(ip)[1]}"
    return s

```

```{code-cell} ipython3
---
nbgrader:
  grade: true
  grade_id: '710312'
  locked: true
  points: 1
  schema_version: 3
  solution: false
  task: false
---
assert print_network("192.168.210.4/24") == '192.168.210.0/24'
```

```{code-cell} ipython3
---
nbgrader:
  grade: false
  grade_id: 1a8549
  locked: false
  schema_version: 3
  solution: true
  task: false
---
def print_broadcast(ip):

    liste = bin2ip(ouc(ip_list_bin(ip_prefixlen(ip)[0]),prefix_len(int(ip_prefixlen(ip)[1]))))
    s = f"{liste[0]}.{liste[1]}.{liste[2]}.{liste[3]}"
    return s

```

```{code-cell} ipython3
---
nbgrader:
  grade: true
  grade_id: 757a91
  locked: true
  points: 1
  schema_version: 3
  solution: false
  task: false
---
assert print_broadcast("192.168.210.4/24") == '192.168.210.255'
assert print_broadcast("172.16.0.0/12") == '172.31.255.255'
```

```{code-cell} ipython3
---
nbgrader:
  grade: false
  grade_id: 1e7443
  locked: false
  schema_version: 3
  solution: true
  task: false
---
def print_hostmin(ip):
    liste = bin2ip(ou(ip_list_bin(ip_prefixlen(ip)[0]),prefix_len(int(ip_prefixlen(ip)[1]))))
    s = f"{liste[0]}.{liste[1]}.{liste[2]}.{liste[3] + 1}"
    return s
```

```{code-cell} ipython3
---
nbgrader:
  grade: true
  grade_id: 2b3368
  locked: true
  points: 1
  schema_version: 3
  solution: false
  task: false
---
assert print_hostmin('172.27.12.14/12') == "172.16.0.1"
```

```{code-cell} ipython3
---
nbgrader:
  grade: false
  grade_id: 475dac
  locked: false
  schema_version: 3
  solution: true
  task: false
---
def print_hostmax(ip):
    liste = bin2ip(ouc(ip_list_bin(ip_prefixlen(ip)[0]),prefix_len(int(ip_prefixlen(ip)[1]))))
    s = f"{liste[0]}.{liste[1]}.{liste[2]}.{liste[3] - 1}"
    return s
```

```{code-cell} ipython3
---
nbgrader:
  grade: true
  grade_id: 7f4009
  locked: true
  points: 1
  schema_version: 3
  solution: false
  task: false
---
assert print_hostmax("192.168.210.4/24") == '192.168.210.254'
```
