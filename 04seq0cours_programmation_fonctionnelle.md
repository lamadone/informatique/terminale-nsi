---
jupytext:
  main_language: python
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3
  name: python3
  language: python
---

# Programmation fonctionnelle

La programmation fonctionnelle est un paradigme[^paradigme] de programmation
consistant à considérer l'éxécution d'un programme comme l'application
successives de fonctions à une entrée permettant une sortie.

Il s'agit d'un des premiers paradigmes de programmation, issu des travaux
d'Alonzo Church sur le $\lambda$-calcul dans les années 1920/1930. À ce
titre, LISP, un des premiers langages de programmation, inventé par John
McCarthy fonctionne essentiellement suivant ce principe. Les zélotes de la
programmation fonctionnelle considèrent même que l'affectation est
quasi-inutile en programmation fonctionnelle.

Un langage fonctionnel comme LISP est basé sur sept primitives :
1. `cons`
2. `car`
3. `cdr`
4. `is`
5. `t`
6. 
7. 

```{code-cell} ipython
def que_des_pommes(sac):
    return all(fruit == "pomme" for fruit in sac)
```

```{code-cell} ipython
import functools
f=lambda s:functools.reduce(lambda x,y:x and y=="pomme",s,True)
```

```{code-cell} ipython
que_des_pommes = lambda input,print="pomme":all(int==print for int in input)
```

[^paradigme]: façon de faire les choses
