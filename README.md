Ajouter ;C:\Program Files\Anaconda\Library\bin à $env:Path

Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope CurrentUser

Reste à traiter avant mars :

  + Arbres binaires de recherche et algos associés (Recherche et insertion d'une clef, équilibrage ?)
  + Méthode diviser pour régner (~tri fusion,~ rotation quart de tour d'une image bitmap)
  + Modularité (écrire des modules, utiliser une API)
  + Structure de données graphes (sans les algos, à voir)


Faire un rappel sur KNN et les tris avant l'épreuve

En cours de traitement

  + ~Protocoles de routage (exposés, pour les TP, à voir avec Cisco Packet Tracer)~
  + ~Composants intégrés d'un SoC~
  + ~Gestion des processus (exposé, voir utilisation de psutil)~


A traiter après mars :
  + Aglos sur les graphes
  + Notion de programme en tant que donnée.
  + Calculabilité, décidabilité
  + Paradigme de programmation (mais déjà évoqué à plusieurs reprises)
  + Programmation dynamique
  + Sécurisation des protocoles de communications (exposés, pas absolument nécessaire, mais bon)
  + Recherche textuelle
  + RSA en projet pour Evan, Kevin, Moustafa, Noé, Sarusan et Achour)
  + QRCode en projet pour les même
