---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.0
kernelspec:
  display_name: Python 3 (system-wide)
  language: python
  name: python3
---

# Projet CV

Le but de ce projet est de manipuler quelques éléments de sql en contexte
«réel» et de réinvestir les connaissances de Première sur le web.

Pour commencer, on peut suivre le [TP sur
Flask](https://lamadone.frama.io/informatique/premiere-nsi/4_IHM/index.html)
avec ses commentaires sur l'[utilisation directe depuis
Jupyter](https://lamadone.frama.io/informatique/premiere-nsi/4_IHM/web_dynamique_python.html)

On peut aussi consulter [la page d'installation des environnements
virtuels](https://lamadone.frama.io/informatique/terminale-nsi/outils/Configurer%20son%20espace%20de%20travail.html)

Pour la deuxième partie du TP, on va mettre en œuvre une base de données qui
suit ce schéma :

```{code-cell} ipython
:tags: [remove-cell]
reload_ext mocodo_magic
```

```{code-cell} ipython
:tags: [remove-input]
%mocodo --input cv.mcd
```

On a donc le diagramme de relation suivant :

```{code-cell} ipython
:tags: [remove-cell]
%mocodo --relations diagram --input cv.mcd
```

```{code-cell} ipython
:tags: [remove-input]
%mocodo --input mocodo_notebook/cv.mld
```

Celui-ci se traduit en SQL (dialecte sqlite) de la façon suivante.
::::{margin}
:::{note}
Ce code pourra directement servir à initialiser la base de données du
projet.
:::
::::

```{code-cell} ipython
:tags: [remove-cell]
%mocodo --title=cv --relations=sqlite --input cv.mcd
```

```{code-cell} ipython
:tags: [remove-cell]
!sed -i 's/.open "CV";/.open "CV.sqlite"/' mocodo_notebook/cv_sqlite.sql
```


```{code-cell} ipython
:tags: [remove-input]
from IPython.display import Code
Code(filename='mocodo_notebook/cv_sqlite.sql',language='sql')
```

Pour la partie HTML, l'utilisation de
[Flask](https://flask.palletsprojects.com) implique l'utilisation de
[Jinja pour les patrons de page](https://jinja.palletsprojects.com/en/3.0.x/templates/).

La page initiale du projet distribuée, donne quelques éléments de syntaxe.

Rendez-vous sur la [page du projet](https://gitlab.com/la-madone/nsi-terminale/cv)
et vous devrez compléter la base de données (voir le fichier
`init_data.sql` qui contiendra les commandes sql pour remplir les tables) et
compléter le patron de la page web, en utilisant Jinja.

Pour la connexion à la base de données sqlite, on peut consulter
https://flask.palletsprojects.com/en/2.0.x/patterns/sqlite3/

Vous rendrez un compte rendu des vos réussites et de vos échecs dans le [dépot suivant](https://nuage-paris.beta.education.fr/s/ReaECJiZogZNWtN) avant le 6 janvier 2022.

```{warning}
Attention, une fois passé la date de demain spécifiée, ce dépôt ne sera plus accessible.
```
% Pour la correction
% SELECT nom,prenom,libellecompetence,niveau from CANDIDAT JOIN SAVOIR USING (idcandidat) JOIN COMPETENCE USING (idcompetence) ;

````{hint}
Pour résumer la lecture de [la page sur la configuration de l'espace de travail](outils/Configurer son espace de travail.md), dans le PowerShell de Windows, depuis le dossier `CV`.
:::{code-block}
$env:Path += 'C:\Program files\Anaconda\Library\bin'
python -m venv --prompt CV .venv
.\.venv\Scripts\Activate.ps1
:::
permet d'installer l'environnement virtuel.

:::{code-block}
pip install flask tox pytest flake8
:::
permet d'installer les paquets nécessaires dans l'environnement virtuel.

:::{code-block}
tox
:::
permet de tester le code.

:::{code-block}
python cv.py
:::
permet de visualiser votre production.
````
