# Mise en œuvre d'un conteneur spécialisé pour les dictionnaires et les ensembles

En s'aidant de [ce résumé sur le fonctionnement interne de Python](https://www.fluentpython.com/extra/internals-of-sets-and-dicts/), écrire un module `structures` permettant d'important les structures de données `dictionnaire` et `ensemble`.

L'interface des ces classes sera similaire à l'interface de `dict` et `set` et cette similarité sera démontrée par un jeu de test proposé.


