---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Algorithme de Douglas Peucker

## Descriptif

L’algorithme de Ramer-Douglas-Peucker sert à simplifier un polygone ou une
polyligne par la suppression de points. L'algorithme a été publié par David
H. Douglas et Thomas K. Peucker en 1973. Il est utilisé en compression de
données vectorielles et en généralisation cartographique.

On trouve un usage de celui-ci dans des logiciels de cartographie ou de suivi de traces GPS

![](filtre_dp.png)

![](trace_sans_filtre_5m_noir_10m_magenta.png)

## Algorithme de type «diviser pour régner»

On va travailler de façon récursive :
1. On choisit les extrémités du chemin à simplifier.
2. On parcourt les points pour trouver celui qui est le plus éloigné
3. Si cette distance est inférieure à un certain seuil, on supprime les
   points entre les bornes.
4. Si cette distance est supérieure au seuil fixé, on applique l'algorithme
   de Douglas-Peucker aux deux polylignes.

```{code-cell} ipython
:tags: [remove-cell]
from myst_nb import glue
```

```{code-cell} ipython
:tags: [remove-cell]
import pysketcher as ps
from pysketcher.backend.matplotlib import MatplotlibBackend
from math import sqrt
from random import random

points = [ps.Point(0.0, 2.5) ] + [ps.Point(float(i), float(random()*2.5 + 2.5)) for i in range(1,19)] + [ps.Point(20.0, 2.5)]

# points = [ps.Point(0.e+00, 2.5e+00),
#  ps.Point(1.e+00, 5.e+00),
#  ps.Point(2.e+00, 4.e+00),
#  ps.Point(3.e+00, 2.e+00),
#  ps.Point(4.e+00, 3.e+00),
#  ps.Point(5.e+00, 1.e+00),
#  ps.Point(6.e+00, 2.e+00),
#  ps.Point(7.e+00, 5.e+00),
#  ps.Point(8.e+00, 1.e+00),
#  ps.Point(9.e+00, 2.e+00),
#  ps.Point(1.e+01, 3.e+00),
#  ps.Point(1.1e+01, 4.e+00),
#  ps.Point(1.2e+01, 5.e+00),
#  ps.Point(1.3e+01, 0.e+00),
#  ps.Point(1.4e+01, 0.e+00),
#  ps.Point(1.5e+01, 2.e+00),
#  ps.Point(1.6e+01, 3.e+00),
#  ps.Point(1.7e+01, 2.e+00),
#  ps.Point(1.8e+01, 0.e+00),
#  ps.Point(2.e+01, 2.5e+00)]

def_segments = lambda points: [ps.Line(points[i],points[i + 1]) for i in range(len(points) - 1)]

segments = def_segments(points)
```

Prenons, par exemple, une polyligne.
```{code-cell} ipython
:tags: [remove-input]
figure = ps.Figure(0.0, 20.0, -0.1, 5.1, MatplotlibBackend)


for segment in segments:
    figure.add(segment)

extremites = ps.Line(points[0],points[-1])
extremites.style.line_color = ps.Style.Color.BLUE
figure.add(extremites)
```

```{code-cell} ipython
:tags: [remove-cell]
def echantillonne_points_entre_extremites(points):
    debut = points[0]
    fin = points[-1]
    m = (fin.y - debut.y)/(fin.x - debut.x)
    p = debut.y - m*debut.x
    sortie = [ps.Point(point.x, point.x * m + p) for point in points]
    return sortie
```

Pour calculer la distance entre un point et la ligne, on peut utiliser par exemple la [formule de Héron](https://fr.wikipedia.org/wiki/H%C3%A9ron_d%27Alexandrie#Formule_de_H%C3%A9ron).

```{prf:proposition}
Soit un triangle dont les côtés sont de mesure $a$, $b$ et $c$. Soit $p = 1/2(a + b +c)$ son demi-périmètre. Alors, son aire $A$ s'exprime
$$ A = \sqrt{p (p - a)(p - b)(p - c) } $$
```

En utilisant une autre formule pour l'aire d'un triangle, on peut donc trouver la hauteur.

On peut désormais simplifier les polylignes.

```{code-cell} ipython
:tags: [remove-cell]
from math import sqrt
def distance(debut, fin, intermediaire):
    a = sqrt((debut.x - intermediaire.x)**2 + (debut.y - intermediaire.y)**2)
    b = sqrt((fin.x - intermediaire.x)**2 + (fin.y - intermediaire.y)**2)
    c = sqrt((debut.x - fin.x)**2 + (debut.y - fin.y)**2)
    p = 1/2*(a + b + c)
    A = sqrt(p*(p - a)*(p - b)*(p - c))
    h = 2*A/c
    return h
```

On a par exemple :
```{code-cell} ipython
n = 8
distance(points[:n][0],points[:n][-1],points[:n][2])
```

```{code-cell} ipython
:tags: [remove-cell]
def trouve_premier_point_eloigne(points):
    distance_max = 0
    indice = 0
    indice_max = 0
    for point in points:
        distance_courante = distance(points[0],points[-1],points[indice])
        if distance_courante < distance_max:
            indice = indice + 1
        else:
            distance_max = distance_courante
            indice = indice + 1
            indice_max = indice
    return indice_max
```

On cherche ensuite l'indice du point le plus éloigné.

```{code-cell} ipython
:tags: [remove-cell]
n = trouve_premier_point_eloigne(points)
glue('n',n)
```

Ici, on trouve `n=`{glue:}`n`

```{code-cell} ipython
:tags: [remove-cell]
points_a = echantillonne_points_entre_extremites(points[:n])
points_b = echantillonne_points_entre_extremites(points[(n-1):])
```

```{code-cell} ipython
:tags: [remove-input]
figure = ps.Figure(0.0, 20.0, -0.1, 5.1, MatplotlibBackend)


for segment in segments:
    figure.add(segment)

extremites = ps.Line(points[0],points[-1])
extremites.style.line_color = ps.Style.Color.BLUE
figure.add(extremites)

segments = def_segments(points_a)

for segment in segments:
    segment.style.line_color = ps.Style.Color.RED
    figure.add(segment)

segments = def_segments(points_b)

for segment in segments:
    segment.style.line_color = ps.Style.Color.RED
    figure.add(segment)
```

On peut désormais considérer le problème sur les deux sous parties de la ligne :
```{code-cell} ipython
:tags: [remove-input]
figure = ps.Figure(0.0, 20.0, -0.1, 5.1, MatplotlibBackend)

segments = def_segments(points[:n])

for segment in segments:
    figure.add(segment)

extremites = ps.Line(points[0],ps.Point(points[n-1].x,2.5))
extremites.style.line_color = ps.Style.Color.BLUE
figure.add(extremites)

segments = def_segments(points_a)

for segment in segments:
    segment.style.line_color = ps.Style.Color.RED
    figure.add(segment)
```
et
```{code-cell} ipython
:tags: [remove-input]
figure = ps.Figure(0.0, 20.0, -0.1, 5.1, MatplotlibBackend)

segments = def_segments(points[n-1:])

for segment in segments:
    figure.add(segment)

extremites = ps.Line(ps.Point(points[n].x,2.5),points[-1])
extremites.style.line_color = ps.Style.Color.BLUE
figure.add(extremites)

segments = def_segments(points_b)

for segment in segments:
    segment.style.line_color = ps.Style.Color.RED
    figure.add(segment)
```

```{code-cell} ipython
:tags: [remove-cell]
def retirer_points(points, seuil):
    nouveaux_points = [points[0]]
    for indice in range(1,len(points) - 2):
        if distance(points[0], points[-1], points[indice]) < seuil:
            pass
        else:
            nouveaux_points.append(points[indice])
    nouveaux_points.append(points[-1])
    return nouveaux_points
```
```{code-cell} ipython
:tags: [remove-cell]
points_gauche_nettoye = retirer_points(points[:n], 0.5)
points_droite_nettoye = retirer_points(points[n-1:], 0.5)
```

```{code-cell} ipython
:tags: [remove-input]
figure = ps.Figure(0.0, 20.0, -0.1, 5.1, MatplotlibBackend)

segments = def_segments(points[:n])
for segment in segments:
    figure.add(segment)

segments = def_segments(points_gauche_nettoye)

for segment in segments:
    segment.style.line_color = ps.Style.Color.MAGENTA
    figure.add(segment)

extremites = ps.Line(points[0],ps.Point(points[n-1].x,2.5))
extremites.style.line_color = ps.Style.Color.BLUE
figure.add(extremites)

segments = def_segments(points_a)

for segment in segments:
    segment.style.line_color = ps.Style.Color.RED
    figure.add(segment)
```
et
```{code-cell} ipython
:tags: [remove-input]
figure = ps.Figure(0.0, 20.0, -0.1, 5.1, MatplotlibBackend)

segments = def_segments(points[n-1:])
for segment in segments:
    figure.add(segment)

segments = def_segments(points_droite_nettoye)
for segment in segments:
    segment.style.line_color = ps.Style.Color.MAGENTA
    figure.add(segment)

extremites = ps.Line(ps.Point(points[n-1].x,2.5),points[-1])
extremites.style.line_color = ps.Style.Color.BLUE
figure.add(extremites)

segments = def_segments(points_b)

for segment in segments:
    segment.style.line_color = ps.Style.Color.RED
    figure.add(segment)
```

On peut désormais traiter le cas où il faut continuer à l'étape suivante.

```{code-cell} ipython
:tags: [remove-cell]
n = trouve_premier_point_eloigne(points_droite_nettoye)
points_a = echantillonne_points_entre_extremites(points_droite_nettoye[:n])
points_b = echantillonne_points_entre_extremites(points_droite_nettoye[(n-1):])
```

```{code-cell} ipython
:tags: [remove-input]
figure = ps.Figure(0.0, 20.0, -0.1, 5.1, MatplotlibBackend)

segments = def_segments(points_droite_nettoye[:n])

for segment in segments:
    figure.add(segment)

extremites = ps.Line(points_droite_nettoye[0],ps.Point(points_droite_nettoye[n-1].x,2.5))
extremites.style.line_color = ps.Style.Color.BLUE
figure.add(extremites)

segments = def_segments(points_a)

for segment in segments:
    segment.style.line_color = ps.Style.Color.RED
    figure.add(segment)
```
et
```{code-cell} ipython
:tags: [remove-input]
figure = ps.Figure(0.0, 20.0, -0.1, 5.1, MatplotlibBackend)

segments = def_segments(points_droite_nettoye[n-1:])

for segment in segments:
    figure.add(segment)

extremites = ps.Line(ps.Point(points_droite_nettoye[n-1].x,2.5),points_droite_nettoye[-1])
extremites.style.line_color = ps.Style.Color.BLUE
figure.add(extremites)

segments = def_segments(points_b)

for segment in segments:
    segment.style.line_color = ps.Style.Color.RED
    figure.add(segment)
```

On peut aussi simplifier les chemins.


```{code-cell} ipython
:tags: [remove-cell]
points = list(points_droite_nettoye)
points_gauche_nettoye = retirer_points(points[:n], 0.5)
points_droite_nettoye = retirer_points(points[n-1:], 0.5)
```

```{code-cell} ipython
:tags: [remove-input]
figure = ps.Figure(0.0, 20.0, -0.1, 5.1, MatplotlibBackend)

segments = def_segments(points[:n])
for segment in segments:
    figure.add(segment)

segments = def_segments(points_gauche_nettoye)

for segment in segments:
    segment.style.line_color = ps.Style.Color.MAGENTA
    figure.add(segment)

extremites = ps.Line(points[0],ps.Point(points[n-1].x,2.5))
extremites.style.line_color = ps.Style.Color.BLUE
figure.add(extremites)

segments = def_segments(points_a)

for segment in segments:
    segment.style.line_color = ps.Style.Color.RED
    figure.add(segment)
```
et
```{code-cell} ipython
:tags: [remove-input]
figure = ps.Figure(0.0, 20.0, -0.1, 5.1, MatplotlibBackend)

segments = def_segments(points[n-1:])
for segment in segments:
    figure.add(segment)

segments = def_segments(points_droite_nettoye)
for segment in segments:
    segment.style.line_color = ps.Style.Color.MAGENTA
    figure.add(segment)

extremites = ps.Line(ps.Point(points[n-1].x,2.5),points[-1])
extremites.style.line_color = ps.Style.Color.BLUE
figure.add(extremites)

segments = def_segments(points_b)

for segment in segments:
    segment.style.line_color = ps.Style.Color.RED
    figure.add(segment)
```

Et recommencer, quand c'est nécessaire.

## Travail demandé :

Étudier et mettre en œuvre l'algorithme proposé.

La mise en œuvre devra s'appuyer sur des exemples fournit par l'auteur.

Esquisser la preuve du calcul de la complexité et de la
terminaison/correction.

## Échéancier

+ Semaine 4 : proposition d'exemples ; écriture d'une fonction distance
+ Semaine 5 : programmation de l'algorithme ; écriture de quelques tests
+ Semaine 6 : rédaction d'un rapport

Quelques éléments pour avancer : https://gitlab.com/la-madone/douglas-peucker

% https://fr.wikipedia.org/wiki/Algorithme_de_Douglas-Peucker
% https://www.tandfonline.com/doi/abs/10.1179/caj.1966.3.1.10
% https://bost.ocks.org/mike/simplify/
% https://pypi.org/project/curvereduce/
% https://pypi.org/project/pyrdp/
% https://pypi.org/project/crdp/
% https://pypi.org/project/rdp-algo/
% https://pypi.org/project/rdp-quick/
% https://pypi.org/project/rdp/

