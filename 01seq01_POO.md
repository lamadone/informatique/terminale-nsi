---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Programmation orientée objet

+++

## Extrait du programme

```{list-table}
:header-rows: 1
* - Contenu
  - Capacité
  - Commentaires
* - Vocabulaire de la programmation objet :
    classes, attributs, méthodes, objets.
  - Écrire la définition d'une classe.

      Accéder aux attributs et méthodes d'une classe.
  - On n'aborde pas ici tous les aspects de la programmation objet comme le polymorphisme et l'héritage.
```


## Vocabulaire utilisé

Dans l'exemple des listes, on utilise la programmation orientée objet (POO) pour réaliser les «objets» nécessaires. Il s'agit d'un _paradigme_ de programmation que nous étudierons plus en détail dans l'année.

La programmation orientée objet à pour but d'abstraire le monde physique en représentant ses objets par des propriétés (attributs) et des opérations (on parle aussi de méthodes). Autrement dit, il s'agit de modéliser un objet de la vie réelle par ses propriétés et par ce qu'il peut faire. Certaines propriétés sont fixées à la création, d'autres peuvent être modifiées dans le cycle de vie de l'objet, et c'est souvent au travers des méthodes qu'elles le sont.

Un des concepts clefs de la programmation objet est la définition de _Classes_ qui définissent des catégories d'objets, alors que l'objet lui est défini comme une _instance_ de classe.

Voyons ça sur quelques exemples :

```{code-cell} ipython
class Chien:
    def __init__(self):
        pass
```

```{code-cell} ipython
Belle = Chien()
```

```{code-cell} ipython
Belle
```

```{code-cell} ipython
type(Belle)
```

On utilise le mot clef __` `__ pour définir une classe d'objets.

La première fonction (`def __init__(self, nom):`) est le _initiateur_ de la classe, qui explicite comment initialiser l'objet, et qui précise, ce que nous allons stocker comme _attributs_ de classe. Pour l'instant, celui-ci ne fait rien. La variable `self` est une référence à l'objet lui même.

```{code-cell} ipython
def __init__(self, age):
    self.age = age
```

```{code-cell} ipython
:tags: [remove-cell]

Chien.__init__ = __init__
```

```{code-cell} ipython
Belle = Chien(3)
```

```{code-cell} ipython
Belle.age
```

On peut modifier l'initialisation de la classe pour prendre comme paramètre d'entrée la date de naissance du chien. La fonction d'initialisation (`__init__`) permet de traiter ce paramètre pour stocker l'age.

```{code-cell} ipython
from datetime import datetime,timedelta
```

```{code-cell} ipython
def __init__(self, date_naissance):
    self.age = round((datetime.now() - datetime.fromisoformat(date_naissance)).days / 365)
```

```{code-cell} ipython
:tags: [remove-cell]

Chien.__init__ = __init__
```

```{code-cell} ipython
Belle = Chien('2017-07-28')
```

```{code-cell} ipython
Belle.age
```

Le constructeur calcule l'age en fonction de la date de naissance qui a été fournie lors de _l'instanciation_ de l'objet.

+++

On peut vouloir doter notre objet d'actions (on parle de _méthodes_). Par exemple, le chien peut aboyer :

```{code-cell} ipython
class Chien:
    def __init__(self):
        pass
    def aboyer(self,message=None):
        if message == None:
            return('Ouaf !')
        else:
            return(message)
```

```{code-cell} ipython
Belle = Chien()
```

```{code-cell} ipython
Belle.aboyer()
```

```{code-cell} ipython
Belle.aboyer('Sébastien !')
```

```{code-cell} ipython
Belle.__class__.__name__
```

## Exercice pour comprendre

+++

Écrire une classe permettant de simuler un personnage capable de se déplacer sur un réseau d'entiers.
Sa position de départ sera le couple 0,0 et l'utilisateur pourra saisir respectivement 'h','j','k','l' pour déplacer le personnage respectivement vers la gauche, le bas, le haut, la droite. On ne considère ici que des coordonnées positives.

```{code-cell} ipython
---
nbgrader:
  grade: false
  grade_id: cell-ff1d9e77791f1b2f
  locked: false
  schema_version: 3
  solution: true
  task: false
---
class Personnage:
    def __init__(self):
        pass
```

```{code-cell} ipython
:tags: [hide-cell]

class Personnage:
    ### BEGIN SOLUTION
    def __init__(self):
        self.position = [0,0]
    def deplacer(self,direction):
        if direction == 'h' and self.position[0] != 0:
            self.position[0] -= 1
        elif direction == 'j' and self.position[1] != 0:
            self.position[1] -= 1
        elif direction == 'k':
            self.position[1] += 1
        elif direction == 'l':
            self.position[0] += 1
        else:
            return 'Erreur'
    def get_position(self):
        return tuple(self.position)
    ### END SOLUTION
```

```{code-cell} ipython
---
nbgrader:
  grade: false
  grade_id: cell-daa1b2a79deea259
  locked: true
  schema_version: 3
  solution: false
  task: false
---
joueur1 = Personnage()
```

```{code-cell} ipython
---
nbgrader:
  grade: true
  grade_id: cell-029d4120bab7d605
  locked: true
  points: 6
  schema_version: 3
  solution: false
  task: false
---
assert joueur1.get_position() == (0,0), 'La position de départ est (0,0)'
assert joueur1.deplacer('h') == 'Erreur', 'On renvoie le message "Erreur" en cas de déplacement impossible'
assert joueur1.deplacer('k') == None, 'Pas de message quand tout se passe bien'
assert joueur1.get_position() == (0,1), 'La position après le déplacement vers le haut doit être (0,1)'
assert joueur1.deplacer('j') == None, "Pas d'erreur cette fois, le déplacement est valide"
assert joueur1.deplacer('j') == "Erreur", "Cette fois, on a une erreur"
```

## Compléments sur la "magie" des classes

+++

Lorsqu'on utilise 
```python
belle = Chien()
```
en réalité, un objet vide nommé `belle` est réservé et la méthode (une fonction) `Chien.__init__(belle)` est appelé, ce qui a pour effet d'initialiser l'objet `belle` comme une instance de `Chien`.

De la même façon, lors de l'appel à la méthode `belle.aboyer('Sébastien')`, ce qui est réellement exécuté est `Chien.aboyer(belle,'Sébastien')`
