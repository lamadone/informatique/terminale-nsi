---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3.9.7 64-bit
  name: python3
---

# Tester le code produit

Dans le cadre de la production de code, il est souvent nécessaire de fournir des interfaces documentées, et de vérifier, sur un certain nombre d'exemples que les fonctions et modules produits correspondent bien aux attentes du donneur d'ordre.

Pour cela, on utilise des cadriciels [^framework] de tests. Certains, comme `doctest` sont intégrés  à Python (au sens qu'il s'agit d'une bibliothèque distribuée avec Python), d'autres nécessitent l'installation de modules tiers[^modules].

+++

```{list-table}
:header-rows: 1

* - Contenus
  - Capacités attendues
  - Commentaires
* - Mise au point des programmes
  - Utiliser des jeux de tests
  - L'importance de la qualité et du nombre de tests est mise en évidence.
    
    Le succès d'un jeu de tests ne garantit pas la correction d'un
    programme.
```

+++

## Doctest

Il s'agit de l'infrastructure minimaliste de test, [livrée dans la distribution standard de Python](https://docs.python.org/3/library/doctest.html). Cette infrastructure est rudimentaire, mais est essentielle, surtout pour son aspect de documentation.

Voyons ça sur un exemple :

```{code-cell} ipython3
def addition(a: int, b: int) -> int:
    """
    Une fonction de démonstration de doctest

    Cette fonction n'a pour but que de montrer le fonctionnement de doctest.

    Args:
        a (int): le premier terme de la somme
        b (int): le deuxième terme de la somme
    Returns:
        int: le résultat de la somme

    Exemple:
    >>> addition(1,2)
    3
    >>> addition(1,2.2)
    Traceback (most recent call last):
    Exception: Les arguments doivent être des entiers, ici on a reçu a=1 et b=2.2
    """
    if not (isinstance(a,int) and isinstance(b, int)):
        raise Exception(f'Les arguments doivent être des entiers, ici on a reçu a={a} et b={b}')
    return a + b
```

Testons « à la main » la fonction.

```{code-cell} ipython3
addition(1,2)
```

```{code-cell} ipython3
addition(1,2.2)
```

La ligne suivante permet d'activer le rendu du module `doctest` sous Jupyter.

```{code-cell} ipython3
%doctest_mode ON
```

Il suffit, pour tester le code, de mettre, par exemple à la fin du fichier les deux lignes suivantes. L'option `verbose=True` permet d'avoir des détails.

```{code-cell} ipython3
from doctest import testmod
testmod(verbose=True)
```

Sans le `verbose=True`, on a l'afichage suivant :

```{code-cell} ipython3
testmod()
```

```{code-cell} ipython3
:tags: [remove-cell]

del addition
```

On peut documenter/tester des classes aussi  :

```{code-cell} ipython3
class Personnage:
    """Une classe pour représenter une personne

    Exemple
    >>> joueur1 = Personnage()
    >>> joueur1.get_point_de_vie()
    100
    """
    def __init__(self):
        self.point_de_vie = 100
    
    def get_point_de_vie(self):
        return self.point_de_vie
```

```{code-cell} ipython3
testmod(verbose=True)
```

Attention, il faut prendre en compte que `doctest` fait de la comparaison de sortie standard, ce qui le rend assez fragile, celle-ci pouvant varier. De plus, lorsqu'il y a modification d'un objet muable [^muable].

```{code-cell} ipython3
:tags: [remove-cell]

del Personnage
```

```{code-cell} ipython3
def une_fonction_sans_return(L):
    """
    Une fonction qui modifie une liste
    L.sort()

    >>> L = [2, 1, 3]
    >>> une_fonction_sans_return(L)
    assert L == [1, 2, 3]
    """
```

```{code-cell} ipython3
testmod(verbose=True)
```

## Un cadriciel de test parmi d'autres

Le but de ce cours n'est pas de faire un état des lieux exhaustifs de tous les cadriciels de test de Python. On peut toutefois citer :
+ [unittest](https://docs.python.org/3/library/unittest.html) livré avec Python
+ [nose](https://pypi.org/project/nose2/) une extension du précédent
+ [pytest](https://docs.pytest.org/en/6.2.x/)
L'industrie de l'informatique s'est assez largement emparé des tests pour la production du code. On peut citer par exemple les méthodes de développement autour du [Test Driven Development (TDD)](https://fr.wikipedia.org/wiki/Test_driven_development), voir par exemple [cette introduction](https://www.guru99.com/test-driven-development.html).

+++

Dans le cours de NSI, on va brièvement présenter pytest, ainsi que son utilisation rudimentaire lors des projets. 

Pour les tests avec pytest, le mot clef [^keyword] `assert` sera assez massivement utilisé, à l'intérieur d'une fonction.

```{code-cell} ipython3
:tags: [remove-input]

from IPython.display import Code
Code(filename='cours/tests/test_main.py',language='python3')
```

Généralement, on place les tests dans un répertoire `tests` en organisant un fichier `test_<nom>` pour chaque fichier `nom.py`.
La commande suivante, lancée depuis un shell d'exécution permet de lancer les tests.

```{code-cell} ipython3
#!pip install pytest
```

```{code-cell} ipython3
:tags: [remove-output]

!py.test-3 cours/tests/
```

Avec les bonnes bibliothèques, on peut assi l'utiliser dans un notebook :

```{margin}
Pour plus d'informations sur [IPyTest](https://pypi.org/project/ipytest/)
```

```{code-cell} ipython3
:tags: [remove-output]

#!pip install ipytest
import ipytest
ipytest.autoconfig()
```

```{code-cell} ipython3
:tags: [scroll_output]

%%ipytest
import pytest
def test_addition_int():
    assert addition(2,3) == 5

def test_addition_float():
    with pytest.raises(Exception):
      addition(2,2.2)

def addition(a, b):
    if not (isinstance(a,int) and isinstance(b, int)):
        raise Exception(f'Les arguments doivent être des entiers, ici on a reçu a={a} et b={b}')
    return a + b
```

De la même façon qu'avec `doctest`, on peut aussi tester des classes.

```{code-cell} ipython3
%%ipytest
def test_creation_personnage():
  joueur = Personnage()
  assert isinstance(joueur,Personnage)

def test_personnage_get_point_de_vie():
  joueur = Personnage()
  assert joueur.get_point_de_vie() == 100

class Personnage:
    def __init__(self):
        self.point_de_vie = 100

    def get_point_de_vie(self):
        return self.point_de_vie
```

L'écriture des tests, et des fonctions qui doivent passer les tests fournira ainsi un cadre robuste à la production de code. Les propositions, auss bien avec `doctest` qu'avec `pytest` préconisent de fournir les tests en amont (traduction de la demande), puis de coder les fonctions correspondantes.

+++

## Exercices

+++

1. Écrire un test permettant de vérifier qu'une fonction renvoie bien un entier.
2. Écrire un test pour vérifier que la fonction `sorted` permet de renvoyer une liste triée.
3. Écrire un test pour vérifier que la fonction `mystere` appliquée à `L`, une liste de longueur `len(L)` renvoie bien
    1. un tuple
    2. contenant un entier entre 0 et et `len(L) - 1`
    3. un élément de la liste `L`.
4. On considère le code suivant :

    ```python
    def addition(a,b):
        return a,b

    def test_addition():
        assert addition(2,3) == 5
    ````

    Indiquer si la fonction est valide au regard du test proposé.

+++

[^framework]: cadriciel est la version française de framework
[^modules]: extensions regroupées permettant d'importer des fonctions
    supplémentaires ; on parle aussi de bibliothèques
[^keyword]: éléments du langage python comme `def` ou `return`
