# coding: utf-8

#
# Activité 1 du chapitre 3 (p54) : Utiliser des APIs et des modules Python
#
# Accès au service OpenWeatherMap

# NE PAS DIFFUSER CETTE CLÉ (créée par l'auteur du chapitre) !
# Le professeur obtient une clé depuis https://home.openweathermap.org/users/sign_up et la fournit aux élèves.
# Les élèves peuvent obtenir eux-même une clé (nécessite l'accès à leur mail).
cle = 'd2d51e0e6f752efafe88511e52b8f7f3'

# -------- Question 1 : Requête explicite à l'API de OpenWeatherMaps

import requests
import json

def requete(cle, ville):
    """ Affiche les informations du site OpenWeatherMaps pour une ville """
    # Interroger le service Web OpenWeatherMap pour la ville donnée
    reponse = requests.get('https://api.openweathermap.org/data/2.5/weather?q='+ville+'&appid='+cle)
    # Afficher le dictionnaire obtenu :
    print(json.dumps(reponse.json(), indent=4))

### COMPLÉTER ici par un test de la fonction ci-dessus
requete(cle, 'Paris')

# -------- Question 3 : Utilisation du module Python pyowm

# module OpenWeatherMap à installer par la commande
#   pip install pyowm
# à exécuter dans le terminal ou depuis EduPython
from pyowm.owm import OWM

# Obtenir un accès au site authentifié par la clé
owm = OWM(cle)

def test_api(cle, ville):
    """ Afficher la météo d'une ville """
    # Obtenir l'objet 'manager' pour gérer une session avec le site OpenWeatherMaps
    manager = owm.weather_manager()
    # Obtenir la météo d'une ville et l'afficher
    observation = manager.weather_at_place(ville)
    print(observation.weather.status, observation.weather.temperature('celsius'))

### COMPLÉTER ici par un test de la fonction ci-dessus
test_api(cle, 'Paris')

# -------- Question 4 : Utilisation plus avancée
#   - accès au registre des villes
#   - accès aux prévisions à 8 jours

def previsions(owm, ville, pays):
    """ Renvoie les prévisions météo (temps et température) d'une ville """
    # Obtenir l'accès au registre des villes
    reg = owm.city_id_registry()
    # Obtenir les informations sur la ou les villes de ce nom
    liste = reg.locations_for(ville, pays)
    if len(liste) == 0: # ville inconnue
        print(ville+' inconnue !')
        return ([], [])
    # Extraire la latitude et longitude de la première ville de la liste
    latitude = liste[0].lat
    longitude = liste[0].lon

    # Obtenir l'objet 'manager' pour gérer une session avec le site OpenWeatherMap
    manager = owm.weather_manager()
    # Obtenir les prévisions pour les coordonnées données
    previsions = manager.one_call(latitude, longitude)

    ### COMPLÉTER ici pour créer un tableau des prévisions de températures
    ### et un tableau des prévisions du temps (couvert, ensoleillé, ...)
    ### Ces informations sont dans le tableau previsions.forecast_daily.
    ### Chaque élément de ce tableau correspond aux prévisions d'une journée.
    ### Ces prévisions sont un objet qui contient deux attributs qui nous intéressent : 
    ###     previsions.forecast_daily[i].temperature('celsius') : température en ºC
    ###     previsions.forecast_daily[i].status : météo sous forme de chaine de caractères
    temperatures = []
    meteo = []
    for i in range(len(previsions.forecast_daily)):
        journee = previsions.forecast_daily[i]
        temperatures.append(journee.temperature('celsius')['day'])
        meteo.append(journee.status)

    # retourner les deux tableaux sous forme d'un tuple
    return (temperatures, meteo)

### COMPLÉTER ici pour afficher les prévisions d'au moins deux villes
### Le pays est le code sur 2 lettres du pays : 
### 'FR' pour France, 'GB' pour Grande-Bretagne, etc.
print('Paris', previsions(owm, 'Paris', 'FR'))
print('Londres', previsions(owm, 'London', 'GB'))

# -------- Section 2, Question 2 : Afficher les prévisions

# from matplotlib import pyplot

### COMPLÉTER ici pour afficher les graphes des prévisions de températures.
### Utiliser la fonction `previsions` ci-dessus pour obtenir les prévisions.
### Utiliser la fonction `pyplot.plot` de la bibliothèque pyplot importée ci-dessus.
###    On peut appeler `plot` plusieurs fois pour afficher plusieurs graphes
### Utiliser la fonction `pyplot.legend` pour afficher la légende avec les noms des villes.
### Utiliser la fonction `pyplot.show` pour afficher le graphe,
### ou `pyplot.savefig` pour l'enregistrer dans un fichier.
# temperaturesParis, meteoParis = previsions(owm, 'Paris', 'FR')
# temperaturesLondres, meteoLondres = previsions(owm, 'London', 'GB')
# pyplot.plot(temperaturesParis)
# pyplot.plot(temperaturesLondres)
# pyplot.legend(['Paris', 'Londres'])
# # pyplot.savefig('meteo.pdf')
# pyplot.show()
