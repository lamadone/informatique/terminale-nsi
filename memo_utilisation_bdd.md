---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Utilisation de sqlite

```{code-cell} ipython
import sqlite3
```

```{code-cell} ipython
:tags: [remove-cell]

!rm test.sqlite
```

```{code-cell} ipython
conn = sqlite3.connect('test.sqlite')
```

```{code-cell} ipython
cur = conn.cursor()
```

```{code-cell} ipython
cur.execute('CREATE TABLE test (id int, nom name);')
```

```{code-cell} ipython
cur.execute('INSERT INTO test VALUES (1, "Habib") ;')
```

```{code-cell} ipython
cur.execute('SELECT * FROM test ;')
```

```{code-cell} ipython
cur.fetchone()
```

```{code-cell} ipython
cur.fetchall()
```
