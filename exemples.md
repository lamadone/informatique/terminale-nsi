---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: '0.8'
    jupytext_version: 1.4.1+dev
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# My simple notebook

Some **intro markdown**!

```{code-cell} ipython
:tags: [mytag]

print("A python cell")
```

## A section

And some more markdown...

```{list-table}
* - Notion
  - Savoir faire
  - Observations
* - Récursivité
  - Écrire des programmes récursifs

    Lire des programmes récursifs
  - 
```

https://github.com/Naereen/notebooks/blob/master/Obfuscated_code_or_piece_of_art.ipynb
