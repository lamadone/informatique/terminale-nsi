"""
fraction.py

une classe (incomplète) Fraction
une fonction egyptian donnant la decomposition égyptienne d'une fraction

Auteur  : S. Hoarau
Date    : 2021-09-19
Licence : CC BY-NC-SA 4.0 http://creativecommons.org/licenses/by-nc-sa/4.0/
"""
import math

class Fraction:
    
    PRECISION = 0.0000001
    
    def __init__(self, num, den):
        if den:
            self.__den = den
        else:
            raise ZeroDivisionError("denominator can't be zero")
        self.__num = num
        
    @property
    def num(self):
        return self.__num
    
    @num.setter
    def num(self, num):
        self.__num = num
        
    @property
    def den(self):
        return self.__den
    
    @den.setter
    def den(self, den):
        if den:
            self.__den = den
        else:
            raise ZeroDivisionError("denominator can't be zero")
    
    @staticmethod
    def reduce(a, b):
        pgcd = math.gcd(a, b)
        return a//pgcd, b//pgcd
    
    @classmethod
    def float(cls, f):
        if isinstance(f, float):
            den = 1
            while abs(int(f) - f) > Fraction.PRECISION:
                den *= 10
                f *= 10
            return Fraction(*Fraction.reduce(int(f), den))
        else:
            raise TypeError(f"{repr(f)} must be a float")
        
    def __str__(self):
        if self.den == 1:
            return str(self.num)
        else:
            return f'{self.num}/{self.den}'
    
    def __repr__(self):
        return f'Fraction({self.num}, {self.den})'
    
    def __eq__(self, f):
        if isinstance(f, int) or isinstance(f, float):
            return self.num % self.den == 0 and self.num // self.den == f
        elif isinstance(f, Fraction):
            return self.num * f.den == f.num * self.den
        return False
    
    def __lt__(self, f):
        if isinstance(f, int) or isinstance(f, float):
            return self.num / self.den < f
        elif isinstance(f, Fraction):
            return self.num * f.den < f.num * self.den
        else:
            raise NotImplemented(f"can't compare {repr(self)} and {repr(f)}")
    
    def __gt__(self, f):
        if isinstance(f, int) or isinstance(f, float):
            return self.num / self.den > f
        elif isinstance(f, Fraction):
            return self.num * f.den > f.num * self.den
        else:
            raise NotImplemented(f"can't compare {repr(self)} and {repr(f)}")
    
    def __add__(self, f):
        if isinstance(f, Fraction):
            num, den = Fraction.reduce(self.num * f.den + f.num * self.den, self.den * f.den)
            return Fraction(num, den)
        elif isinstance(f, int):
            num, den = Fraction.reduce(self.num + self.den * f, self.den)
            return Fraction(num, den)
        elif isinstance(f, float):
            return self.__add__(Fraction.float(f))

    def __sub__(self, f):
        if isinstance(f, Fraction):
            num, den = Fraction.reduce(self.num * f.den - f.num * self.den, self.den * f.den)
            return Fraction(num, den)
        elif isinstance(f, int):
            num, den = Fraction.reduce(self.num - self.den * f, self.den)
            return Fraction(num, den)
        elif isinstance(f, float):
            return self.__sub__(Fraction.float(f))
    
    def __mul__(self, f):
        if isinstance(f, Fraction):
            num, den = Fraction.reduce(self.num * f.num, self.den * f.den)
            return Fraction(num, den)
        elif isinstance(f, int):
            num, den = Fraction.reduce(self.num * f, self.den)
            return Fraction(num, den)
        elif isinstance(f, float):
            return self.__mul__(Fraction.float(f))        
        
        
    def simplify(self):
        num, den = Fraction.reduce(self.num, self.den)
        return Fraction(num, den)
    
    def auto_simplify(self):
        self.num, self.den = Fraction.reduce(self.num, self.den)


def egyptian(f):
    if isinstance(f, Fraction):
        if f < 1:
            reste = f.simplify()
            decompo = []
            n = 2
            while reste != 0:
                unit_frac = Fraction(1, n)
                while reste < unit_frac:
                    n += 1
                    unit_frac = Fraction(1, n)
                decompo.append(unit_frac)
                reste -= unit_frac
            return decompo
        else:
            raise ValueError(f"{f} is greater than 1 and has no egyptian decomposition")
    else:
        raise TypeError('Only Fraction smaller than 1 has egyptian decomposition')