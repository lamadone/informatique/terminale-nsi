---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: 'Python 3.7.6 64-bit (''base'': conda)'
  name: python3
---

# Comparaison d'un algorithme récursif et sa version itérative

+++

On va utiliser comme mesure d'efficacité le temps.

```{code-cell} ipython
def factorielle(n):
    if n > 0:
        return n*factorielle(n-1)
    else:
        return 1
```

```{code-cell} ipython
factorielle(7)
```

```{code-cell} ipython
%timeit factorielle(7)
```

```{code-cell} ipython
def factorielle_iterative(n):
    f = 1
    for i in range(1,n+1):
        f = i*f
    return f
```

```{code-cell} ipython
factorielle_iterative(7)
```

```{code-cell} ipython
%timeit factorielle_iterative(7)
```

```{code-cell} ipython
memo = {0: 1}
def factorielle_memoisation(n):
    if n in memo:
        return memo[n]
    else:
        memo[n] = n*factorielle_memoisation(n - 1)
        return memo[n]
```

```{code-cell} ipython
%%timeit memo = {0: 1}
factorielle_memoisation(7)
```

````{margin}
```python3
import sys
sys.getrecursionlimit()
```

```python3
sys.setrecursionlimit(20000)
```

```python3
sys.getrecursionlimit()
```
````
