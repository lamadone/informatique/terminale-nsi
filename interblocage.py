from filelock import FileLock

print("Exemple de self-deadlock...")

RESSOURCE = "verrou.lock"

print("Verrou 1", flush=True)
verrou1 = FileLock(RESSOURCE)
verrou1.acquire(timeout=-1)
#verrou1.release()

print("Verrou 2", flush=True)
verrou2 = FileLock(RESSOURCE)
verrou2.acquire(timeout=-1)
verrou2.release()

print("C'est fini")
