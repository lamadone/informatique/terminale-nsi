---
jupytext:
  text_representation:
    format_name: myst
kernelspec:
  display_name: Python 3
  name: python3
---

# Quelques exemples

## Factorielle de $n$ (notée $n!$)

On définit $$
    \left\{\begin{array}{l} 0! = 1 \\ n! = n\times (n-1)! \end{array} \right.
$$

```{code-cell} ipython
:tags: [hide-cell]
def factorielle(n):
    if n == 0:
        return 1
    else:
        return n * factorielle(n-1)
```

```{code-cell} ipython
factorielle(7)
```

## PGCD de $a$ et $b$

On peut remarquer que `pgcd(a,b)= pgcd(b, b - a)` et même que `pgcd(a,b) = pgcd(b, reste(a,b))`.

```{code-cell} ipython
:tags: [hide-cell]
def pgcd(a,b):
    r = a % b
    if r == 0:
        return b
    else:
        return pgcd(b,r)
```

```{code-cell} ipython
pgcd(2,3)
```

```{code-cell} ipython
pgcd(27,81)
```

## Retourner une chaîne de caractère

On veut renverser l'ordre des lettres d'une chaîne de caractères.

```{code-cell} ipython
:tags: [hide-cell]
def renverse(chaine):
    if len(chaine) == 1:
        return chaine
    else:
        return renverse(chaine[1:]) + chaine[0]
```

```{code-cell} ipython
assert renverse('robert') == 'trebor'
assert renverse('noel') == 'leon'
```


## Décomposition d'un entier en une somme d'entiers

On cherche à décomposer un entier en une somme d'entiers :

+ 4 = 1 + 1 + 1 + 1
+ 4 = 1 + 1 + 2
+ 4 = 2 + 1 + 1
+ 4 = 1 + 2 + 1
+ ...

```{code-cell} ipython
:tags: [hide-cell]
def decompose(n, decompo_actuelle = None):
    if decompo_actuelle is None:
        decompo_actuelle = []
    if n == 0:
        return [decompo_actuelle]
    else:
        decompositions = []
        for i in range(1, n+1):
            nouvelle_decompo = decompo_actuelle + [i]
            decompositions += decompose(n-i, nouvelle_decompo)
        return decompositions
```


```{code-cell} ipython
decompose(4)
```

## Permutations d'une liste

Donner toutes les permutations d'une liste.

```{code-cell} ipython
:tags: [hide-cell]
def calculer_permutations(liste):
    if len(liste) == 0:
        return [[]]
    permutations = []
    for indice in range(len(liste)):
        element = liste[indice]
        reste = liste[:indice] + liste[indice+1:]
        permutations_reste= calculer_permutations(reste)
        for perm in permutations_reste:
            perm.insert(0, element)
            permutations.append(perm)
    return permutations
```


```{code-cell} ipython
:tags: [scroll_output]
calculer_permutations([1,2,3,4,5])
```

## Récurrence double : l'exemple des coefficients binomiaux

On définit le nombre de combinaisons de $p$ parmi $n$ par $\binom{n}{p} = \frac{n!}{p!(n-p)!}$.

On peut démontrer que $$
    \binom{n}{p} = \binom{n-1}{p-1} + \binom{n-1}{p}
$$

+++

Écrire une fonction qui calcule les nombres de combinaisons.

```{code-cell} ipython
:tags: [hide-cell]
def combinaison(n,p):
    if n < 0 or p < 0:
        return 0
    elif n == 0 and p == 0:
        return 1
    elif n == 0 and p == 1:
        return p
    else:
        return combinaison(n-1, p-1) + combinaison(n-1,p)
```

```{code-cell} ipython
combinaison(5,3)
``` 
