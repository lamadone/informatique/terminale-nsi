---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: 'Python 3.7.6 64-bit (''base'': conda)'
  name: python3
---

```{code-cell} ipython
help('len')
```

```{code-cell} ipython
?max
```

```{code-cell} ipython
import PIL.Image
```

```{code-cell} ipython
?PIL.Image
```

```{code-cell} ipython
?PIL.Image.new
```

```{code-cell} ipython
import io
```

```{code-cell} ipython
?io
```

```{code-cell} ipython
def f(x: float) -> float:
    """renvoie le triple de x"""
    return 3*x
```

```{code-cell} ipython
?f
```

```{code-cell} ipython
#!pip install  --user nb_mypy
```

```{code-cell} ipython
%load_ext nb_mypy
```

```{code-cell} ipython
%nb_mypy
```

```{code-cell} ipython
def f(x: float) -> float:
    """renvoie le triple de x"""
    return 3*x
```

```{code-cell} ipython

```
