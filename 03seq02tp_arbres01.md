---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: 'Python 3.9.9 64-bit (''.venv'': venv)'
  name: python3
---

# Quelques fonctions utiles sur les arbres.

Dans tout le document, on considère l'arbre suivant

```{code-cell} ipython
from binarytree import build
# Build a tree from list representation
values = [ 7 , 3 , 2 , 6 , 9 , None , 1 , 5 , None ,
10 , 8 , None , None , 11 , None , 12 , 13 , None , None ,14 ]
arbre = build(values)
print(arbre)
```
On considère que la profondeur de la racine est de 0.

1. Donner la profondeur des nœuds
    1. 7
    2. 9
    3. 14
2. Donner la hauteur de l'arbre
3. Donner la taille de l'arbre

## Écriture des fonctions

Écrire une fonction permettant de trouver la profondeur d'un nœud[^indice1].

Écrire une fonction récursive permettant de déterminer la hauteur d'un arbre.

```{code-cell} ipython
:tags: [hide-cell]
def hauteur(arbre):
    if arbre is not None:
        return 1 + max(hauteur(arbre.left), hauteur(arbre.right))
    else:
        return 0
```

On doit obtenir

```{code-cell} ipython
:tags: []
hauteur(arbre)
```


Écrire une fonction récursive permettant de déterminer la taille d'un arbre.

```{code-cell} ipython
:tags: [hide-cell]
def taille(arbre):
    if arbre is not None:
        return 1 + taille(arbre.left) + taille(arbre.right)
    else:
        return 0
```

On doit obtenir

```{code-cell} ipython
:tags: []
taille(arbre)
```

[^indice1]: on pourra réutiliser la fonction de parcours par niveau
