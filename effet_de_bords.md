---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
orphan: true
authors:
 - Guillaume Connan
 - Serge Bays
 - Vincent-Xavier Jumel
---

# Sur l'affectation et le passage de paramètre

```{code-cell}
:tags: [remove-cell]
%load_ext tutormagic
```

Sur une idée et un travail de Guillaume Connan
%src: https://mooc-forums.inria.fr/moocnsi/t/1ere-utiliser-ou-pas-leffet-de-bord/1452/50

::::{warning}
L'iframe de [https://Pythontutor.com](https://pythontutor.com) nécessite
d'autoriser le code (js, xhr et iframe) depuis pythontutor.
On peut ajouter
```text
frama.io pythontutor.com frame allow
frama.io pythontutor.com script allow
frama.io pythontutor.com xhr allow
```
à la configuration d'uMatrix.
::::

## appel par valeur

car on ne change pas l’objet après appel :

```{code-cell}
:tags: [remove-input, hide-ouptut]
%%tutor -l python3 -s -h 600
 
non_mutable = "pas touche"

def call_me(non_mutable):
    non_mutable = "touché"
    print(f"Dans le corps de la fonction {non_mutable}")


call_me(non_mutable)

print(f"Mais à l'extérieur {non_mutable}")
```

## appel par référence

```{code-cell}
:tags: [remove-input, hide-ouptut]
%%tutor -l python3  -h 600 -s

mutable = ["attrape", "moi"]

def appelle_moi(mutable):
    mutable += [" si tu peux"]
    print(f"Dans le corps de la fonction {mutable}")


appelle_moi(mutable)

print(f"Et à l'extérieur {mutable}")
```

Attention, ça ne fonctionne pas comme C

## Une autre écriture

```{code-cell}
:tags: [remove-input, hide-ouptut]
%%tutor -l python3 -s -h 600

mutable = ["attrape", "moi"]

def appelle_moi_autrement(mutable):
    mutable = mutable + [" si tu peux"]
    print(f"Dans le corps de la fonction {mutable}")


appelle_moi_autrement(mutable)

print(f"Et à l'extérieur {mutable}")
```

## Affectation en Python

```{code-cell}
:tags: [remove-input, hide-ouptut]
%%tutor -l python3 -s -h 600

mutable = ["attrappe", "moi"]

def appelle_moi(mutable):
    mutable.append(" si tu peux")
    print(f"Dans le corps de la fonction {mutable}. id = {id(mutable)}")


appelle_moi(mutable)

print(f"Et à l'extérieur {mutable}. id = {id(mutable)}")

mutable = ["attrappe", "moi"]

def appelle_moi_autrement(mutable):
    mutable = mutable + [" si tu peux"]
    print(f"Dans le corps de la fonction {mutable}. id = {id(mutable)}")


appelle_moi_autrement(mutable)

print(f"Et à l'extérieur {mutable}. id = {id(mutable)}")
```

## Réécriture de certaines fonctions

```{code-cell}
:tags: [remove-input, hide-ouptut]
%%tutor -l python3 -s -h 600

def appelle_moi_autrement():
    paramètre = liste
    paramètre.append(" si tu peux")
liste = ["attrape", "moi"]
appelle_moi_autrement()
```

```{code-cell}
:tags: [remove-input, hide-ouptut]
%%tutor -l python3 -s -h 600

def appelle_moi_autrement():
    paramètre = liste
    paramètre = paramètre + ["  si tu peux"]
liste = ["attrape", "moi"]
appelle_moi_autrement()
```
