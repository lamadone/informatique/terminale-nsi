---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: 'Python 3.8.12 64-bit (''NSI Terminale'': conda)'
  name: python3
---

# Quelques exemples sur l'utilisation des fils (processus légers ou threads)

```{code-cell} ipython
from time import sleep
```

```{code-cell} ipython
def f1():
    for _ in range(5):
        print("Bonjour",end=' ')
        sleep(0.01)


def f2():
    for _ in range(5):
        print("tout le monde")
        sleep(0.01)
```

```{code-cell} ipython
def sequentielle():
    f1()
    f2()
sequentielle()
```

```{code-cell} ipython
from threading import Thread

def entrelace():
    p1 = Thread(target=f1)
    p2 = Thread(target=f2)
    p1.start()
    p2.start()

entrelace()
```

```{code-cell} ipython
compteur = 0
limite = 400

def calcul():
    global compteur
    for c in range(limite):
        temp = compteur
        sleep(0.0000001)
        compteur = temp + 1

compteur = 0
calcul()
compteur 
```

```{code-cell} ipython
compteur = 0
limite = 100

mesFils = []
for i in range(4):
    p = Thread(target = calcul)
    p.start()
    mesFils.append(p)

for p in mesFils:
    p.join()

compteur
```

```{code-cell} ipython
from threading import Lock
verrou = Lock()

def calcul():
    global compteur
    for c in range(limite):
        verrou.acquire()
        temp = compteur
        sleep(0.0000001)
        compteur = temp + 1
        verrou.release()
```

```{code-cell} ipython
compteur = 0
limite = 100

mesFils = []
for i in range(4):
    p = Thread(target = calcul)
    p.start()
    mesFils.append(p)

for p in mesFils:
    p.join()

compteur
```

```{code-cell} ipython

```
