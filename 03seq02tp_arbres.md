---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# TP 1 sur les arbres

Le but de ce TP est de manipuler des arbres dans des cas simples et d'écrire
des algorithmes permettant de les parcourir.

```{margin}
Pour ce TP, il est nécessaire d'installer la bibliothèque `binarytree`.
```

```{code-cell} ipython
:tags: [margin, remove-output]
#!pip install --user binarytree
```

```{code-cell} ipython
:tags: [margin, remove-cell]
#!pip install binarytree
```


## Prise en main de binarytree.

Créer un arbre avec `binarytree`.

Un arbre est représenté par une classe `Node()`, qui dispose d'attributs
`left` et `right` qui seront des `Node()`.

Recopier et compléter le code suivant.

```{code-cell} ipython
from binarytree import Node
root = Node(1)
```

```{code-cell} ipython
:tags: [hide-input]
root.left = Node(2) # le fils gauche de la racine.
root.right = Node(4) # le fils droit de la racine.
root.left.right = Node(3) # le fils droit du fils gauche de la racine.
root.left.right.right = Node(5)
# le fils droit du fils droit du fils gauche de la …
# Permet de voir notre arbre racine.
print(root)
```

On peut aussi créer l'arbre à partir d'une liste.


```{code-cell} ipython
from binarytree import build
# Build a tree from list representation
values = [ 7 , 3 , 2 , 6 , 9 , None , 1 , 5 , None ,
10 , 8 , None , None , 11 , None , 12 , 13 , None , None ,14 ]
root = build(values)
print(root)
```

## Parcours d'arbre

Il existe différentes façon de parcourir un arbre.

### Parcours préfixe d'un arbre

Observons un premier parcours, avec les fonctions «builtin» du module.

```{code-cell} ipython
:tags: [margin]
arbre2 = build(values)
```

```{code-cell} ipython
print(arbre2,arbre2.preorder)
```

Que peut-on dire de ce parcours ?

+++

Écrire une fonction `préfixe` qui permet de parcourir un arbre de cette
façon.

```{code-cell} ipython
:tags: [hide-cell]
def prefixe(arbre, parcours = []):
    parcours.append(arbre.value)
    if arbre.left != None:
        prefixe(arbre.left, parcours)
    if arbre.right != None:
        prefixe(arbre.right, parcours)
    return parcours
```

L'appel de cette fonction sera le suivant :

```{code-cell} ipython
prefixe(arbre2)
```

### Parcours postfixe d'un arbre

```{code-cell} ipython
print(arbre2.postorder)
```

Que peut-on dire de ce parcours ?

+++

Coder une fonction récursive `postfixe`.

```{code-cell} ipython
:tags: [hide-cell]
def postfixe(arbre, parcours = []):
    if arbre.left != None:
        postfixe(arbre.left, parcours)
    if arbre.right != None:
        postfixe(arbre.right, parcours)
    parcours.append(arbre.value)
    return parcours
```

L'appel de cette fonction sera le suivant :

```{code-cell} ipython
postfixe(arbre2)
```

### Parcours infixe d'un arbre

```{code-cell} ipython
arbre2.inorder
```

Que peut-on dire de ce parcours ?

+++

Coder une fonction récursive `infixe`.

```{code-cell} ipython
:tags: [hide-cell]
def infixe(arbre, parcours = []):
    if arbre.left != None:
        infixe(arbre.left, parcours)
    parcours.append(arbre.value)
    if arbre.right != None:
        infixe(arbre.right, parcours)
    return parcours
```

L'appel de cette fonction sera le suivant :

```{code-cell} ipython
infixe(arbre2)
```

### Parcours par niveau

```{code-cell} ipython
arbre2.levelorder
```

Que peut-on dire de ce parcours ?

+++

Coder une telle fonction. On peut utiliser une file.

:::::{margin}
::::{tip}
Pour avoir rapidement une file en Python, on peut utiliser une liste :
```python
# Initalise une file vide
l = []
# Enfile valeur
l.append(valeur)
# Défile
l.pop(0)
```
::::
:::::

```{code-cell} ipython
:tags: [hide-cell]
def parcours(arbre):
    file = []
    res = []
    file.append(arbre)
    while len(file) > 0:
        voisin = file.pop(0)
        res = res + [voisin]
        if voisin.left is not None:
            file.append(voisin.left)
        if voisin.right is not None:
            file.append(voisin.right)
    return res
```

La fonction s'utilise ainsi.

```{code-cell} ipython
parcours(arbre2)
```


