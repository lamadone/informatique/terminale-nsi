---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

<div style=" margin:20px;padding:15px; background-color:#AA6666; border:4px solid #117864; -moz-border-radius:9px; -khtml-border-radius:9px; -webkit-border-radius:9px; border-radius:9px;text-align:center;color:white;font-size:1.5em">
L'ordonnancement SJF
</div>

+++

![image.png](attachment:image.png)

+++

<div style=" margin:20px;padding:15px; background-color:#AA6666; border:4px solid #117864; -moz-border-radius:9px; -khtml-border-radius:9px; -webkit-border-radius:9px; border-radius:9px;text-align:center;color:white;font-size:1em">
SDD : modéliser les processus
</div>

+++


Nous allons étudier la situation suivante : 

L'ordonnanceur va attribuer la CPU aux processus en suivant l'algorithme Shortest Path First.

Voici les temps d'arrivée et les durées des 4 processus que nous allons traiter :

|processus|durée|arrivée|
|:--:|:--:|:--:|
|P1|7|7|
|P2|8|8|
|P3|14|14|
|P4|5|8|


 <b><font color="grey" size=6>⚙ </font><font  color="grey" size=4> A faire vous même 1</font></b> 

    Chaque processus sera modélisé à l'aide d'un dictionnaire en python, avec les clefs 'name' 'duree' 'arrivee'


**Ecrire les 4 dictionnaires P1, P2, P3 et P4 puis créer une liste processus qui est une liste de dictionnaires contenant ces 4 processus**

```{code-cell}
P1 = {'name':'P1','duree':7,'arrivee':3}
P2 = {'name':'P2','duree':8,'arrivee':2}
P3 = {'name':'P3','duree':14,'arrivee':0}
P4 = {'name':'P4','duree':8,'arrivee':5}

processus = [P1,P2,P3,P4]
```

```{code-cell}
# test de votre implémentation
from test_tpordonnanceur import *
test1(processus)
```

<div style=" margin:20px;padding:15px; background-color:#AA6666; border:4px solid #117864; -moz-border-radius:9px; -khtml-border-radius:9px; -webkit-border-radius:9px; border-radius:9px;text-align:center;color:white;font-size:1em">
Enfiler les processus quand ils arrivent
</div>
Chaque processus arrive à un temps donné, et doit être inséré à sa place dans la file.

Exemples :

> La file contient un processus elu (intouchable) et un seul processus P1 d'une durée de 12. Un nouveau processus P2 arrive d'une durée de 4.
>
> Il faut enfiler P2 avant P1, la file devient [elu, P2, P1]


> La file contient un processus elu (intouchable) et un seul processus P1 d'une durée de 12. Un nouveau processus P2 arrive d'une durée de 14.
>
> Il faut enfiler P2 après P1, la file devient [elu, P1, P2]

> La file contient un processus elu (intouchable) et 
> + un  processus P1 d'une durée de 12. 
> + un processus P2 d'une durée de 14. file = [elu, P1,P2]
> 
> arrive P3 d'une durée de 13
>
> Il faut enfiler P3 avant P2, la file devient [elu, P1, P3, P2]





+++

 <b><font color="grey" size=6>⚙ </font><font  color="grey" size=4> A faire vous même 2</font></b> 

Complétez le code de la fonction enfiler :

```{code-cell}
def enfiler(file,P) :
    """
    :param P:dict: un processus
    :param file: la file des processus en cours de traitement
    La fonction insère le processus dans la file à sa place (par ordre de prio) dans la file. 
    La priorité est définie par la durée, les plus court avant. Le temps restant n'est pas pris en compte, seulement la durée
    totale d'exécution des processus.
    Important : Si deux processus ont même durée, l'ordre d'arrivée est conservé.
    """
    duree = P['duree']
    j = 1
    while j< len(file) and duree >= file[j]['duree'] :
        j+=1
    file.insert(j,P)
    return file
```

```{code-cell}
# test
test_enfiler(enfiler)
```

 <b><font color="grey" size=6>⚙ </font><font  color="grey" size=4> A faire vous même 3</font></b> 

**Déroulez le code de la fonction enfiler :**

+ A t=0 on enfile P1 (durée 15)
+ a t = 3  on enfile P2 (durée 10)

Donnez la valeur de j à la fin du while et la file obtenue quand P2 est entré.
+ A t = 5  on enfile P3 (durée 13)


Donnez la valeur de j à la fin du while et lafile obtenue quand P3 est entré.

+ A t = 7  on enfile P4 (durée 10)

Donnez la valeur de j à la fin du while et lafile obtenue quand P4 est entré.

+++

Vos réponses ici (double clic pour éditer)

à t = 0 : j = 0 : file = [P1]

à t = 3 : j = 0 : file = [P2, P1]

à t = 5 : j = 1 : file = [P2, P3, P1]

à t = 7 : j = 1 : file =  [P2, P4, P3, P1]

+++

 <b><font color="grey" size=6>⚙ </font><font  color="grey" size=4> A faire vous même 4</font></b> 

**Appel de enfiler :**

Ecrire la fonction arriver dont les spécifications sont données ci-dessous

```{code-cell}
def arriver(processus,file,t) :
    """
    :params processus: liste de dict contenant tous les processus a traiter
    :param file: liste de dict contenant les processus arrivés dans l'ordre de priorité
    :param:t: int: temps (en unité arbitraire)
    La fonction parcoure la liste des processus, et si l'un d'eux arrive au temps t, elle l'insère dans la file 
    en appelant la fonction enfiler().
    """
    for P in processus :
        if P['arrivee'] == t :
            enfiler(file,P)
```

```{code-cell}
# test de votre fonction
test2(arriver)
```

<div style=" margin:20px;padding:15px; background-color:#AA6666; border:4px solid #117864; -moz-border-radius:9px; -khtml-border-radius:9px; -webkit-border-radius:9px; border-radius:9px;text-align:center;color:white;font-size:1em">
Exécuter un quantum du processus élu
</div>
A chaque instant la file contient la liste ordonnée des processus. Le processus d'indice 0 est donc élu.

 <b><font color="grey" size=6>⚙ </font><font  color="grey" size=4> A faire vous même 5</font></b> 

**écrire la fonction exec_quantum dont les spécifications sont données ci-dessous.**

```{code-cell}
def exec_quantum(file,t) :
    """
    :param file: file des processus en cours de traitement
    :param t: temps
    la durée restant (clé reste) du processus situé en tête de liste (indice 0) est décrémentée.
    Si après décrémentation elle vaut 0, le processus est terminé et donc retiré de la file.
    
    La fonction modifie ke processus élu (modification du dictionnaire) et si besoin la variable file, 
    et renvoie le nom du processus élu.
    Elle affiche le temps actuel, le processus élu, et s'il est terminé elle affiche aussi cette information.
    Exemples :
    >>> exec_quantum([P2,P3],0)
    t= 0 élu P2
    
    >>> P2['reste'] = 1
    >>> exec_quantum([P2,P3],4)
    t= 4 élu P2 Processus terminé
    """
    P = file[0]
    P['duree'] -= 1
    if P['duree'] == 0 :
        file.pop(0)
        print('t=',t,'élu',P['name'],'Processus terminé')
    else :
        print('t=',t,'élu',P['name'])
    return P['name']
```

```{code-cell}
test3(exec_quantum)
```

<div style=" margin:20px;padding:15px; background-color:#AA6666; border:4px solid #117864; -moz-border-radius:9px; -khtml-border-radius:9px; -webkit-border-radius:9px; border-radius:9px;text-align:center;color:white;font-size:1em">
Exécuter un quantum du processus élu
</div>
Vous disposez maintenant de toutes les fonctions requises. 

 <b><font color="grey" size=6>⚙ </font><font  color="grey" size=4> A faire vous même 6</font></b> 


**Complétez le code ci-dessous pour faire afficher l'ordonnancement de nos 4 processus.**

```{code-cell}

lst_process = [P1,P2,P3,P4]
file = []
t=0

while t == 0 or file != [] :
    """
    ordonancement des processus
    """
    arriver(lst_process,file,t) # regarde si un nouveau processus arrive, et si oui l'ajoute dans la file
    exec_quantum(file,t)        # exécute un quantum de temps du processus elu
    t+=1
```

```{code-cell}

```
