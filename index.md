# Accueil

Une page d'accueil pour ce que je présente en Terminale NSI

La référence est bien sur le [programme](https://cache.media.eduscol.education.fr/file/SPE8_MENJ_25_7_2019/93/3/spe247_annexe_1158933.pdf), publié au BOEN

Les documents produits dans le cadre du cours doivent être au format demandé (généralement `<nom> <prenom> <titre du TP>`), suivi d'une extension de fichier (`.ipynb` ou `.pdf`)

```{tableofcontents}
```
