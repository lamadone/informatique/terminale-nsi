---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Conception des bases de données

```{code-cell} ipython
:tags: [remove-cell]

#!pip3 install mocodo
```

```{code-cell} ipython
:tags: [remove-cell]

%reload_ext mocodo
```

Lors de la phase de conception, il est courant de représenter graphiquement les relations entre tables avec un grpahe Entité Relation. Voici le graphe entité relation correspondant à l'exemple traité dans ce cours.

```{code-cell} ipython
:tags: [remove-input]

%%mocodo
AUTEUR: IdAuteur, Nom, Prénom, Langue, Date de naissance
ÉCRIT, 1N AUTEUR, NN LIVRE
LIVRE: IdLivre, Titre, IdAuteur, Année Parution
LANGUE: IdLangue, Langue
PARLE, 1N AUTEUR, 1N LANGUE
THEME: IdTheme, Theme
CONCERNE, NN LIVRE, NN THEME : IdRelation
```

On représente aussi souvent les tables de la façon suivantes.

```{code-cell} ipython
:tags: [remove-input]

%%mocodo -t mld 
AUTEUR: IdAuteur, Nom, Prénom, Langue, Date de naissance
ÉCRIT, 1N AUTEUR, NN LIVRE
LIVRE: IdLivre, Titre, IdAuteur, Année Parution
LANGUE: IdLangue, Langue
PARLE, 1N AUTEUR, 1N LANGUE
THEME: IdTheme, Theme
CONCERNE, NN LIVRE, NN THEME : IdRelation
```

Dans cette représentation, les clés primaires sont soulignées.

+++

On peut aussi représenter le diagramme sql.

```{code-cell} ipython
:tags: [remove-cell]

%%mocodo -t diagram
AUTEUR: IdAuteur [int], Nom [text], Prénom [text], Langue [int], Date de naissance [date]
ÉCRIT, 1N AUTEUR, NN LIVRE
LIVRE: IdLivre [int], Titre [text], IdAuteur [int], Année Parution [int]

LANGUE: IdLangue [int], Langue [text]
PARLE, 1N AUTEUR, 1N LANGUE

THEME: IdTheme [int], Theme [text]
CONCERNE, NN LIVRE, NN THEME : IdRelation [int]
```

```{code-cell} ipython
:tags: [remove-input]

%mocodo --input mocodo_notebook/sandbox.mld
```

Cette représentation permet aussi de retrouver le schéma sql proposé dans la page précédente.

```{code-cell} ipython
:tags: [remove-cell]

%%mocodo --title=livres --relations=sqlite
AUTEUR: IdAuteur [int], Nom [text], Prénom [text], Langue [int], Date de naissance [date]
ÉCRIT, 1N AUTEUR, NN LIVRE
LIVRE: IdLivre [int], Titre [text], IdAuteur [int], Année Parution [int]
LANGUE: IdLangue [int], Langue [text]
PARLE, 1N AUTEUR, 1N LANGUE
THEME: IdTheme [int], Theme [text]
CONCERNE, NN LIVRE, NN THEME : IdRelation [int]
```

```{code-cell} ipython
:tags: [remove-input]

from IPython.display import Code
Code(filename='mocodo_notebook/sandbox_sqlite.sql',language='sql')
```

```{code-cell} ipython

```
