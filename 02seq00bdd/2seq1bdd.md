---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Base de données avec Python

## Sqlite

On utilisera essentiellement [Sqlite](https://sqlite.org) dans le cadre du
cours de NSI pour les raisons suivantes :

+ intégré à Python
+ stocke les informations dans un fichier texte
+ s'intère également à Jupyter
+ dispose de la plus part des mécanismes des SGBD
+ ne nécessite pas de serveur supplémentaire

Mais l'essentiel de ce que nous allons voir au sujet de `sqlite` est
transférable à d'autres SGDB comme [MariaDB](https://mariadb.org)[^mariadb]
ou [PostgreSQL](https://postgresql.org) pour ne citer que les SGDB libres
[^liberté].

+++

Ce cours a pour obectif de présenter les bases de la manipulation de base de
données. Un autre cours permettra d'avoir quelques  éléments de
compréhension de la conception de base de données.

+++
## Extrait du programme

```{list-table}
:header-rows: 1
* - Contenu
  - Capacité
  - Commentaires
* - Langage SQL :
    requêtes d’interrogation et de mise à jour d’une base de données.
  - Identifier les composants d’une requête.

      Construire des requêtes d'interrogation à l’aide des clauses du
      langage SQL : SELECT, FROM, WHERE, JOIN.

      Construire des requêtes d’insertion et de mise à jour à l’aide de :
      UPDATE, INSERT, DELETE.
  - On peut utiliser DISTINCT, ORDER BY ou les fonctions d’agrégation sans utiliser les clauses GROUP BY et HAVING.
```

+++

Pour utiliser `sqlite` avec __Jupyter__, il est nécessaire d'installer
`ipython-sql`

::::{margin}
:::{tip}
Cette installation n'est à faire que la première fois.
:::
:::{warning}
Il faut peut-être passer l'option `--user` après `pip3`
:::
Lorsqu'on est administrateur de sa machine (Windows Home par exemple), on peut ne pas utiliser `--user`.
::::

```{code-cell} ipython
---
tags: [remove-output]
---
#!pip install --user ipython-sql
```
```{code-cell} ipython
---
tags: [remove-cell]
---
#!pip install ipython-sql
```

:::{margin}
On charge le code dans Jupyter
:::

```{code-cell} ipython
---
tags: [remove-output]
---
%reload_ext sql
```

## Utilisation

L'extension `sql` ajoutée à Jupyter permet d'activer les commandes en
un ligne du type `%sql <REQUETE>` où `<REQUETE>` correspond à une commande
SQL valide.

:::{note}
Pour disposer d'un mode multiligne, on utilisera `%%sql`.
:::


::::{margin}
:::{warning}
Attention, pour que ce qui suit fonctionne correctement, il est impératif
que la base de données `livres` soit vide.
:::
::::

```{code-cell} ipython
---
tags: [remove-cell]
---
!rm livres.sqlite
```

````{tabbed} Windows
```ipython3
!del livres.sqlite
```
````

````{tabbed} Linux / Mac
```ipython3
!rm livres.sqlite
```
````

### connexion à la base de donnée

On crée la connexion à la base de données.

```{code-cell} ipython
%sql sqlite:///livres.sqlite
```

:::{margin}
Sous Python, on utilisera
```python
import sqlite3
base_de_donnees = sqlite3.connect('livres.sqlite')
```
:::

+++

### Création d'une table

Pour créer une table `Langues` contenant un identifiant numérique et le nom
de la langue, on utilise une requête `CREATE TABLE`.

```sql
CREATE TABLE Langues (
    IdLangue INTEGER PRIMARY KEY,
    Langue TEXT
);
```

::::{margin}
:::{warning}
Ne pas oublier d'écrire `%%sql` sur la première ligne
:::
::::


```{code-cell} ipython
:tags: [remove-input]
%%sql
CREATE TABLE Langues (
    IdLangue INTEGER PRIMARY KEY,
    Langue TEXT
);
```

:::{margin}
Sous Python, l'exécution des requêtes se fait avec `execute`
```python
curseur = base_de_donnees.cursor()
curseur.execute(<REQUETE SQL>)
```
:::

La création d'une table correspond à une création de structure. C'est une
vision assez proche de la définition d'une classe en programmation orientée
objet. Cependant, cette classe ne permettrait que d'instancier des objets
avec des attributs, sans aucune méthode[^procedure_sql].

+++

### Insertion de données

Une fois la table créée, on peut insérer des données dedans, avec la requête
`INSERT INTO <Table>`

:::{margin}
L'habitude veut qu'on écrive les mots spécifiques de SQL en capitales et les
tables et colonnes en bas de casse.
:::

```sql
INSERT INTO Langues 
    (Langue)
    VALUES 
    ("Français"),
    ("Anglais")
    ;
```
```{code-cell} ipython
:tags: [remove-input]
%%sql
INSERT INTO Langues 
    (Langue)
    VALUES 
    ("Français"),
    ("Anglais")
    ;
```

On peut noter ici qu'il n'a pas été nécessaire d'écrire la valeur de
`IdLangue`.

+++

### Lire les données insérées

On peut désormais lire les données de la table.

```sql
SELECT * FROM Langues ;
```

```{code-cell} ipython
:tags: [remove-input]
%sql SELECT * FROM Langues ;
```

:::{margin}
On vérifie que `IdLangue` a bien été incrémenté.
:::

:::{tip}
On peut ne sélectionner que certaines colonnes en remplaçant `*` par le nom
des colonnes.
:::


## Créer une table Auteurs

Cette table doit disposer de
* un identifiant
* un nom d'auteur
* un prénom d'auteur
* une langue maternelle de l'auteur
* une année de naissance

```{code-cell} ipython
:tags: [hide-cell]
%%sql
CREATE TABLE Auteurs (
    IdAuteur       INTEGER  PRIMARY KEY,
    NomAuteur      TEXT,
    PrenomAuteur   TEXT,
    IdLangue       INTEGER,
    AnneeNaissance INTEGER,
    FOREIGN KEY(IdLangue) REFERENCES Langues(IdLangue)
);
```

+++

## Alimenter la table Auteurs

Alimenter ensuite cette table avec les données suivantes :


| Nom      | Prenom       | annee naissance | langue   |
| -------- | ------------ | --------------- | -------- |
| Orwell   | George       | 1903            | Anglais  |
| Herbert  | Frank        | 1920            | Anglais  |
| Asimov   | Isaac        | 1920            | Anglais  |
| Huxley   | Aldous       | 1894            | Anglais  |
| Bradbury | Ray          | 1920            | Anglais  |
| K. Dick  | Philip       | 1928            | Anglais  |
| Barjavel | René         | 1911            | Français |
| Boulle   | Pierre       | 1912            | Français |
| Van Vogt | Alfred Elton | 1912            | Anglais  |
| Verne    | Jules        | 1828            | Français |

```{code-cell} ipython
:tags: [hide-cell]
%%sql
INSERT INTO Auteurs   (NomAuteur,PrenomAuteur,AnneeNaissance,IdLangue)
VALUES
("Orwell","George",1903,2),
("Herbert", 	"Frank", 	1920 	,2),
("Asimov", 	"Isaac", 	1920 	,2),
("Huxley", 	"Aldous", 	1894 	,2),
("Bradbury", 	"Ray", 	1920 	,2),
("K. Dick", 	"Philip", 	1928 	,2),
("Barjavel", 	"René", 	1911 	,1),
("Boulle", 	"Pierre", 	1912 	,1),
("Van Vogt", 	"Alfred Elton", 	1912, 	2),
("Verne", 	"Jules", 	1828, 	1)
;
```

+++

## Opérations sur les tables

On peut ensuite réaliser une copie de la base de données pour expérimenter
les actions suivantes.


### Sélectionner que certaines lignes

```{code-cell} ipython
%sql SELECT NomAuteur, PrenomAuteur FROM Auteurs WHERE AnneeNaissance = 1920 ;
```

### Supprimer des informations

```{code-cell} ipython
%sql SELECT IdAuteur FROM Auteurs ;
```

```sql
%sql DELETE FROM Auteurs WHERE IdAuteur = 1 ;
```

```{code-cell} ipython
:tags: [remove-cell]
%sql DELETE FROM Auteurs WHERE IdAuteur = 1 ;
```

```{code-cell} ipython
%sql SELECT IdAuteur FROM Auteurs ;
```

Pour supprimer toute une table :

```sql
%sql DROP TABLE Auteurs ;
```
::::{margin}
:::{warning}
Attention cette opération est définitive !
:::
::::


+++

### Modifier une ligne
```{code-cell} ipython
:tags: [remove-cell]
%%sql INSERT INTO Auteurs   (IdAuteur,NomAuteur,PrenomAuteur,AnneeNaissance,IdLangue)
VALUES
(1,"Orwell","George",1903,2)
```

```sql
UPDATE Auteurs SET PrenomAuteur = "Georges" WHERE PrenomAuteur = "George" ;
```

```{code-cell} ipython
:tags: [remove-input]
%sql UPDATE Auteurs SET PrenomAuteur = "Georges" WHERE PrenomAuteur = "George" ;
```

```{code-cell} ipython
%sql SELECT NomAuteur, PrenomAuteur FROM Auteurs ;
```

## Jointure de tables

+++

La table Auteurs ne contient pas la Langue à proprement parler, mais un
identifiant pointant vers cette Langue. Pour cumuler ces deux informations,
on utilise `JOIN`

```sql
SELECT NomAuteur, PrenomAuteur, Langue, AnneeNaissance  
    FROM Auteurs 
    JOIN Langues 
    ON Auteurs.IdLangue = Langues.IdLangue
```

```{code-cell} ipython
:tags: [remove-input]
%%sql
SELECT NomAuteur, PrenomAuteur, Langue, AnneeNaissance  
    FROM Auteurs 
    JOIN Langues 
    ON Auteurs.IdLangue = Langues.IdLangue
```

+++

## Autres opérations

+++

### Compter les auteurs

+++

Pour compter les éléments d'une table :

```sql
SELECT COUNT(*) FROM Auteurs ;
```

```{code-cell} ipython
:tags: [remove-input]
%sql SELECT COUNT(*) FROM Auteurs ;
```

+++

Compter les auteurs du XX<up>ème</up> siècle

```sql
SELECT COUNT(*) FROM Auteurs WHERE AnneeNaissance > 1900 ;
```
```{code-cell} ipython
:tags: [remove-input]
%sql SELECT COUNT(*) FROM Auteurs WHERE AnneeNaissance > 1900 ;
```


+++

### Recherche dans une table

+++

Trouver le nom d'un auteur à partir de son prénom

```sql
SELECT NomAuteur FROM Auteurs WHERE PrenomAuteur = 'Jules';
```

+++

* Attention, la recherche est sensible aux majuscules
```sql
%sql SELECT NomAuteur FROM Auteurs WHERE PrenomAuteur = 'jules';
```
```{code-cell} ipython
:tags: [remove-input]
%sql SELECT NomAuteur FROM Auteurs WHERE PrenomAuteur = 'Jules';
```


+++

Pour une recherche avec un motif :
```sql
SELECT NomAuteur,PrenomAuteur FROM Auteurs WHERE PrenomAuteur LIKE '%u%';
```
```{code-cell}
:tags: [remove-input]
%sql SELECT NomAuteur,PrenomAuteur FROM Auteurs WHERE PrenomAuteur LIKE '%u%';
```


+++

### Ordonner les résultats

+++

On peut ordonner les résultats :
```sql
SELECT * from Auteurs ORDER BY AnneeNaissance;
```
```{code-cell} ipython
:tags: [remove-input]
%sql SELECT * from Auteurs ORDER BY AnneeNaissance;
```


+++

On peut aussi ordonner les résultats en ordre inverse
```sql
SELECT * from Auteurs ORDER BY AnneeNaissance DESC;
```
```{code-cell} ipython
%sql SELECT * from Auteurs ORDER BY AnneeNaissance DESC;
```

[^mariadb]: Le rachat de MySQL par Oracle et l'abandon du développement de
  la version OpenSource à conduit à cette divergence libre.
[^liberté]: au sens des licences libres, recensées par la
  [FSF](https://fsf.org)
[^procedure_sql]: On peut stocker des procédures en sql, mais cela dépasse
  largement le contenu du cours de NSI. C'est une pratique répandue, mais
  qui ne fait pas nécessairement consensus.
