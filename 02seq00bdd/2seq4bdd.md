---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Exercices sur les bases de données

## Exercice 1

On considère la structure d’une base de données « bibliothèque » composée des 5 tables suivantes :

  * OUVRAGE (num\_ouvrage, éditeur, titre, premier\_auteur)
  * EXEMPLAIRE (num\_ouvrage, num\_exemplaire, date\_acquisition, prix, état\_exemplaire) avec état\_exemplaire = {sorti, disponible, réservé}
  * ABONNE (num\_abonné, nom\_abonné, date\_abonnement, nbre\_emprunts\_en\_cours, état\_abonné) avec état\_abonné = {valide, relancé, exclu}
  * DEMANDE\_EMPRUNT (num\_demande, date\_demande, num\_abonné, num\_ouvrage, état\_demande) avec état\_demande = {en\_attente, acceptée, refusée}
  * EMPRUNT (num\_emprunt, date\_emprunt, num\_demande, num\_ouvrage, num\_exemplaire)

Donner les clés primaires et étrangères pour chacune des tables ci-dessus.

## Exercice 2

```{image} https://gitlab.com/nreveret/nsi_exercices_terminale/-/raw/main/chapitres/database/images/db_nba.PNG
```

1. Identifier les clefs primaires
2. Identifier les clefs étrangères
3. Est-il possible qu'un agent soit associé à une seule commande ?
4. Est-il possible qu'une commande soit associée à plusieurs clients ?


## Exercice 3

Faire les exercices de la page http://deptfod.cnam.fr/bd/tp/

```{code-cell} ipython
:tags: [remove-cell]

%reload_ext mocodo
```

## Exercice 4

On considère la maintenance d'un réseau de laverie.

```{code-cell} ipython
:tags: [remove-input]
%%mocodo
MATERIEL: Codemat, Nommat, Dateachat, Fournitlavage, Coûtacquisition
LAVERIE: Numlaverie, Dateouverture, Ruelav, villelav, cplav
SALARIÉ: Numsal, Nomsal, Prénomsal, Adressesal, CPsal, villesal, Télsal
```

On considère vraies les affirmations suivantes :

+ Une même intervention peut-être effectuées par plusieurs salariés.
+ Un salarié peut réaliser plusieurs interventions.
+ Un matériel n'appartient qu'à une seule laverie.

**Reliez les tables entre-elles, en indiquant les cardinalités**

## Exercice 5

On considère une chaine hôtelière avec les contraintes suivantes :

+ une chambre donnée (surface, étage, nombre de lits, numéro) ne peut se
trouver que dans un hôtel
+ un hôtel possède plusieurs chambres
+ Un client peut réserver plusieurs chambres (à des dates différentes)
+ Une même chambre peut être réservée par plusieurs clients (à des dates
    différentes)
+ Une chambre ne peut appartenir qu'à une seul catégorie (standard, luxe …)

```{code-cell} ipython
:tags: [remove-input]
%%mocodo
CHAMBRE: , ,,
APPARTENIR, XX CHAMBRE, XX CATEGORIE
CATEGORIE: ,
HOTEL: , , ,
POSSEDE, XX HOTEL, XX CHAMBRE
CLIENT: id_client, nom_client, prénom_client
RÉSERVE, XX CLIENT, XX CHAMBRE : date_reservation
```

**Recopier et compléter le schéma.**

## Exercice 6

Soit un gestionnaire de parkings souterrains. On suppose que sa base de
données possède le schéma suivant :

```{code-cell} ipython
:tags: [remove-input]
%%mocodo -t diagram
CLIENT: numClient, nom, prénom
SE GARER, 11 CLIENT, 1N EMPLACEMENT : horodatage, duree, tarif
EMPLACEMENT: numEmpl, surface, niveau, numPark
APPARTIENT, 1N EMPLACEMENT, 1N PARKING
PARKING: numPark, rue, ville
```
```{code-cell} ipython
:tags: [remove-input]
%mocodo --input mocodo_notebook/sandbox.mld
```

1. Expliquer pourquoi la clef primaire de la relation `SE GARER` comprend la
   date (`horodatage`).

   Le gestionnaire vous demande que, dans tous les parkings, les
   emplacements soient numérotés à partir de 1. Ainsi, le parking 1
   disposerai des place 1, 2, 3, …, le parking 2 également etc.
2. Proposer une modification pour prendre en compte cette contrainte.


## Exercice 7

Le camping de Camargue loue des emplacements pour installer des tentes ou
des caravanes. Il loue également des bungalows avec terrasse.

```{code-cell} ipython
:tags: [remove-input]
%%mocodo
Client: numCli, nomCli, prénomCli, rueCli, cpCli, villeCli, melCli, telCli
Location: ,                                               ,,
Emplacement: idEmpl, alléeEmpl, surface, ,
TypeEmplacement: idTypeEmple, libelleTypeEmpl, éléctricité, eau, tarifEmpl,

Saison: idSaison, libelleSaison
Bungalow: idBung, alléeBung, anneeBung, ,
Zone: idZone, libelleZone, ,
TypeBung: idTypeBung, libelleTypeBung, tarifBung
```

```{sidebar} Fiche de location L5638 imprimée
<div sytle="border: 1pt">
Numéro de bungalow : B23

3 allées des ormes

<p style="text-align: right">Client numéro 12734</p>
<p style="text-align: right">Nom: Oot</p>
<p style="text-align: right">Prénom : Robert</p>

Date de début : 15/08/2021

Date de  fin : 31/08/2021



Saison : Haute

Prix TTC : 1850 €
</div>
```
+ une location est réalisée par un seul client par bungalow
+ une location est réalisée par un seul client par emplacement
+ chaque emplacement appartient à une zone déterminée et possède un type
précis
+ chaque bungalow appartient à une zone déterminée et possède un type
précis
+ une location est réalisée pour une durée précise (date de début et de fin)
+ les locations n'ont pas le même prix en saison basse, normale ou haute.

