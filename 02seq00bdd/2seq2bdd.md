---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---



# Exemple complet

+++

Créer une table Livre et une table Thème avec les données suivantes :

| Titre                     | NomAuteur | PrenomAuteur | AnneeNaissance | Langue   | AnneePubli | Themes                                                 |     |
| ------------------------- | --------- | ------------ | -------------- | -------- | ---------- | ------------------------------------------------------ | --- |
|                           |           |              |                |          |            |                                                        |     |
| 1984                      | Orwell    | George       | 1903           | Anglais  | 1949       | Totalitarisme, science-fiction, anticipation, Dystopie |     |
| Dune                      | Herbert   | Frank        | 1920           | Anglais  | 1965       | science-fiction, anticipation                          |     |
| Fondation                 | Asimov    | Isaac        | 1920           | Anglais  | 1951       | science-fiction, Economie                              |     |
| Le meilleur des mondes    | Huxley    | Aldous       | 1894           | Anglais  | 1931       | Totalitarisme, science fiction, dystopie               |     |
| Fahrenheit 451            | Bradbury  | Ray          | 1920           | Anglais  | 1953       | science-fiction, Dystopie                              |     |
| Ubik                      | K. Dick   | Philip       | 1928           | Anglais  | 1969       | science-fiction, anticipation                          |     |
| Chroniques martiennes     | Bradbury  | Ray          | 1920           | Anglais  | 1950       | science-fiction, anticipation                          |     |
| La nuit des temps         | Barjavel  | René         | 1911           | Français | 1968       | science-fiction, tragédie                              |     |
| Blade Runner              | K. Dick   | Philip       | 1928           | Anglais  | 1968       | Intelligence artificielle, science fiction             |     |
| Les Robots                | Asimov    | Isaac        | 1920           | Anglais  | 1950       | science fiction, Intelligence artificielle             |     |
| La Planète des singes     | Boulle    | Pierre       | 1912           | Français | 1963       | science fiction, Dystopie                              |     |
| Ravage                    | Barjavel  | René         | 1911           | Français | 1943       | Science-Fiction, anticipation                          |     |
| Le Maître du Haut Château | K. Dick   | Philip       | 1928           | Anglais  | 1962       | Dystopie, Uchronie                                     |     |
| Le monde des A            | Van Vogt  | Alfred Elton | 1912           | Anglais  | 1945       | science fiction, IA                                    |     |
| La Fin de l’éternité      | Asimov    | Isaac        | 1920           | Anglais  | 1955       | science-fiction, voyage dans le temps                  |     |
| De la Terre à la Lune     | Verne     | Jules        | 1828           | Français | 1865       | Science-Fiction, aventure                              | 

La table livre comporte un `IdLivre` (clef primaire sur la table), un `TitreOuvrage`, une référence à `IdAuteur` et une `AnneePubli`.

```{code-cell} ipython
:tags: [remove-cell]
reload_ext sql
```
```{code-cell} ipython
:tags: [remove-cell]
%sql sqlite:///livres.sqlite
```

```{code-cell} ipython
%%sql 
CREATE TABLE Livres (
    IdLivre       INTEGER  PRIMARY KEY,
    TitreOuvrage      TEXT,
    IdAuteur       INTEGER,
    AnneePubli INTEGER,
    FOREIGN KEY(IdAuteur) REFERENCES Auteurs(IdAuteur)
);
```

```{code-cell} ipython
%%sql
INSERT INTO Livres (TitreOuvrage, IdAuteur, AnneePubli) VALUES
("1984",1,1949),
("Dune",2,1965),
("Fondation",3,1951),
("Le meilleur des mondes",4,1931)
```

+++

## Les thèmes

```{code-cell} ipython
%%sql
CREATE TABLE Themes (
    IdTheme INTEGER PRIMARY KEY,
    Theme TEXT
);
```

```{code-cell} ipython
%%sql
INSERT INTO Themes (Theme) VALUES
("Science-Fiction"),
("Totalitarisme"),
("Anticipation"),
("Dystopie"),
("Intelligence artificielle"),
("Utopie"),
("Voyage dans le temps"),
("Économie") ;
```

+++ {"slideshow": {"slide_type": "slide"}}

## Relation entre ouvrages et thèmes

Une table dédié pour cette relation

```{code-cell} ipython
%%sql
CREATE TABLE RelationLivreThemes (
    IdRelation INTEGER PRIMARY KEY,
    IdLivre INTEGER,
    IdTheme INTEGER,
    FOREIGN KEY (IdLivre) REFERENCES Livres(IdLivre),
    FOREIGN KEY (IdTheme) REFERENCES Themes(IdTheme)
) ;
INSERT INTO RelationLivreThemes (IdLivre,IdTheme)
VALUES (1,1),(1,2),(1,3),(1,4) ;
```

## Cardinalité

Les relations entre les premières tables étaient de types $1-n$, c'est à
dire qu'à un auteur correspondait une langue par exemple (mais qu'une langue
était parlée/écrite chez plusieurs auteurs)

La nouvelle relation entre ouvrages et thèmes est $n-n$

Pour récupérer ces relations, il est nécesaaire de  passer par une table externe.

```{code-cell} ipython
%%sql
SELECT TitreOuvrage, Theme
FROM Livres
JOIN RelationLivreThemes
USING (IdLivre) JOIN Themes USING (IdTheme)
```


