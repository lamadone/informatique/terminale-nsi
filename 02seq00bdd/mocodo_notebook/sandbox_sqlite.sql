.open "LIVRES";

CREATE TABLE "AUTEUR" (
  "idauteur" int,
  "nom" text,
  "prénom" text,
  "langue" int,
  "date_de_naissance" date,
  PRIMARY KEY ("idauteur")
);

CREATE TABLE "ÉCRIT" (
  "idauteur" int,
  "idlivre" int,
  PRIMARY KEY ("idauteur", "idlivre"),
  FOREIGN KEY ("idauteur") REFERENCES "AUTEUR" ("idauteur"),
  FOREIGN KEY ("idlivre") REFERENCES "LIVRE" ("idlivre")
);

CREATE TABLE "LIVRE" (
  "idlivre" int,
  "titre" text,
  "idauteur" int,
  "année_parution" int,
  PRIMARY KEY ("idlivre")
);

CREATE TABLE "LANGUE" (
  "idlangue" int,
  "langue" text,
  PRIMARY KEY ("idlangue")
);

CREATE TABLE "PARLE" (
  "idauteur" int,
  "idlangue" int,
  PRIMARY KEY ("idauteur", "idlangue"),
  FOREIGN KEY ("idauteur") REFERENCES "AUTEUR" ("idauteur"),
  FOREIGN KEY ("idlangue") REFERENCES "LANGUE" ("idlangue")
);

CREATE TABLE "THEME" (
  "idtheme" int,
  "theme" text,
  PRIMARY KEY ("idtheme")
);

CREATE TABLE "CONCERNE" (
  "idlivre" int,
  "idtheme" int,
  "idrelation" int,
  PRIMARY KEY ("idlivre", "idtheme"),
  FOREIGN KEY ("idlivre") REFERENCES "LIVRE" ("idlivre"),
  FOREIGN KEY ("idtheme") REFERENCES "THEME" ("idtheme")
);