---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: 'Python 3.7.6 64-bit (''base'': conda)'
  name: python3
---

```{code-cell} ipython
def bon_parenthésage(expression: str) -> bool:
    """
    Une fonction pour vérifier si une chaine de caractères est "bien" parenthésée.

    Parameters:
    -----------
    expression : str
        Une chaine contenant des parenthèses
    
    Returns:
    --------
    bool
        True si la chaine est correctement parenthésée
        False sinon
    Examples:
    ---------
    >>> bon_parenthésage("( a + b)*[c + d]")
    True
    >>> bon_parenthésage("(a * [b + c] + d)*e")
    True
    >>> bon_parenthésage("([a + b) * c] * d)")
    False
    >>> bon_parenthésage("]([a + b])")
    False
    """
    pile = []
    PAIRE_DELIMS = {'{':'}','(':')','[':']'}
    for caractère in expression:
        if caractère in PAIRE_DELIMS:
            pile.append(caractère)
        if caractère in PAIRE_DELIMS.values():
            try:
                if not (caractère == PAIRE_DELIMS[pile.pop()]):
                    return False
            except IndexError:
                return False
    if len(pile) > 0:
        return False
    return True
```

```{code-cell} ipython
%doctest_mode ON
from doctest import testmod
testmod(verbose=True)
```

```{code-cell} ipython

```
