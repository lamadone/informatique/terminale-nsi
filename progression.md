Première séquence {#première-séquence .unnumbered}
=================

Rentrée à fin septembre {#rentrée-à-fin-septembre .unnumbered}
-----------------------

Notions travaillées {#notions-travaillées .unnumbered}
-------------------

-   Listes, piles, files

-   vocabulaire de la POO

-   Paradigmes de programmation (exposés)

Deuxième séquence {#deuxième-séquence .unnumbered}
=================

Début octobre aux vacances de la toussaint {#début-octobre-aux-vacances-de-la-toussaint .unnumbered}
------------------------------------------

Notions travaillées {#notions-travaillées-1 .unnumbered}
-------------------

-   Base de données

-   Sécurisation des communications

-   Mise au point d'un programme

Troisième séquence {#troisième-séquence .unnumbered}
==================

Mois de novembre {#mois-de-novembre .unnumbered}
----------------

Notions travaillées {#notions-travaillées-2 .unnumbered}
-------------------

-   Recherche textuelle

-   Modularité

-   Gestion des processus

-   Arbres

Premier projet {#premier-projet .unnumbered}
==============

Décembre + vacances {#décembre-vacances .unnumbered}
-------------------

Création d'un site web de recherche textuelle dans un corpus de textes.

Notions travaillées {#notions-travaillées-3 .unnumbered}
-------------------

On revient sur la mise en place des outils liés à la PO et aux BD

Séquence 4 {#séquence-4 .unnumbered}
==========

Janvier et début février {#janvier-et-début-février .unnumbered}
------------------------

Notions travaillées {#notions-travaillées-4 .unnumbered}
-------------------

-   Exposés : SoC

-   Arbres

-   Graphes

-   Protocole de routage

-   Algos sur les graphes

-   Algos sur les arbres

Projet 2 {#projet-2 .unnumbered}
========

Programmation dynamique ?

Séquence 5 {#séquence-5 .unnumbered}
==========

-   Structure de données

-   Récursivité

-   Notion de programme en tant que donnée Calculabilité, décidabilité
    (exposé ?)

-   Méthode diviser pour régner
