---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Généralités sur les graphes

## Définitions

:::{prf:definition} Graphe
* Un graphe orienté est la donnée d'un ensemble de sommets $S$ et d'arcs
$A$ sous la forme de couples $(s_i, s_j)$, où $s_k$ est élément de $S$.
* Si, pour toute arcs $(s_i, s_j)$, $A$ contient aussi l'arc $(s_j, s_i)$,
  on dit que le graphe est non orienté. On parle alors d'arêtes et on les
  notes sous forme d'ensemble.
* Si l'ensemble des arcs contient des triplets de la forme $(s_i, s_j, p)$,
  avec $s_k \in S$ et $p \in ]0 ; +\infty[$, on dit que le graphe est
  pondéré.
:::

:::{margin} En mathématiques
  Les graphes y sont étudiés de façon différentes
  avec un autre outillage : les matrices.
:::

::::{prf:example}
:::{mermaid}
graph LR
    A((A)) --> B((B))
    A --> C((C))
    B --> D((D))
    C --> D
    D --> A
    D --> C
:::
::::

En Python, on note un tel graphe sous la forme d'un dictionnaire, dont les clefs sont les sommets et les valeurs sont la liste des voisins.

::::{prf:example}
:::{code-block} python
graphe = {
  'A': ['B', 'C'],
  'B': ['D'],
  'C': ['D'],
  'D': ['C', 'A']
}
:::
::::

:::{margin} Cas des graphes non orientés
  On note une arête (par exemple A-B) par l'arc A->B et l'arc B->A.
:::

:::{margin} Cas des graphes pondérés
  Dans ce cas, on remplace la liste des voisins par un dictionnaire des pondérations.
:::

## Représentation avec les matrices

:::{prf:definition} Matrice
En mathématiques, une matrice est objet algébrique se ramenant à un tableau à $n$ lignes et $p$ colonnes. Si $ n = p$, on dit que la matrice est carrée.
:::

:::{margin} Matrice en mathématiques
  Les matrices sont un sujet d'étude très vaste, aussi bien en mathématiques qu'en méthodes numériques.
:::

:::{prf:definition} Matrice d'adjacence
La matrice d'adjacence du graphe $G = (\{s_1,...,s_n\}, \{(s_i, s_j), ..., (s_k, s_l)\})$ est un tableau de booléens (ou de nombres pour les graphes pondérés), contenant `True` dans la cellule $(i,j)$ si $(s_i,s_j)$ dans le graphe et `False` sinon.
:::

::::{prf:example} Matrice d'ajacence de l'exemple
:::{code-block} python
M = [
  [False, True, True, False],
  [False, False, False, True],
  [False, False, False, True],
  [True, False, True, False]
]
:::
::::

:::{prf:remark} Coût de stockage
:class: "margin"
Comme la matrice d'adjaence d'un graphe à $n$ sommets est carrée de taille $n^2$, le coût de stockage peut vite devenir important.
:::

[Algos sur les graphes](10seq03_graphes_parcours.md)
