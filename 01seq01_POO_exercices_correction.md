---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: 'Python 3.7.6 64-bit (''base'': conda)'
  name: python3
---

# Correction du TP sur la mise en oeuvre d'une classe Fraction

```{include} ./cours/fraction.py
:start-line: 11
:end-line: 23
:code: python
:parser: myst_parser.sphinx_
```

```{code-cell} ipython
f = Fraction(2,3)
```

```{code-cell} ipython
f
```

```{code-cell} ipython
print(f)
```

```{code-cell} ipython
f.numerateur
```

```{code-cell} ipython
f.egal(Fraction(4,6))
```

```{code-cell} ipython
f.egal(Fraction(4,5))
```

```{code-cell} ipython
f.somme(Fraction(4,5))
```

```{code-cell} ipython
print(_)
```

```{code-cell} ipython
g = f.inverse()
```

```{code-cell} ipython
type(g)
```

```{code-cell} ipython
simplifier(Fraction(4,6))
```

```{code-cell} ipython
f
```

```{code-cell} ipython
g = Fraction(4,5)
```

```{code-cell} ipython
f + g
```

```{code-cell} ipython

```
