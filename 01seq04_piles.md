---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---


# Piles

## Présentation sommaire

Une pile est une structure de données organisée suivant le principe du dernier arriver, premier parti. On utilise souvent l'acronyme anglais __LIFO__ : Last In, First Out.

```{image} https://st2.depositphotos.com/1020091/11330/v/450/depositphotos_113304572-stock-illustration-plates-stack-icon.jpg
:alt: Pile d'assiette
:width: 150px
:align: center
```


Il faut se représenter une pile d'assiette manipulée par un enfant : celui-ci n'a pas le droit de prendre plus de deux assiettes simultanément. Il ne peut donc prendre que la première assiette, celle du dessus de la pile. De même, il ne peut que poser une assiette sur le haut d'une pile.

On considère qu'il  peut créer des piles et que certaines piles sont vides (il faut imaginer un emplacement vide dans le placard).


## Quatre primitives essentielles

+ Créer une Pile

```{code-cell} ipython
:tags: [remove-cell]
from cours.structures import Pile
```

```{code-cell} ipython
P = Pile()
```

+ Est vide

```{code-cell} ipython
P.est_vide()
```

+ Empiler

```{code-cell} ipython
P.empiler(1)
```

```{code-cell} ipython
P.empiler(2)
```

+ Dépiler

```{code-cell} ipython
P.depiler()
```

```{code-cell} ipython
type(P)
```

## Exercices


Écrire une _classe_ qui permet de modéliser une pile.

```{code-cell} ipython
class Pile:
  pass
```

```{code-cell} ipython
:tags: [remove-cell]
class Pile:
    def __init__(self):
        self._L = []
    def __repr__(self):
        return f"{self._L}"
    def empiler(self, a):
        self._L = self._L + [a]
    def depiler(self):
        return self._L.pop()
    def est_vide(self):
        if len(self._L) == 0:
            return True
        else:
            return False
```

```{code-cell} ipython
p = Pile()
assert p.est_vide() == True
assert p.empiler(1) == None
assert p.est_vide() == False
assert p.empiler(2) == None
assert p.depiler() == 2
assert p.est_vide() == False
assert p.depiler() == 1
assert p.est_vide() == True
```

Compléter la classe avec une méthode ___len___ (on écrira `_len` pour éviter
de redéfinir la fonction `len()` de Python.)

```{code-cell} ipython
:tags: [hide-cell]
def _len(self):
    return len(self._L)
```

```{code-cell} ipython
:tags: [remove-cell]
setattr(Pile,'len',_len)
setattr(Pile,'__len__',_len)
```

```{code-cell} ipython
p = Pile()
for i in range(10):
    p.empiler(1)
assert p.len() == 10
p = Pile()
for i in range(24):
    p.empiler(i)
assert p.len() == 24
```

Écrire une méthode ___top___ permettant de renvoyer l'élément de tête de la pile (sans le dépiler)

```{code-cell} ipython
:tags: [hide-cell]
def top(self):
    return self._L[-1]
```

```{code-cell} ipython
:tags: [remove-cell]
setattr(Pile,'top',top)
```


```{code-cell} ipython
p = Pile()
for i in range(16):
    p.empiler(i**2)
assert p.top() == 225
```

Écrire une méthode ___clear___ permettant de vider la pile.

```{code-cell} ipython
:tags: [hide-cell]
def clear(self):
    self._L = []
```

```{code-cell} ipython
:tags: [remove-cell]
setattr(Pile,'clear',clear)
```

```{code-cell} ipython
p = Pile()
for i in range(10):
    p.empiler(i**2)
p.clear()
assert p.est_vide() == True
```

Écrire une méthode ___dup___ permettant de dupliquer l'élément de tête

```{code-cell} ipython
:tags: [hide-cell]
def dup(self):
    self.empiler(self.top())
```

```{code-cell} ipython
:tags: [remove-cell]
setattr(Pile,'dup',dup)
```


```{code-cell} ipython
p = Pile()
for i in range(10):
    p.empiler(i**2)
p.dup()
assert p.depiler() == p.depiler()
```

Écrire une méthode ___swap___ permettant d'échanger les deux éléments du sommet de la pile

```{code-cell} ipython
:tags: [hide-cell]
def swap(self):
    if len(self._L) < 2:
        raise Exception('La taille de la pile doit être ≥ 2')
    self._L[-1],self._L[-2] = self._L[-2],self._L[-1]
```

```{code-cell} ipython
:tags: [remove-cell]
setattr(Pile,'swap',swap)
```


```{code-cell} ipython
p = Pile()
for i in range(10):
    p.empiler(i**2)
p.swap()
assert p.depiler() == 64
assert p.depiler() == 81
p = Pile()
p.empiler(1)
try:
    p.swap()
except Exception as msg: 
    print(msg) 
```

On a désormais la classe «complète»

```{code-cell} ipython
:tags: [remove-input, hide-output]
from IPython.display import Code
import inspect
from cours.structures import Pile
code = inspect.getsource(Pile)
Code(data = code,language = 'python3')
```

