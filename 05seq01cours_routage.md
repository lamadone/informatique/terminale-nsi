# Routage dans un réseau

## Rappels de Première 

### Les modèles du réseau

Afin de bien comprendre le principe d'un réseau, on s'intéresse dans un
premier temps à modéliser les interactions sur celui-ci.
L'[OSI](https://osi) a proposé un modèle en couche assez théorique qui
permet par exemple d'investiguer une panne.

|n°|Couche | Exemple de norme|
|--|-------|-----------------|
|7 | Applicationion | Web |
|6 | Présentation | HTML/XML |
|5 | Session | HTTPS |
|4 | Transport | TCP |
|3 | Réseau | IP |
|2 | Liaison | Ethernet |
|1 | Physique | RJ45 |

Ainsi, on commence à une des extrémités du tableau et on se dirige vers
l'autre extrémité en testant de façon rigoureuse les différents éléments du
réseau.

On peut en provfiter pour rappeler quelques notions sur les différentes
couches :
1. Sur la __couche physique__ transitent des suites de bits, c'est à dire
   des éléments pouvant être à 0 ou à 1. Cette couche physique peut-être
   constitué par des supports variées : paire de cuivre, ondes (lumineuse ou
       hertzienne, …) Un problème sur cette couche correspond généralement à
   un problème dans le monde physique.
2. Sur la __couche de liaison__ (souvent notée L2) transitent désormais des
   trames, des unités (regroupement de bits). Ces trames peuvent être
   reconnues par certains équipements (les switches). La norme désormais
   majoritaire de trame est Ethernet et on utilise essentillement de la
   commutation de trames (avec un commutateur ou switch). Cette commutation
   à ce niveau n'est possible que sur des «petits» réseaux.
3. Sur la __couche de réseau__ transitent des paquets, qui permettent
   réellement de parler de réseau. Ici nos paquets seront de type IP
   (Internet Protocol). Cette couche doit prendre en charge trois fonctions :
    + le contrôle de flux
    + le routage
    + l'adressage
4. Sur la __couche de transport__ on achemine des messages, c'est à dire des
   données utiles pour les utilisateurs finaux (on parle de datagramme TCP
       ou UDP, ou encore de paquet ICMP)
5. Sur la __couche de session__, on gère les connexions, c'est à dire
   un ensemble de paquets permettant la connexion entre deux éléments du
   réseaux. Il s'agit souvent de ce qu'on appelle «protocoles applicatifs»
   correspondant à des protocoles comme SSH, HTTP/HTTPS, IMAP, SMTP, … Dans
   cette couche, on gère l'établissement de la session.
6. Sur la __couche de présentation__, on gère la syntaxe utilisée pour
   communiquer. Par exemple, la syntaxe pour communiquer des pages WEB est
   constituée de «verbes» comme GET, …
7. Sur la __couche d'application__, on gère la sémantique de l'échange,
   c'est à dire le sens que l'on veut lui donner.

On peut noter que pour la éléments entre deux réseaux, seules les 3
premières couches sont utilisées :

```
+--+--------------+                       +--+--------------+
|7 | Application  |                       |7 | Application  |
|6 | Présentation |                       |6 | Présentation |
|5 | Session      |                       |5 | Session      |
|4 | Transport    |    +--+----------+    |4 | Transport    |
|3 | Réseau       |    |3 | Réseau   |    |3 | Réseau       |
|2 | Liaison      |----|2 | Liaison  |----|2 | Liaison      |
|1 | Physique     |    |1 | Physique |    |1 | Physique     |
+--+--------------+    +--+----------+    +--+--------------+

```

À côté et en parallèle de ce modèle assez complet et théorique, on utilise
souvent un modèle simplifié, mais pratique, le modèle TCP/IP qu'on peut
résumer avec le tableau suivant.

| #id | Couche |
|-----|--------|
|1 | Application (Application web)|
|2 | Transport (TCP) |
|3 | internet (IP) |
|4 | Physique (Ethernet et réseau Physique) |

:::{warning}
Les couches de ces deux modèles coïncident partiellement et le premier
modèle (OSI) est très complet et ne précise rien quand aux technologies
employées.
:::

### Les adresses sur le réseau

Sur un réseau, on distingue plusieurs types d'adresses :
+ les adresses MAC (Media Access Control), sur la couche 2 du modèle OSI ;
+ les adresses IP (Internet Protocol), sur la couche 3 du modèle OSI.

:::{prf:definition} Adresse MAC
Une adresse MAC est un ensemble de 6 nombres hexadécimaux à deux chiffres de
la forme xx:xx:xx:xx:xx:xx
:::

:::{info}
Les 3 à 4 premiers chiffres identifient un fabriquant et le reste identifie
(comme un numéroe de produit) l'équipement. On peut utiliser un site comme
https://maclookup.app/ pour trouver le nom du fabriquant.
:::

L'adresse MAC est celle présente dans une trame Ether sur la couche 2 du
modèle OSI. C'est celle-ci qui est reconnue par les switchs.

Pour les adresses IP, on peut distinguer IPv4 avec des adresses codées avec
4 octets et les adresses IPv6 codées avec 8 blocs de 2 octets.

:::{exercise}
Calculer le nombre de total d'adresses IPv4 et IPv6.
:::

On peut voir le [](TP%20sur%20les%20masques%20de%20réseau.md).

## Notions sur le routage

### Nécessité du routage

::::{margin}
Le [RFC 1918](https://datatracker.ietf.org/doc/html/rfc1918#section-3)
définit 3 espaces particulier d'adresse IPv4 (10.0.0.0/8, 172.16.0.0/12 et
  192.168.0.0/16) qui ne peuvent être routées en dehors d'un AS (Autonomous
    System, l'entité qui gère de façon centralisée un réseau). Pour
accéder à internet depuis une telle adresse, il est nécessaire de faire du
(S)NAT (**S**ource **N**etwork **A**ddress **T**ranslation). Il est possible
aussi d'accéder à certaines ressources (IP:PORT ou IP) en faisant du DNAT
(**D**estination NAT).
Par exemple, la commande suivante, sur un noyau Linux
```sh
iptables -t nat -A POSTROUTING -s 192.168.1.0/24 -o eth0 -j MASQUERADE
```
permettent de «masquer» les adresses IP du préfixe 192.168.1.0/24 et de
faire comme si elles sortaient de l'interface eth0 du routeur.
La commande
```sh
iptables -t NAT -A PREROUTING -d 192.0.2.1 --dport 80 --to 192.168.2.23:8080 -j DNAT
```
permet de rediriger les paquets IP arrivant sur l'adresse 192.0.2.1 sur le
port 80 et de les rediriger vers l'adresse 192.168.2.23 sur le port 8080.
::::

Il y a plusieurs arguments qui imposent le besoin de routage :
+ la commutation Ethernet ne gère que de petits réseaux
+ le besoin de cloisonner des réseaux (un service particulier d'une
    entreprises, une entreprise par rapport aux autres, …
+ la pénurie d'adresse IPv4 qui impose l'utilisation du NAT

### Principe généraux

:::{prf:definition} routage
Le **routage** est l'action de transférer des paquets IP d'un réseau à un autre.
:::

:::{prf:definition} routeur
Un **routeur** est un équipement qui réalise l'action de routage.
:::

Chaque équipement d'un réseau dispos d'une table de routage qui indique
comment acheminer les paquets.

:::{info}
La commande `ip route show` permet d'afficher la table de routage d'un
ordinateur avec un noyau Linux.
:::

On peut schématiser un échange de type ICMP ECHO REQUEST/REPLY (le mécanisme
    mis en œuvre lorsqu'on utilise la commande `ping`.)

:::{mermaid}
sequenceDiagram
  Client1 ->> R1: Envoi vers la première destination
  Note over R1: Décision de routage
  R1 ->> R2: Envoi vers la destination suivante
  R2 ->> Client2: Réception par le client final
  Client2 ->> R2: Envoi du paquet en retour
  R2 ->> R1: Envoi vers la destination suivante
  R1 ->> Client1: Réception en retour
:::

::::{warning}
Le routage doit être fait dans les deux sens. Autrement dit, si on considère
R1 d'ip 192.0.2.1 et de masque 255.255.255.0 et R2 d'ip 198.51.100.1 et de
masque 255.255.255.0 liés par un cable, la table de routage de R1 doit
contenir

|Destination|Passerelle|Masque|Metrique|
|-----------|----------|------|--------|
|198.51.100.0|192.0.2.1|255.255.255.0|100|

et la table de routage de R2 doit contenir

|Destination|Passerelle|Masque|Metrique|
|-----------|----------|------|--------|
|192.0.2.0|198.51.2.1|255.255.255.0|100|
::::

:::{note}
:class: dropdown
Dans l'exemple classique présenté ci-contre, les chemins aller et retour
sont les mêmes, mais ça n'est pas une obligation.
:::

## Pratique du routage

On peut distinguer 3 méthodes générales pour maintenir les tables de
routages :
1. méthode statique : l'administrateur réseau configure les équipements avec
   les tables de routage
2. méthode à «vecteur de distance» : on définit une distance en nombre de
   sauts (routeurs traversés) pour construire, sur chaque routeur, la table
   de routage à partir des tables de routage des voisins.
3. méthode à «vecteur de chemin» : on minimise les chemins parcourus en
   affectant un coût à chaque chemin

### Routage statique

On considère le réseau suivant :
![](Routage_statique.png)

% https://gist.github.com/vincentxavier/0b9112c7ba854cc077ea5cb521a54a1d

Dans ce réseau, pour passer du routeur 1 au routeur 5, on peut passer par :
+ directement
+ par R2
+ par R4
+ par R3 - R4

Sur R1, on aura la table de routage partielle suivante
|Destination|Passerelle|Masque|Metrique|
|-----------|----------|------|--------|
|192.165.25.0|192.0.2.2|255.255.255.0|100|
|10.125.14.0|203.0.113.2|255.255.255.0|100|
|10.0.12.0|176.18.5.2|255.255.255.0|100|

:::{exercise}
Donner les tables de routages des autres routeurs
:::

### Routage avec RIP

RIP (Router Information Protocol) est un protocole de routage à vecteur de
distance. La distance retenue est le nombre de saut, c'est à dire le nombre
de routeurs traversés. Dans l'exemple précédent, on a la distance entre R2
R3 qui est de 2.

### Routage avec OSPF



<!--
https://datatracker.ietf.org/doc/html/rfc2453 RIP

https://datatracker.ietf.org/doc/html/rfc2328 OSPF

https://datatracker.ietf.org/doc/rfc5737/

https://fr.wikipedia.org/wiki/Routing_Information_Protocol

https://fr.wikipedia.org/wiki/Open_Shortest_Path_First

https://go.drawthe.net/#

https://github.com/cidrblock/drawthe.net

https://yaser-rahmati.gitbook.io/gns3/l2-switching-simulation

https://yaser-rahmati.gitbook.io/gns3/lab-8-basic-router-setup

https://www.sysnettechsolutions.com/en/configure-rip-version-2-on-cisco-router-in-gns3/

https://www.9tut.com/configuring-rip-gns3-lab

https://gns3vault.com/Labs/OSPF

https://gns3vault.com/ospf/ospf-single-area

https://go.drawthe.net/#
https://ciscotracer.wordpress.com/2014/01/08/routage-dynamique-avec-rip/
https://www.laintimes.com/configurer-le-protocole-ospf-avec-cisco-packet-tracer/
https://www.technig.com/configure-ospf-routing-protocol/
https://mermaid-js.github.io/mermaid/#/stateDiagram

https://www.sysnettechsolutions.com/en/configure-ospf-on-cisco-router-in-gns3/
https://www.science.smith.edu/~jcardell/Courses/CSC249/slides/
-->
