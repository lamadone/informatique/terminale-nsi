---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: 'Python 3.7.6 64-bit (''base'': conda)'
  name: python3
---

```{code-cell} ipython3
import PIL.Image
```

```{code-cell} ipython3
import requests
r = requests.get('https://cdn.wallpapersafari.com/59/22/SQJuby.jpg')
```

```{code-cell} ipython3
import io
```

```{code-cell} ipython3
im = PIL.Image.open(io.BytesIO(r.content))
```

```{code-cell} ipython3
im.size
```

```{code-cell} ipython3
quadrant1 = im.crop((0,0,im.size[0]//2,im.size[1]//2))
```

```{code-cell} ipython3
quadrant1
```

```{code-cell} ipython3
quadrant2 = im.crop((im.size[0]//2 , 0, im.size[0],im.size[1]//2))
quadrant2
```

```{code-cell} ipython3
quadrant3 = im.crop((0,im.size[1]//2, im.size[0]//2,im.size[1]))
quadrant3
```

```{code-cell} ipython3
quadrant4 = im.crop((im.size[0]//2 ,im.size[1]//2 , im.size[0],im.size[1]))
quadrant4
```

```{code-cell} ipython3
image = PIL.Image.new(mode=im.mode,size=im.size)
```

```{code-cell} ipython3
image.paste(quadrant1,(im.size[0]//2 ,0))
image.paste(quadrant2,(im.size[0]//2 ,im.size[1]//2))
image.paste(quadrant3,(0,im.size[1]//2 ))
image.paste(quadrant4,(0,0))
image
```

```{code-cell} ipython3
def rotation(image):
    m, n = image.size # dimensions de l’image
    assert m == n, "l’image doit être carrée"
    assert n & (n-1) == 0, "la dimension doit être une puissance de 2"
    # Pas de cas de base : si n = 1 on ne fait rien
    if n > 1:
        k = n // 2
        # Diviser : découpe des blocs
        A = image.crop((0, 0, k, k))
        B = image.crop((k, 0, n, k))
        C = image.crop((k, k, n, n))
        D = image.crop((0, k, k, n))
        # Résoudre : rotation des blocs
        rotation(A)
        rotation(B)
        rotation(C)
        rotation(D)
        # Combiner : permutation des blocs
        image.paste(D,(0,0))
        image.paste(A,(k,0))
        image.paste(B,(k,k))
        image.paste(C,(0,k))
```

```{code-cell} ipython3
im
```

```{code-cell} ipython3
rotation(im)
```

```{code-cell} ipython3
im
```

```{code-cell} ipython3

```
