---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: 'Python 3.7.6 64-bit (''base'': conda)'
  name: python3
---

# Programmation orientée objet (TD)

## Pour commencer

```{exercise}
1. Écrire une classe `Eleve` qui contiendra les attributs `nom`, `classe` et
   `note`.
2. Instancier trois élèves de cette classe.
3. Écrire une fonction `compare(eleve1, eleve2)` qui renvoie le nom de
   l'élève ayant la meilleur note.
4. Écrire la méthode `__eq__` qui renvoie `True` si les élèves ont la même
   note et `False` sinon.
```

````{exercise}
Écrire deux classes, `Auteur` et `Livre`
```{kroki}
:type: mermaid

classDiagram


  class Livre {
    + auteur: Auteur
    + titre: str
    + annee_edition: int
    + modifie_date_publication(annee)
  }

  class Auteur {
    + nom: str
    + prenom: str
  }


```
````

```{exercise}
Écrire une classe Player qui :

1. ne prendra aucun argument lors de son instanciation.
2. affectera à chaque objet créé un attribut energie valant 3 par défaut.
3. affectera à chaque objet créé un attribut alive valant True par défaut.
4. fournira à chaque objet une méthode blessure() qui diminue l'attribut energie de 1.
5. fournira à chaque objet une méthode soin() qui augmente l'attribut energie de 1.
6. si l'attribut energie passe à 0, l'attribut alive doit passer à False et ne doit plus pouvoir évoluer.
```



## TD sur les fractions

Le but de ce Travail Dirigé est de prendre en main la réalisation d'une classe et l'utiliser en tant qu'objet.
Le T.D. se focalise donc sur cet aspect, même si d'autres aspects peuvent être travaillés.


Le travail doit être déposé avant demain dans le [dépot suivant](https://nuage03.apps.education.fr/index.php/s/RpdEXWB9SkJG29f)

```{warning}
Attention, une fois passé la date de demain soir, ce dépot ne sera plus accessible.
Votre fichier doit être au format Notebook (`*.ipynb)`, nommé suivant le schéma : `<date (format aaad-mm-jj)> <nom> <prenom> <titre du TP>`
```

+++

```{exercise}
Rédiger une classe `Fraction` permettant de
1. créer un objet Fraction à partir de son numérateur et dénominateur ;
2. tester si deux fractions sont égales avec

    ```python
    f = Fraction(2,3)
    g = Fraction(4,6)
    assert f.egal(g) == True
    ```

3. additionner deux fractions

    ```python
    f = Fraction(2,3)
    g = Fraction(4,5)
    h = f.somme(g)
    assert h.egal(Fraction(22,15)) == True
    ```
4. soustraire deux fractions
5. multiplier deux fractions
6. prendre l'inverse d'une fraction
7. diviser deux fractions
8. donner le numérateur et le dénominateur
9. simplier une fraction (peut être une fonction en dehors de la classe)

```

+++

On veillera à commenter ses fonctions, à donner des indications de types et à tester les entrées sorties sur des exemples simples.

+++

```python
def somme(a: float, b:float) -> float
    """Renvoie la somme de deux float
    
    Une bête addition.
    
    Args:
        a (float): Le premier opérande
        b (float): Le deuxième opérande
    
    Returns:
        float: la somme
        
    Exemples:
    >>> somme(2.2, 5.3)
    7.5
    """
    return a+b

assert somme(2.2, 5.3) == 7.5
```

`````{tip}
````{note}
Si la `docstring` contient `>>> fonction(args)` suivi du résultat attendu, on peut placer
```python
%%doctest
from doctest import testmod
testmod(verbose=True)
```
dans le Notebook Jupyter pour tester la fonction.
````
`````

```{code-cell} ipython
:tags: [hide-cell]

from IPython.display import Code
```

```{code-cell} ipython
:tags: [hide-cell]

Code(filename='fraction.py',language='python3')
```

```{code-cell} ipython

```
