---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---


# Structures de données essentielles



## Les listes

Attention, l'objet `list` de Python n'est pas une liste au sens des structures de données essentielles. Les créateurs du langage ont voulu le doter d'un objet multifonctions dont la mise en œuvre est masquée.

Dans ce cours, nous utiliserons `list` pour désigner le type `list` de Python, qui nous servira à nos implémentations et _Liste_ (ou `Liste` dans le code) pour la structure de donnée _Liste_


### Opérations possibles avec les listes

```{code-cell} ipython
from cours.structures import Liste
```

```{code-cell} ipython
liste = Liste()
```

```{code-cell} ipython
liste
```

```{code-cell} ipython
liste.tete(1)
```

```{code-cell} ipython
liste
```

```{code-cell} ipython
liste.queue()
```

```{code-cell} ipython
liste
```

```{code-cell} ipython
liste.queue()
```

```{code-cell} ipython
liste.est_vide()
```

```{code-cell} ipython
liste.tete('a')
```

```{code-cell} ipython
liste
```

```{code-cell} ipython
nl = Liste()
for i in range(10):
    nl.tete(i)
nl.queue()
nl.queue()[1].queue()
```

À l'aide de l'exemple ci-dessus, donner les fonctions (primitives) d'une liste.

```{code-cell} ipython
---
nbgrader:
  grade: false
  grade_id: cell-bfb05d1fda7f90a3
  locked: true
  schema_version: 3
  solution: false
  task: false
tags: [raises-exception]
---
liste[0]
```

Expliquer l'erreur présente ici.


On trouvera des informations supplémentaires sur
{cite:ps}`PythonListImplementation`

```{code-cell} ipython

```
