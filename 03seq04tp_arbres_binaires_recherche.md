---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: 'Python 3.9.9 64-bit (''.venv'': venv)'
  name: python3
---

# Arbres binaires de recheche

## Définition

```{prf:definition} arbre binaire de recherche
Un **arbre binaire de recherche** est un arbre binaire dans lequel, on a la
propriété suivante pour tout nœud `e` :
  + tous les nœuds du sous arbre gauche ont une valeur inférieure à la valeur de `e`
  + tous les nœuds du sous arbre droite ont une valeur supérieure ou égale à la valeur de `e`
```

```{code-cell} ipython
:tags: [remove-cell]
from binarytree import Node
from IPython import display
```

```{code-cell} ipython
:tags: [remove-input]
abr = Node(5)
abr.left = Node(4)
abr.right = Node(8)
abr.left.left = Node(3)
affichage = abr.graphviz()
affichage
```

```{code-cell} ipython
:tags: [remove-input]
abr = Node(8)
abr.left = Node(5)
abr.left.left = Node(4)
abr.left.left.left = Node(3)
abr
affichage = abr.graphviz()
affichage
```
sont des arbres binaires de recherche. En revanche
```{code-cell} ipython
:tags: [remove-input]
abr = Node(1)
abr.left = Node(2)
abr.right = Node(3)
abr
affichage = abr.graphviz()
affichage
```
n'en est pas un.

```{exercise}
Donner tous les arbres binaires de recherche à 3 éléments
```

## Recherche dans un ABR

Écrire une fonction `appartient` qui permet de chercher dans un ABR

```{code-cell} ipython
def appartient(x,abr):
  pass
```

```{code-cell} ipython
:tags: [remove-cell]
def appartient(x,abr):
    '''détermine si x apparaît dans l'ABR
    '''
    if abr is None :
        return False
    if x < abr.valeur:
        return appartient(x,abr.gauche)
    elif x > abr.valeur:
        return appartient(x,abr.droite)
    else :
        return True
```

```{code-cell} ipython
:tags: [remove-input]
abr = Node(5)
abr.left = Node(4)
abr.right = Node(8)
abr.left.left = Node(3)
abr
affichage = abr.graphviz()
affichage
```
```{code-cell} ipython
:tags: [remove-cell]
from nb_myst import glue
glue('appartient',appartient(8,abr))
```
Le code suivant
```{code-cell} ipython
:tags: [remove-output]
appartient(8,abr)
```
renvoie {glue:}`appartient`, alors que le code
```{code-cell} ipython
:tags: [remove-cell]
glue('nappartient_pas',appartient(7,abr))
```
```{code-cell} ipython
:tags: [remove-output]
appartient(7,abr)
```
renvoie {glue:}`nappartient_pas`.



## Complexité de la recherche dans un ABR


